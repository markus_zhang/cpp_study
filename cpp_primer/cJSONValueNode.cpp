#include "cJSONValueNode.h"

long JSONValueNode::GetAsInteger() const
{
	return std::stol(jsonValue);
}

double JSONValueNode::GetAsDouble() const
{
	return std::stod(jsonValue);
}