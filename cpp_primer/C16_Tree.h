#pragma once
#include <vector>
#include "C16_TreeNode.h"

template <typename T>
class pBSTree
{
public:
	pBSTree() : root(nullptr), size(0) {}
	pBSTree(const T& newItem);

	int getSize() const { return size; }
	int getHeight() const;
	void insert(const T& newItem);
	void display() const;
	void displayTree() const;
	void remove(const T& item);
	bool contains(const T& Item) const;
private:
	pBSTreeNode<T>* root;
	int size;

	pBSTree(const pBSTree& newTree) {}

	int getHeightHelper(pBSTreeNode<T>* parentNode) const;
	void insertRecursion(pBSTreeNode<T>*& parentNode, const T& newItem);	// Recursively insert newItem, see insert() for entry
	void displayRecursion(pBSTreeNode<T>* node) const;
	void displayTreeHelper(std::vector<pBSTreeNode<T>*>& vNodes) const;
	bool containsHelper(pBSTreeNode<T>* node, const T& item) const;
	pBSTreeNode<T>*& getNodeByValue(const T& item);
	pBSTreeNode<T>*& getNodeByValueHelper(pBSTreeNode<T>*& node, const T& newItem);	// Must pass by reference otherwise creates a copy
	void removeNode(pBSTreeNode<T>*& node);
	pBSTreeNode<T>*& getPredecessor(pBSTreeNode<T>*& node);
	pBSTreeNode<T>*& getSuccessor(pBSTreeNode<T>*& node);
	pBSTreeNode<T>*& getPredecessorHelper(pBSTreeNode<T>*& node);	// Must pass by reference otherwise creates a copy
	pBSTreeNode<T>*& getSuccessorHelper(pBSTreeNode<T>*& node);
};

template <typename T>
pBSTree<T>::pBSTree(const T& newItem)
{
	root = new pBSTreeNode(newItem);
}


template <typename T>
int pBSTree<T>::getHeight() const
{
	return getHeightHelper(root);
}

template <typename T>
int pBSTree<T>::getHeightHelper(pBSTreeNode<T>* parentNode) const
{
	if (parentNode == nullptr)
	{
		return 0;
	}
	else
	{
		return 1 + std::max(getHeightHelper(parentNode->getLeftChild()), getHeightHelper(parentNode->getRightChild()));
	}
}

template <typename T>
void pBSTree<T>::insert(const T& newItem)
{
	insertRecursion(root, newItem);
	size += 1;
	return;
}

template <typename T>
void pBSTree<T>::insertRecursion(pBSTreeNode<T>*& parentNode, const T& newItem)
{
	//	Stop
	if (parentNode == nullptr)
	{
		parentNode = new pBSTreeNode<T>(newItem);
	}
	else
	{
		if (newItem <= parentNode->getItem())
		{
			insertRecursion(parentNode->getLeftChild(), newItem);
		}
		else
		{
			insertRecursion(parentNode->getRightChild(), newItem);
		}
	}
}

template <typename T>
void pBSTree<T>::display() const
{
	if (size <= 0)
	{
		return;
	}
	displayRecursion(root);
	std::cout << std::endl;
	return;
}

template <typename T>
void pBSTree<T>::displayRecursion(pBSTreeNode<T>* node) const
{
	//	Display left children -> Display self -> Display right children
	if (node->hasLeftChild())
	{
		displayRecursion(node->getLeftChild());
	}

	node->displayNode();

	if (node->hasRightChild())
	{
		displayRecursion(node->getRightChild());
	}
}

template <typename T>
void pBSTree<T>::displayTree() const
{
	//	Display as a tree
	std::vector<pBSTreeNode<T>*> vNodes;
	vNodes.push_back(root);

	displayTreeHelper(vNodes);
}

template <typename T>
void pBSTree<T>::displayTreeHelper(std::vector<pBSTreeNode<T>*>& vNodes) const	// Pass by value
{
	int vectorSize = vNodes.size();
	bool bNonNull = false;
	std::vector<pBSTreeNode<T>*> vNextLevel;
	for (auto i : vNodes)
	{
		if (i == nullptr)
		{
			std::cout << ", ";
			vectorSize -= 1;
		}
		else
		{
			i->displayNode();
			vectorSize -= 1;
			vNextLevel.push_back(i->getLeftChild());
			vNextLevel.push_back(i->getRightChild());
			bNonNull = true;
		}
		if (vectorSize <= 0)
		{
			std::cout << std::endl;
			break;
		}
	}
	vNodes.clear();
	if (bNonNull)
	{
		displayTreeHelper(vNextLevel);
	}
}

template <typename T>
void pBSTree<T>::remove(const T& item)
{
	//	1. Find the node
	//	2. If it's a leaf node, just delete (how?)
	//	3. If it has one child, replace the item with child's item and proceed to remove child
	//	4. If it has two children, choose either the inorder predecessor or successor
	pBSTreeNode<T>* node = getNodeByValue(item);
	if (node != nullptr)
	{
		removeNode(node);
	}
	else
	{
		std::cout << "Not Found!" << std::endl;
	}
}

template <typename T>
void pBSTree<T>::removeNode(pBSTreeNode<T>*& node)
{
	if (node->isLeaf())
	{
		delete node;
		node = nullptr;
		return;
	}
	if (!node->hasLeftChild())
	{
		node->setItem(node->getRightChild()->getItem());
		removeNode(node->getRightChild());
		return;
	}
	if (!node->hasRightChild())
	{
		node->setItem(node->getLeftChild()->getItem());
		removeNode(node->getLeftChild());
		return;
	}
	//	Two children case
	node->setItem(getPredecessor(node)->getItem());
	removeNode(getPredecessor(node));

	return;
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getPredecessor(pBSTreeNode<T>*& node)
{
	//	Goes left and then goes right wherever possible
	//	Note that node must have two children (see removeNode for condition)
	pBSTreeNode<T>* leftChild = node->getLeftChild();	//	This 100% will not be nullptr no need to check
	return getPredecessorHelper(leftChild);
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getSuccessor(pBSTreeNode<T>*& node)
{
	//	Goes left and then goes right wherever possible
	//	Note that node must have two children (see removeNode for condition)
	pBSTreeNode<T>* rightChild = node->getRightChild();	//	This 100% will not be nullptr no need to check
	return getSuccessorHelper(rightChild);
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getPredecessorHelper(pBSTreeNode<T>*& node)
{
	if (node->hasRightChild())
	{
		return getPredecessorHelper(node->getRightChild());
	}
	else
	{
		return node;
	}
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getSuccessorHelper(pBSTreeNode<T>*& node)
{
	if (node->hasLeftChild())
	{
		return getPredecessorHelper(node->getLeftChild());
	}
	else
	{
		return node;
	}
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getNodeByValue(const T& item)
{
	if (size <= 0)
	{
		return root;
	}
	return getNodeByValueHelper(root, item);
}

template <typename T>
pBSTreeNode<T>*& pBSTree<T>::getNodeByValueHelper(pBSTreeNode<T>*& node, const T& item)
{
	if (node == nullptr)
	{
		return node;	// Must have this line to stop if not found
	}
	if (node->getItem() == item)
	{
		return node;
	}
	if (node->getItem() > item)
	{
		return getNodeByValueHelper(node->getLeftChild(), item);
	}
	else
	{
		return getNodeByValueHelper(node->getRightChild(), item);
	}
}

template <typename T>
bool pBSTree<T>::contains(const T& Item) const
{
	if (size <= 0)
	{
		return false;
	}
	if (root->getItem() == Item)
	{
		return true;
	}
	if (root->getItem() > Item)
	{
		return containsHelper(root->getLeftChild(), Item);
	}
	else
	{
		return containsHelper(root->getRightChild(), Item);
	}
}

template <typename T>
bool pBSTree<T>::containsHelper(pBSTreeNode<T>* node, const T& item) const
{
	if (node == nullptr)
	{
		return false;
	}
	if (node->getItem() == item)
	{
		return true;
	}
	if (node->getItem() > item)
	{
		return containsHelper(node->getLeftChild(), item);
	}
	else
	{
		return containsHelper(node->getRightChild(), item);
	}
}