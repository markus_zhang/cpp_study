#pragma once
#include "C19_TriNode.h"

template <typename T>
class pTriTree
{
public:
	pTriTree() : root(nullptr), size(0) {}
	pTriTree(const T& newItem);

	int getSize() const { return size; }
	int getHeight() const;
	void insert(const T& newItem);
	void display() const;
	void displayTree() const;
	void remove(const T& item);
	bool contains(const T& Item) const;
private:
	pTriNode<T>* root;
	int size;

	void displayHelper(pTriNode<T>* parentNode) const;
	void insertHelper(pTriNode<T>*& parentNode, const T& newItem);
	void 
};

template <typename T>
void pTriTree<T>::display() const
{

}

template <typename T>
void pTriTree<T>::displayHelper(pTriNode<T>* parentNode) const
{
	if (parentNode != nullptr)
	{
		if (parentNode->isLeaf())
		{
			parentNode->displaySmall();
			parentNode->displayBig();
			return;
		}
		if (parentNode->isThreeNode())
		{
			displayHelper(parentNode->getLeftChild());
			parentNode->displaySmall();
			displayHelper(parentNode->getMiddleChild());
			parentNode->displayBig();
			displayHelper(parentNode->getRightChild());
			return;
		}
		if (parentNode->isTwoNode())
		{
			displayHelper(parentNode->getLeftChild());
			parentNode->displaySmall();
			displayHelper(parentNode->getRightChild());
			return;
		}
	}
}

template <typename T>
void pTriTree<T>::insertHelper(pTriNode<T>*& parentNode, const T& newItem)
{
	//	Step 1.		Check if parentNode is nullptr
	//	Step 2.1	If parentNode == nullptr then create a new node and put newItem into it 
	//				(parentNode is passed by reference so should be linked already)
	//	Step 2.2	If parentNode != nullptr then it could be leaf or twoNode or threeNode
	//	Step 3.1	If parentNode is leaf, then it may contain 1 or 2 elements
	//	Step 3.1.1	If parentNode is leaf containing 1 element, simply put newItem in parentNode
	//	Step 3.1.2	If parentNode is leaf containing 2 element, move the middle one of the three (newItem, smallItem and bigItem)
	//				1) parentNode is the root
	//				2) parentNode is not the root
	//	Step 3.2.1	parentNode is the root, we need to create a new root, insert the middle one of the three
	//				and then create leftChild for the new root, 
}