### SQL Notes

- **ALIAS**

	You must use the ALIAS everywhere once it has been declared somewhere in the script
	
	Example:
	```SQL
	SELECT	m.username                AS "User Name",
			m.balance                 AS "Remaining Balance"
	FROM   fund_modifications AS fm
			LEFT JOIN member AS m
				ON fm.user_id = m.id    
	WHERE  fm.type = "Impressions"
			AND m.username IS NOT NULL;
	```
	Note that we **must** use *m* instead of *member* in
	```SQL
	SELECT
	```
	even when we define the **ALIAS** `m` after it.

- **LEFT JOIN**
	
	The Syntax is:
	```SQL
	SELECT column_name(s)
	FROM	table1
			LEFT JOIN table2
				ON table1.column_name = table2.column_name;
	```
	It returns the `JOIN` of two tables:
	
	table1 with all of its rows, and table2, joined with table1 (imagine table2's columns put on the right side of table1, if we `SELECT` all of them we can actually see this). Then it check if for each row it satisfies `table1.column_name = table2.column_name`, if not then the content of table2 will be `NULL`.

    1) Note that **LEFT JOIN** will return **duplicate** records if ```DISTINCT``` is not used with ```SELECT``` or there is ```GROUP BY``` present. Consider the first piece of code:

		```SQL
		SELECT	m.username                AS "User Name",
				m.balance                 AS "Remaining Balance"
		FROM   fund_modifications AS fm
				LEFT JOIN member AS m
					ON fm.user_id = m.id    
		WHERE  fm.type = "Impressions"
				AND m.username IS NOT NULL;
		```
		```LEFT JOIN``` joins `member` to `fm` and let's see what happens for this record in table `member`:

		![L E F T J O I N 00](Images/LEFT_JOIN_00.png)

		`id` is 1 for `Manwin` and we need to match with the `user_id` field in `fund_modifications`:

		![L E F T J O I N 01](Images/LEFT_JOIN_01.png)

		There are many records with `user_id = 1`, and essentially the query will bring a lot of duplicate records:

		![L E F T J O I N Dup](Images/LEFT_JOIN_Dup.png)
		
		(BTW `IS NOT NULL` filters the null values)
		
		Now If we use
		```SQL
		SELECT DISTINCT
		```
		or put
		```SQL
		GROUP  BY m.username;
		```
		at the end of the query then we will be able to remove the duplicates:

		![L E F T J O I N No Dup](Images/LEFT_JOIN_NoDup.png)

	2) Multiple `LEFT JOIN`
		
		Consider the following script:
		```SQL
		SELECT DISTINCT
				member.username                AS "User Name",
				member.balance                 AS "Remaining Balance",
				member.id                      AS "Member User ID"
				#fm.user_id		       AS "Funds User ID"
		FROM    fund_modifications AS fm
				LEFT JOIN member AS m
					   ON fm.user_id = m.id  
				LEFT JOIN employee AS e 
					   ON m.account_manager = e.id
		WHERE   fm.type = "Impressions"
				AND member.username IS NOT NULL;
		```		
		The first `LEFT JOIN`, combined with `IS NOT NULL` (remember that `LEFT JOIN` actually returns **ALL** records of `fund_modifications` but some of them contain `NULL` for the `member` part if they don't match the `LEFT JOIN`), returns the rows in table `fund_modifications` that has a matching `user_id` with `id` in table `member`.

		The second `LEFT JOIN`, takes the result of first one, joins with table `employee`, contains the same number of rows, if **not filtered** and there is mismatch then there will be `NULL` values.

- **DISTINCT**
	
	Used with `SELECT`
	```SQL
	SELECT DISTINCT speed, ram 
	FROM PC;
	```
	It will *only* look at `speed` and `ram` when checking duplicates.

- **GROUP BY**

	The common error is that it only returns the first record:

	https://stackoverflow.com/questions/10830660/mysql-group-by-returns-only-first-row

	```SQL
	SELECT fm.employee_id AS "Employee ID", fm.new_balance AS "New Balance"
	FROM fund_modifications AS fm
	WHERE fm.employee_id = 777
	GROUP BY fm.employee_id
	```
	Here we want to display all `New Balance` of `Employee ID` that is 777. However the only record returned is:

	![G R O U P B Y 00](Images/GROUP_BY_00.png)

	`GROUP BY` will only display the first record encountered if there are some columns in `SELECT` that are **NOT** included in it. In our case `fm.new_balance` is in `SELECT` but not in `GROUP_BY`, thus the problem.

	The correct answer:
	```SQL
	SELECT fm.employee_id AS "Employee ID", fm.new_balance AS "New Balance"
	FROM fund_modifications AS fm
	WHERE fm.employee_id = 777
	GROUP BY fm.employee_id, fm.new_balance
	```
	![G R O U P B Y 01](Images/GROUP_BY_01.png)


	Another example that shows grouping by `GROUP BY`:
	```SQL
	SELECT fm.employee_id AS "Employee ID", fm.new_balance AS "New Balance"
	FROM fund_modifications AS fm
	WHERE fm.employee_id IN (777, 862)
	GROUP BY fm.employee_id, fm.new_balance
	```

	![G R O U P B Y 02](Images/GROUP_BY_02.png)

- **HAVING**

	Use *Having* with aggregate functions:

	Example: Find the makers producing PCs but not Laptops
	>The database scheme consists of four tables:
	
	>Product(maker, model, type)
   
	>PC(code, model, speed, ram, hd, cd, price)
	
	>Laptop(code, model, speed, ram, hd, screen, price)
	
	>Printer(code, model, color, type, price)

	We can use the following query:

	```SQL
	SELECT Product.maker
	FROM Product
	GROUP BY Product.maker
	HAVING SUM(IF(Product.Type = 'PC', 1, 0)) > 0
	AND SUM(IF(Product.Type = 'Laptop', 1, 0)) = 0
	```
	The important part is to use `SUM` + `IF` to count the number of PC or Laptop for a specific maker. And we must use `HAVING` for aggregate functions.

- **MAX** and other aggregate functions

1) One thing we want to do is to get, say, the Maximum of a column:

	Example:
    
	Find the printer models having the highest price. Result set: model, price.

	Solution:
	```SQL
	SELECT Printer.model, Printer.price
	FROM Printer
	WHERE Printer.price = 
	(
		SELECT MAX(Printer.price)
		From Printer
	)	
	```

	The trick is to realize that
	```SQL
	WHERE Printer.price = MAX(Printer.price)
	```
	doesn't work, and we have to use a second `SELECT`.

2) More advanced example: 
	
	![M A X 00](Images/MAX_00.png)

	I need to select each distinct home holding the maximum value of datetime.

	Result should be:
	
	![M A X 01](Images/MAX_01.png)

	So we want to get the *latest* `datettime` for each `home`.

	Solution:

	```SQL
	SELECT tt.*
	FROM topten tt
	INNER JOIN
		(SELECT home, MAX(datetime) AS MaxDateTime
		FROM topten
		GROUP BY home) groupedtt 
	ON tt.home = groupedtt.home 
	AND tt.datetime = groupedtt.MaxDateTime
	```

- Use **LENGTH()** and **REPLACE()** to check the number of special characters

	Find all ship names consisting of three or more words (e.g., King George V).

	Solution:
	```SQL
	SELECT name
	FROM Ships
	WHERE LENGTH(name) - LENGTH(REPLACE(name, ' ', '')) >= 2
	```

	The trick is to use `REPLACE()` to remove the spaces, and then use `LENGTH()` to calculate the lengths and then do a comparison. This is similar to what we do in Excel.

- Use **LOCATE()** to check if sub_string is in master_string

  Find the capital and the name where the capital includes the name of the country.

  Solution:
  ```SQL
  SELECT capital, name
  FROM world
  WHERE LOCATE(name, capital)>0 
  ```

  Bonus: Let's say, in addition of the previous question, we also want to filter out countries that have the same names as their capitals.

  Solution:
  ```SQL
  SELECT capital, name
  FROM world
  WHERE LOCATE(name, capital)>0
		AND CHAR_LENGTH(name) < CHAR_LENGTH(capital)  
  ```

- Use **REPLACE()** to replace substring with another string

  There is not much to say here, just memorize the grammar:

  ```SQL
  REPLACE(master_string, sub_string, another_string)
  ```  

  For example, this removes *name* from *capital*

  ```SQL
  REPLACE(capital, name, '')
  ```

- Notes on **Nested SELECT**

1. We can use a sub `SELECT`, **if it returns a single value**, in the parent `SELECT` (not after `FROM` or `IN`):

  **Ex**
  Show the population of China as a multiple of the population of the United Kingdom:

  We need to get the population of UK as the denominator, and since it's just one value, we can directly put it in the `SELECT`:

  ```SQL
  SELECT ROUND(population/(SELECT population FROM world WHERE name = 'United Kingdom'))
  FROM world
  WHERE name = 'China'
  ```

2. We can use a sub `SELECT` after `IN` if we need to limit the conditions of `WHERE`:

  **Ex**
  List each country and its continent in the same continent as 'Brazil' or 'Mexico'.

  So we need to grab the continent of both countries and apparently we need to put a sub query after `WHERE` for continent:

  ```SQL
  SELECT name, continent FROM world
  WHERE continent IN
	(SELECT continent FROM world
	 WHERE name IN ('Brazil', 'Mexico'))
  ```

- **NON-EQUI JOINS**

1. We can use Non-equi self inner joins to **locate duplicate records**. For example, I'd like to get potential duplicates in `Person` table:

```SQL
SELECT 
	P1.BusinessEntityID AS "ID",
	P1.FirstName AS "First Name",
	P1.LastName AS "Last Name"
FROM
	Person.Person AS P1
	INNER JOIN Person.Person AS P2
		ON P1.BusinessEntityID <> P2.BusinessEntityID
			AND P1.FirstName = P2.FirstName
			AND P1.LastName = P2.LastName
```
Because `BusinessEntityID` is the Primary Key, there is no duplicate on this field. However, we could have same FirstName and LastName.

![Self Join Duplicates](Images/SQL_Scenario/self_join_duplicates.png)

2. We can also user non-equi self joins to compare each record with others. For example, I'd like to see a list of **pairs of Territories** whose 2011 Sales are within [-50,000, 50,000] for each other.

  So first we need to grab the 2011 total sales:

```SQL
SELECT
	SOH.TerritoryID AS 'Territory',
	SUM
	(
	  CASE WHEN SOH.OrderDate <= '2011-12-31' THEN SOH.SubTotal ELSE 0 END
	) AS '2011 Sales'
FROM
	Sales.SalesOrderHeader AS SOH
GROUP BY SOH.TerritoryID
```

  Then use a self inner join to do the comparison:

```SQL
SELECT 
	Total1.Territory AS 'Territory 1',
	Total2.Territory AS 'Territory 2',
	Total1.[2011 Sales] AS 'T1 2011 Sales',
	Total2.[2011 Sales] AS 'T2 2011 Sales'
FROM
	(
		SELECT
			SOH.TerritoryID AS 'Territory',
			SUM
			(
				CASE WHEN SOH.OrderDate <= '2011-12-31' THEN SOH.SubTotal ELSE 0 END
			) AS '2011 Sales'
		FROM
			Sales.SalesOrderHeader AS SOH
		GROUP BY SOH.TerritoryID
	) AS Total1
	INNER JOIN
	(
		SELECT
			SOH.TerritoryID AS 'Territory',
			SUM(
				CASE WHEN SOH.OrderDate <= '2011-12-31' THEN SOH.SubTotal ELSE 0 END
				) AS '2011 Sales'
		FROM
			Sales.SalesOrderHeader AS SOH
		GROUP BY SOH.TerritoryID
	) AS Total2
	ON Total1.[2011 Sales] BETWEEN Total2.[2011 Sales] - 500000 AND Total2.[2011 Sales] + 500000
		AND Total1.Territory <> Total2.Territory
ORDER BY Total1.Territory ASC
```

  The Self-Join excludes self-comparison by using `Total1.Territory <> Total2.Territory', and the result is:

![Self Join Comparison Each Other](Images/SQL_Scenario/self_join_comparison_each_other.png)

- **NOT LIKE**

We need to be careful about using `NOT LIKE` in `WHERE` clause, because it automatically removes all `NULL`.

Let's say we have a column `member.email` with a lot of NULLs, the following `WHERE` clause will automatically remove all NULLs:

```SQL
WHERE member.email NOT LIKE '%mindgeek%'
```

On face, it only removes all `member.email` that contains `mindgeek`. Actually it also removes all NULLs.

- **AND OR precedence**

AND has higher precedence than OR, this can be annoying at times.

Let's say I want to make sure the selection satisfies the following conditions **at the same time**:

```SQL
gentry.game_id = 86886
AND gentry.ordered_time >= '2018-11-20'
AND gentry.status <> 'add'
```
I also want to satisfy **either** of the following conditions:

```SQL
//	Condition 1
member.email NOT LIKE '%test%' 
AND member.email NOT LIKE '%mindgeek'
```

```SQL
//	Condition 2
member.email IS NULL
member.email
```
The wrong way to write the query:

```SQL
WHERE 
	gentry.game_id = 86886
	AND gentry.ordered_time >= '2018-11-20'
	AND gentry.status <> 'add'
	member.email NOT LIKE '%mindgeek%'
	AND member.email NOT LIKE '%menbang%'
	AND member.email NOT LIKE '%nutakuctest%'
	OR member.email IS NULL)
```
Because it actually means either `member.email IS NULL` or all those `AND` combined.

The right way to write the query:

```SQL
WHERE 
	gentry.game_id = 86886
	AND gentry.ordered_time >= '2018-11-20'
	AND gentry.status <> 'add'
	AND ((
			member.email NOT LIKE '%mindgeek%'
			AND member.email NOT LIKE '%menbang%'
			AND member.email NOT LIKE '%nutakuctest%'
		)
	OR member.email IS NULL)
```

- **NULL Culprit**

Let's keep it short. Considering the following expression:

```SQL
NOT 22 IN (1, 2, 3, 44, 567)
```
This will return `TRUE`

However, if we have a `NULL` in the list:

```SQL
NOT 22 IN (1, 2, 3, 44, 567, NULL)
```

This will be evaluated to:
```SQL
NOT (22=1, 22=2, 22=3, 22=4, 22=5, 22=NULL)
```

And further evaluated as:

```SQL
NOT (FALSE OR FALSE OR FALSE OR FALSE OR FALSE OR UNKNOWN)
```

And finally:

`NOT UNKNOWN`, because `FALSE OR UNKNOWN` evaluates to `UNKNOWN`

The tricky part is: `NOT UNKNOWN` is still `UNKNOWN`

So that whenver you write a query with something like this:

```SQL
WHERE 
  table.column NOT IN
  (
	SELECT
	  table.column
	FROM table
	WHERE --some predicate
  )
```

And it returns nothing, you will want to check if the sub query returns some NULL value. One of them is going to be trouble.

You can either explicitly exclude `NULL` from the query:

```SQL
WHERE 
  table.column NOT IN
  (
	SELECT
	  table.column
	FROM table
	WHERE --some predicate
	  AND IS NOT NULL
  )
```

Or, use `EXISTS`, because it always returns `TRUE` or `FALSE`:

```SQL
WHERE 
  table.column NOT EXISTS
  (
	SELECT
	  table.column
	FROM table
	WHERE --some predicate
  )
```
