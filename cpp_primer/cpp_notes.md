### C++ Notes

#### Scenario 1: Read text file into a std::string

  ```C++
  #include <string>
  #include <iostream>
  #include <fstream>
  #include <sstream>

  int LoadSource(std::string source, std::vector<std::string>& output)
  {
	std::ifstream sourceFile(source);
	if (sourceFile.fail())
	{
		std::cout << "Failed to load Source file!" << std::endl;
		return -1;
	}

	std::stringstream buffer;
	buffer << sourceFile.rdbuf();
	std::string sourceCode(buffer.str());

	std::cout << sourceCode << std::endl;
	}
  ```

  The key is that we can read the text file into a `std::stringstream` buffer and then construct a `std::string` from it.