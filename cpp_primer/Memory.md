### Dynamic Memory
- shared_ptr
  - Counts (Three rules)
    - When you **copy** a ```std::shared_ptr```, the count increases by 1
	```
	std::shared_ptr<int> p(q);	//	Increment the count in q
	```
	- When you **assign**, the count of asignee decreases by 1, while the count of assigner increases by 1
    ```
	std::shared_ptr<int> p = std::make_shared<int>(100);
	//	q is another shared_ptr pointing to somewhere else
	q = p;		//	count of q decreases by 1, count of p increases by 1
	```
	- When you **destroy**, the count decreases by 1
      - This is done through the destrcutor of the ```std::shared_ptr``` class

  - Stuff learned from impelementing the ```cStrBlob``` class
    - Must use a ```std::shared_ptr``` (or any other kind of pointer) to allow multiple copies to share the same std::vector<std::string>
      - If not pointer, then what if one of the objects gets destroyed?
    - Can use ```std::make_shared``` to initialize the ```std::shared_ptr```
    - Recommend to implement a ```const``` version of ```front()``` and ```back()```
    - ```typedef``` is very useful
    - Use ```size_type``` instead of ```int```

  - You can define your own *deleter* and *allocator*, maybe we will talk about it later.
  - The Control block:
    - Each object (that std::shared_ptrs point to) has a **control block**
    - Control block consists of:
      - Reference count
      - Weak count (related to *Weak pointer*)
      - Cutom deleter/allocator, etc.

  - **We must take care that there is only one control block for one object**
    - Summary of ways to create a ```std::shared_ptr```
      - From ```std::make_shared```, a control block is also created. In this case we expect that this is the first ```std::shared_ptr``` so it's natual to create a new control block
      - From another ```std::shared_ptr```, this is usually from assignment and copy. No new control block is to be created.
      - From a ```std::unique_ptr```. Natually a control block is to be created becasue no such structure is needed for a ```std::unique_ptr```
      - From a raw pointer. This is the most error prone method. Consider this:
		```
		int i = 1024;
		int* pi = &i;		// A raw pointer
		std::shared_ptr<int> spi1(pi);	// Calls a ctor that creates a shared_ptr from a raw pointer
		std::shared_ptr<int> spi2(pi);	// Calls a ctor that creates a shared_ptr from a raw pointer
		//	Do something with the pointers
		}	//	Both go out of scope
		```
		Now the object (i.e. the integer i) has **two** control blocks. Whence spi1 goes out of scope, because the refence count of that control block is reduced to 0, object is released.

		However, spi2 is still pointing to object and its control block has no idea what just happened. So it also tries to release the object once spi2 goes out of scope.

		**ACCESS VIOLATED**

    - Alternative way to create ```shared_ptr``` from raw pointers
      - Create the first one as usual:
		```
		std::shared_ptr<int> spi1(pi);
		```
 	  - **Create the second one with a copy constructor:**
		```
		std::shared_ptr<int> spi2(spi1);
		``` 
		In this way we make sure that there is only one control block.

    - Be aware of returning *this* pointer
		```
		//	We use a ```vector``` of ```shared_ptr```
		std::vector<std::shared_ptr<Foo>> processedFoos;
		
		class Foo
		{
		public:
			void process();
		};

		void Foo::process()
		{
			// Process...
			processedFoos.emplace_back(this);	// Returning a *this* pointer
		}
		```
		Recall that passing a pointer is passing by value, so equivalently we are creating a ```shared_ptr``` from a raw pointer

		Now if there are other ```shared_ptr``` pointing to this ```Widget```, then you are looking at the gate to hell.

	- STL provides a weird method to safely create a ```shared_ptr``` from a *this* pointer:
		```
		class Foo : public std::enable_shared_from_this<Foo>	// CRTP
		{
		public:
			void process();
		};
		void Foo::process()
		{
			// Process...
			processedFoos.emplace_back(shared_from_this);	// Safer
		}
		```
    - Usually ```std::shared_ptr``` can only be initiated by other pointers that point to dynamic memory because ```std::shared_ptr``` uses *delete* to release memory
	  
	  However, we can also use other pointers (e.g. pointing to an integer, not *new*), but we need a customer *deleter*

  - Custom deletor
    - You need a custom deleter if you bind a ```std::shared_ptr``` to an object that is not on the heap.
    - E.g. let's say you have a code base which is used by C programmers, then ```struct``` is your only option. You do nothave the luxury of ctor or dtor.
	  
      Now assume that you have a ```GetFile``` function that opens a file:
	```
	void Process(/* some parameters */)
	{
		cFile RawFile = GetFile(/* some arguments */);
		// Process
	}
	```
	There are two problems:

    1) We did not close the file
    2) Even if we did write a ```CloseFile``` before it went out of scope, exceptions during runtime may deny it from working
    
    We can use a ```std::shared_ptr``` with Custom Deletor to solve the problems:
	```
	void Process(/* some parameters */)
	{
		cFile RawFile = GetFile(/* some arguments */);
		std::shared_ptr<cFile> pRawFile(&RawFile, CloseFile);
		// Process
	}
	```
	In which ```&RawFile``` menas that the ```std::shared_ptr``` reads a temporary raw pointer and ```CloseFile``` is the function to close the file even if an exception occurs
	```
	void CloseFile(cFile* pf)
	{
		//	Close file
	}
	```
	The only requirement is that the custom deletor **only takes a single argument of type cFile\***

- Smart pointers support ```.``` dereference and ```->``` dereference.
  - The ```.``` dereference simply returns the functionalities of the smart pointer, e.g. ```get()```
  - While the ```->``` dereference exposes the functionalities of the wrapped raw pointer
  - If the raw pointer points to an incomplete type (e.g. ```SDL_Window```) then ```->``` is not allowed

- Difference between ```std::shared_ptr``` and ```std::unique_ptr``` in default constructor
  - TBH I don't fully get the problem, but let's say we have a ```std::unique_ptr```:
	```
	std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> m_Window;
	```
	First thing is that we must provide the customer deleter when we define it.

	For ```std::shared_ptr```, we can delay injecting the deleter until initialization:
	```
	std::unordered_map<std::string, std::shared_ptr<SDL_Texture>> m_TextureMap;
	// Initialization
	
	```

  - You also need to initialize the smart pointers in a ctor, could be a non-default, or delegate a default one that does the job:
	```
	cGraphics::cGraphics(int xWin, int yWin, std::string texid, std::string texres) : cGraphics()
	// cGraphics() does the job
	cGraphics() : m_Window(nullptr, SDL_DestroyWindow), m_Renderer(nullptr, SDL_DestroyRenderer) {}
	```
	However it seems to OK not to null initialize the ```std::shared_ptr```. I'm not sure about this though.

- get() actually gives you the 
