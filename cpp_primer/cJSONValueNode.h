#pragma once

#include <string>
#include "cJSONNode.h"

class JSONValueNode : public JSONNode
{
public:
	JSONValueNode(const std::string& v, std::string k) : JSONNode(node_type::jValue, k)
	{
		jsonValue = v;
	}
	std::string GetAsString() const { return jsonValue; }
	long GetAsInteger() const;
	double GetAsDouble() const;
private:
	std::string jsonValue;
};