#include "cJSONArrayNode.h"

void JSONArrayNode::push_back(std::shared_ptr<JSONNode> node)
{
	jsonArray.push_back(node);
}

const std::shared_ptr<JSONNode>& JSONArrayNode::operator[](int index)
{
	return jsonArray[index];
}