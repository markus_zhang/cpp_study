## Building the XASM Assembly

### How a simple assembler works

#### **Steps to convert ASM to machine code**

  1. Reduce each instruction *mnemonic* to *opcode* based on a **look-up table**.

  2. Convert *variables* and *arrays* to **relative stack indices**, based on the **scope** in which they reside.

  3. Record each line *label*'s index within the instruction stream and replace all references to these labels (e.g. `JMP` or `Call`) with those indices.

  4. Remove whitespaces, commas and comments. Reduce everything to binary form as oppsed to ASCII.

  5. Write the binary form into an executable XVM file.

#### **Assembling Instructions**

  The assembler needs to **map** mnemonics to opcodes, with the help of a lookup table.

  The easiest way to implement the *lookup table* is to use a C++ `std::vector<std::string>`, for example:

  ```
  Mov <- position 0
  Add
  Sub
  // Other instructions
  ```

  So that opcode `0` represents mnemonics `Mov`, and etc. Then we will simply loop through the `vector` to get indices. This is definitely primitive, but remember that **compilation is not runtime** so you can take however long you want without affecting the games.

  Another way is to use a **Hash Table**, equally easy to implement. After all we only have a few dozens of instructions and either way it's going to be fast enough.

  Now the trickier part is Operand. An instruction can **NOT** take all types of operands, as we already know, so we need a way to tell the assembler about which type it can take and which type it cannot.

  We use the **operand list** to store such information. This list contains a `vector` of **bit vectors**. A sample of bit vector looks like this:

  ![Chap09 00 Operand List](Images/Script/Chap09_00_Operand_List.png)

  You can see that each bit is actually a flag, with `0` means that the operand cannot be of this type, and `1` means that it can.

  `Mov  A, B`

  The `Mov` instruction contains two operands, in which `A` can **only** be memory reference and should be set as `100000` while B will be set as `111100`.

  More details about operands will be revealed in the next two sections.

#### **Assembling Variables**

  Recall from last chapter that variables are simply a **symbolic name** for a relative stack index. Bascially we can find them by indexing from the top of the **stack frame**. For the **Globals** we pick them up from the bottom of the runtime stack with positive indices. Note that we start from 0 if we count from the bottom and -1 if we start from the top.

  ![Chap09 01 Stack Indexing](Images/Script/Chap09_01_Stack_Indexing.png)

  Because of the nature of the stack, the **active stackframe** is always at the top of the runtime stack. If it makes another function call, a new stack frame is pushed on top of it.

  Now we need to picture the *stack frame* into more details. Recall that from the top it looks like this:

  ```
  Local Variables
  Return Address
  Passed Parameters
  ```

  **Example 1:**

  We have the following function calls:

  ```
  Func0(X, Y, Z);
  Func1(W);
  Func2(Param0, Param1);
  ```

  The stack frames should look like this, assuming no local variables:

  ```
  Return Address  <- Func2() stack frame
  Param1
  Param0
  Return Address  <- Func1() stack frame
  W
  Return Address  <- Func0() stack frame
  Z
  Y
  X
  ```

  **Example 2**

  Assuming we have the following function:

  ```
  Func MyFunc (Param0, Param1)
  {
    var X;
    var Y;
    var Z;
    var LocalArray[3];
  }
  ```

  The stack frame looks like this:

  ```
  X               <- -2         
  Y               <- -3
  Z               <- -4
  LocalArray[0]   <- -5
  LocalArray[1]   <- -6
  LocalArray[2]   <- -7
  Return Address  <- -8
  Param1          <- -9
  Param0          <- -10
  ```

  Note that the stack frame **starts from -2** instead of -1, this small discrepancy with previous sections will be explained later in the next chapter. For now we only need to remember that we will actually push **an extra element** after pushing the whole stack frame.

  We are not done yet. COnsider the following instruction:

  `Mov  X, 3`

  Assuming X is at index -2 (as in the last example), and the opcode for `Mov` is 0, the whole line becomes:

  `0 -2 3`

  However the assembler doesn't know that `-2` is **actually a stack index** and `3` is a literal integer. We need to assign some values to each type and put them in front of the operands.

  Assuming `0` means stack index and `1` means integer literal, the instruction becomes:

  `0 0 -2 1 3`

  Last but not least, **Global variables** are put at the bottom of the stack.

#### **Assembling Operands**

  List of Operand types:

  ![Chap09 02 Operand Type](Images/Script/Chap09_02_Operand_Type.png)

  It doesn't matter which value we assign to each type, as long as the assembler knows that and there is no duplicate.

  **One note about arrays:**

  *Array with Literal Index* means that the assembler knows at which position the specific element resides in the array.

  `Mov X, MyArray[2]`

  The assembler **knows** that this instruction copies the value of the third element of `Myarray` to `X`. Because it knows the **Array Base Address** of `MyArray`, it simply substract 2 from this base address and reaches the **absolute stack index** of this specific element.

  ```
  MyArray[0]  <- Array Base Address
  MyArray[1]
  MyArray[2]
  ```

  In contrast, *Array with variable Index* means that the assembler has no idea about the position of the element at compilation time, and can only resolve it on runtime.

  Now the assembler knows **which type** each operand is, we need to tell it **how many** operands each instruction has. Otherwise it would lose itself in a stream of numbers and strings.

  An easy but not-so-elegant fix is to simply **insert** another number indicating the number of operands **between the opcode and the first opearnd**. Thus the line of instruction achieves full status:

  >Opcode + # of Operands + Type of 1st Operand + 1st Operand + Type of 2nd Operand + 2nd Operand + ...

  `Mov MyVar, 99`

  Can be evaluated as:

  |Opcode|# of Operands|Type of 1st|1st|Type of 2nd|2nd|
  |------|-------------|-----------|---|-----------|---|
  | Mov  |             |           |MyVar|         |99 |
  | 0    |     2       |    3      | -8  |   0     |99 |

  Note that we assume that `MyVar` has an **absolute stack index** of -8 on the stack. Remember that variables are **always represented by its index on the stack**.

  So the instruction becomes:

  `0 2 3 -8 0 99`

#### **Assembling String Literals**

  Remember how we assemble integer and float literals? Yes we embed them into the instruction stream directly. But we are **not** going to do the same for strings, because it's going to be too cumbersome if we have large strings.

  Instead we use a *string table* to store all strings. We will scan the source file to put all of the strings into a `std::vector<std::string>` or something similar, then we will **use the index to subsititue the string** in the source file.

  How do we let the assembler know that `4` means the fifth element of the string table, not an integer literal, not the index of some variable? Remember that we need to assign a *type-value* to each operand? That's the trick to make it happen.

#### **Assembling Jumps and Function Calls**

  The family of `Jump` instructions usually has a ``*Line Lable* following it. *Function names* do the same thing, plus they also assign a different **scope** and **stack frame**.

  We will again use two-pass assembling to deal with the problem. Please note that function calls are **much more complex** than jumping to labels, becauseit carries some extra baggage including **scope** and **parameters** and **return value**. This section is just a conceptual overview.

  **First pass**

  1) Scan the source file to gather index of each instruction. This has nothing to do with scopes (we are not there yet), and will just assign a sequential number to each **non-directive** line.

      Remember that **directives** are only meaningful for humans and will be removed from the assembled code ultimately, so there is no sense to keep them here. Labels, comments and whitespaces are not counted as well.

  2) In the middle of the first pass, whenever we find a *line label*, we will store the index of its **next instruction** into a data structure, with the label as the key. This is to build up the **label table**.

  **Second pass**

  1) The second pass converts mnemonics to opcode (and other stuffs), and most related to this section, it reminds the VM by **putting a label operand type** whenever it sees a label operand, and converts the operand into index in the **label table**.

      This is called *resolving* the label. My thought is that *maybe* we can also use the opportunity to calculate the # of references of each label and get rid of those with 0 reference. If they are used in the future we can pick them up with a new scan.

  **Function Calls**

  Function call is a similar but more complicated matter. In addition to keying in the function name with the index of next instruction -- just like label solving, we also need to record the **return address**, **parameters**, **local variables**, i.e. to build the whole *stack frame*.

### XASM Overview

#### **Memory Management**

  All in all, we will keep the whole script file in memory, instead of reading in chunks.

#### **Input: Structure of an XVM Assembly Script**

  How do we deal with source files that have a variety of styles and formats?

- **Directives**
 
  Directives are simply directions, and will not be reduced to (virtual) machine code.

  `SetStackSize` may take parameters of positive **integer** or 0, in later case it will assume the default value of 1024. If the programmer passes `0` to `SetStackSize`, it also indicates default stack size. In fact, we don't even need to decalre stack size if we only need the default size.

  Most importantly, `SetStackSize` may **only appear once** in a single script file.
  >SetStackSize 1024  

  **Func**

  The center piece of the XVM Assembly language. Except for some directives, e.g. `Var`, almost everything in XVM Assembly must reside in a function.

  As in `C++`, we define a `_main` function for XVM Assembly:

  ```
  Func _main
  {
    ;Code
  }
  ```

  The `_main` function serves as the entry point for the whole script. And XVM Assembler needs to set a flag about **whether there is a `_main` function in the source file**, because function names are just for humans, and will be reduce to indices on the **function table** when passed to the virtual machine.
  
  The assembler needs to check that proper function syntax is followed and the same function cannot appear more than once.

  Functions also cannot be nested (although I need this is a good feature to support in the future).

  Last but not least, `Ret` is not mandatory. Regardless of whether the user provides a `Ret` or not, the assembler will add a default `Ret` at the end of the function.

  **Var and Var[]**

  This is one of the directives that **can** exist in and out of any scope. Those outside are **globals** and insiders are **locals**.

  We do not support declaration of variables on the same line, so you have to declare them one by one, on different lines.

  We can declare two variables with the **same name** only if they are not in **the same or overlapping scopes**. For example, this is legal:

  ```
  Func A
  {
    Var X;
  }
  Func B
  {
    Var X;
  }
  ```

  But this is not legal: (I'm not very sure about this)

  ```
  Func A
  {
    Var X;
    Call B;
  }
  Func B
  {
    Var X;
  }
  ```

  Arrays are declared in XTremeScript like this:

  ```
  Var ArrayName[ArraySize]
  ```

  In XTremeScript, unlike XVM Assembly, `ArraySize` cannot be a variable and must be a integer literal. It's also legal to use the same name for an array as well as a variable, because it's relatively easy to parse the [] after the array declaration. So this is legal:

  ```
  Var X
  Var X[16]
  Mov X[2], X
  ```

  Variables and arrays consist of the locals of a function, and they are listed on the stack in the same order as they are declared in the function. Consider a XVM Assembly example:

  ```
  Func MyFunc
  {
    Var X
    Var Y
    Var MyArray[16]
  }
  ```

  The stack layout will be like this:

  ```
  X             <- -2 (start from -2, not -1)
  Y     
  MyArray[0]
  ...
  MyArray[15]
  Return Address
  Parameters...
  ```

  Last but not least, **forward reference** is possible is we scan and get all the variables in the first pass. However it may lead to code not readable, so we will take extra effort to **remove this feature** (yes it actually is easier to simply allow this feature instead of removing it).

  **Params**

  First thing about **Params** is that they are not *new*. When you pass a parameter to another function, that parameter must be in memory already, right? Translated to XVM Assembly, it means that every parameter is laready represented by an index on the runtime stack, **pushed by the caller function**.

  <span style="color:purple">
  However I still don't get what exactly does the VM do with parameters. The previous paragraph seems to suggest that parameters are simply another name for existing variabels already on the stack. But this would render generating the stack frame of callee function extremely impossible.
  </span>

  <span style="color:purple">

  Consider the example:
  
  ```
  Func MyFunc()
  {
    Var V1
    Var V2
    Call MyFunc2()
  }
  ;Whatever
  Func MyFunc2()
  {
    Param P1
  }
  ```
  </span>

  <span style="color:purple">
  Here is the confusion: Is P1 V1 or V2?
  </span>

  For the same reason, there is no **Global Parameter**, I mean you have to pass parameters in functions, even for _main(). For sure you can pass global variables as parameters, but the parameters themselves are inside some functions when passed.

  **We do not allow Param[]**, parameter arrays. However maybe later we can implement it, as in Visual Basic we have array of parameters.

  `Param MyParam`

  Recall that the number of parameters also affects the size of the *stack frame*. Assuming our function looks like this:

  ```
  Func MyFunc
  {
    ;Declare Parameters
    Param P1
    Param P2
    ;Local variables
    Var V1
    Var V2
    Var Array1[3]
    ;Code
    Mov Array[0] P1
    Add P2, P1
    Mov Array[1] P2
  }
  ```

  Ignore the `Code` part, the stack frame should look like this:

  ```
  V1              <- -2
  V2
  Array[0]  
  Array[1]
  Array[2]
  Return Address
  P1
  P2  
  ```

  Because the nature of the runtime stack, each stack frame will be poped after the function that generates it returns to the caller. We must make sure that **the number of parameters match whatever were pushed onto the stack**. There is no way to check against it.

  Last but not least, we recommend that you declare the parameters in **XVM Assembly** **in reverse of** the order they are **declared in XTremeScript**.

  In XTremeScript:
  `Func MyFunc(X, Y, Z)`

  It would be more beneficial to declare the parameters in rever order in the translated XVM Assembly:

  ```
  Func MyFunc
  {
    Param Z
    Param Y
    Param X
  }
  ```

  So that it will look this on stack:
  ```
  Return Address
  X
  Y
  Z
  ```

  **Identifiers**

  What is the naming convention of functions/variables/parameters? We would enforce the following two rules:

  >All identifiers must consist of letters, numbers and underscores and cannot begin with a number.

  >Each identifier is case INsensitive.

- **Instructions**

  **General format**

  This is the easy part, each instruction looks like this:

  `Mnemonic   Operand, Operand, ...`

  You can have 0~N operands, and there must be **at least one space between Mnuemonic and operands**. We also do NOT support multi-line instructions, because unlike `C`, there is no semicolon after each line, so there is no way to seperate two lines effectively.

  There is not much to say about different types as we will go back to look at it really closely. Becasue we need to teach the compiler to recognize each type.

- **Line Labels**

  `MyLable:`

- **Host API Function Calss**

  We use `CallHost` to call functions defined by host applications and exposed to scripting.

  The syntax is almost identical to `Call`, and you also push parameters onto stack and receive return value through `_RetVal`. The only difference is that we do not have a *function table* for API functions, so we have to save the function names as string in the executables so that the VM can call them in runtime.

- **_Main() Function**

  For reasons unknown, it is stated that the `_Main()` function must be appended with an `Exit` instruction at the end.

- **_RetVal Register**

  This is the register to hold return value. It exists in global scope.

- **Comments**

  The semicolon may appear anywhere in the line. Again, no multi-liner is allowed, so if you want multi lines of comments you need to put a semicolon in front of each line.

#### A complete example script

  ```
  ; Example script
  ; Demonstrates the basic layout of an XVM
  ; assembly-language script.

  ; ---- Globals ----------------------------------
  Var GlobalVar
  Var GlobalArray [ 256 ]

  ; ---- Functions --------------------------------
  ; A simple addition function
  Func MyAdd
  {
    ; Import our parameters
    Param Y
    Param X

    ; Declare local data
    Var Sum
    Mov Sum, X
    Add Sum, Y

    ; Put the result in the _RetVal register
    Mov _RetVal, Sum

    ; Remember, Ret will be automatically added
  }

  ; Just a bizarre function that does nothing in particular
  Func MyFunc
  {
    ; local data
    Var MySum

    ; Going to test the Add function, so we'll
    ; start by pushing two integer parameters.
    Push  16
    Push  32

    ; Next we make the function call itself
    Call  MyAdd

    ; finally, grab the return value from _RetVal
    Mov MySum, _RetVal

    ; Multiply MySum by 2 and store it in GlobalVar
    Mul MySum, 2
    Mov GlobalVar, MySum

    ; Set some array values
    Mov GlobalArray [ 0 ], "This"
    Mov GlobalArray [ 1 ], "is"
    Mov GlobalArray [ 2 ], "an"
    Mov GlobalArray [ 3 ], "array."
  }

  ; _Main () will be automatically executed
  Func _Main
  {
    ; Call the MyFunc test function
    Call MyFunc
  }
  ```

  <span style="color:purple">

  Things to note:

  1) Parameters are directly pushed onto stack. I'm not sure if it's the same if we are passing local variables as parameters (in this example integer literals are passed as parameters)

  2) After the `Call`, we can grab the return value directly from `_RetVal`.

  3) None of the functions has `Ret`, which is OK. Note that `MyAdd` manually moves the return value in the register.

  4) Note where are globals located and where is `_Main()` located.

  </span>

### Output: Structure of an XVM Executable

  The output is the **XTremeScript Executable**, or `.XSE` file.

#### Overview

  The `.XSE` is a binary format file, and data is writeen in the form of bytes, words and double-words. There are two exceptions: First, **floating-point** data is written directly to the file **as-is**; Next, **String** is stred as an **uncompressed, byte-for-byte** copy, preceded by a **four-byte length indicator**. Unlike C-String, it is not null-terminated.

  In general, each field of the file is prefixed by a size field.

  <span style="color:purple">
  
  This reminds me of the format of DOOM .WAD file, while each directory is prefixed with a size field.
  
  The author claims that:

  </span>

  >allows entire blocks of file to be loaded into memory very quickly by C's buffered input routines in a single call.  

  <span style="color:purple">

  From this article:

  http://www.eecs.umich.edu/courses/eecs380/HANDOUTS/cppBinaryFileIO-2.html

  And here:

  https://courses.cs.vt.edu/cs2604/fall02/binio.html#read

  It seems that we can use the following code to read binary file for any length we like, with just a parameter:

  ```C++
  char buffer[100];
  ifstream infile; 
  infile.open("Source.XS", ios::binary | ios::in);
  infile.read(buffer, 100); // Read 100 bytes
  ```

  </span>

  The following is an overview of .XSE format:

  ![Chap09 03 X S E Format](Images/Script/chap09_03_XSE_Format.png)

  Let's go through them one by one:

#### The Main Header

  The Header contains all information the VM needs to handle the executable file, with the following format:

  |    Name   |Size (Bytes)|    Description    |
  |-----------|------------|-------------------|
  | ID String |       4    |        XSE0       |
  |  Version  |       2    |     Major.Minor   |
  | Stack Size|       4    |Set by SetStackSize|
  | Global Data Size| 4    |Total size of global data|
  | Is _Main() Present|  1 | 1 if present, 0 if not |
  | _Main() Index | 4      |Index position of _Main()|

  **Version** is set up as Major.Minor, for example the current version is **0.4** so it should be `00 04`.

  **Global Data Size**, I'd assume that we need to calculate the total size and put it here?

  Here is a sample header:

  ![Chap09 04 Sample Header](Images/Script/chap09_04_Sample_Header.png)

  In this sample **Stack Size** would be `00 04 00 00`, which is `0x0400` in Hex and 1024 in Int. We have a total of 12 bytes of **Global Data**. _Main() presents and it resides at index 4 on the **function table**.

#### Instruction Stream

  The instruction stream contains its own header structure, as well as the actual stream data.

  The stream data consists of instructions of the following structure, as we already learned in previous lessions:

  ```
  OpCode + Operand Stream
                  ^
            Operand Size + Operands
                              ^
                          Operand Type + Operand Data
  ```

  First let's talk about the **header**, it's just a four-byte integer that specifies the number of instructions in the stream.

  We then goes down to individual **Instruction Structure**, which consists of a 2-byte **OpCode** as well as the **Operand Stream**.

  Going deeper into the **Operand Stream** we find a one-byte **Size** that describes the number of operands, as well as individual **Operand Structure**.

  The **Operand Structure** is the last structure and contains a 1-byte **Type** indicator and the actual **Operand Data**.

  The whole scheme can be decribed by the following table:

  |OpCode|Operand Count|Operand Type|Operand Data|Operand Type|Operand Data|
      |------|-------------|------------|------------|--------------|-------------|
  |   0  |    2        |    3       |     -4     |    0       |     2      |

  Maybe it's better to show as a pic...

  ![Chap09 05 Instruction Stream](Images/Script/chap09_05_Instruction_Stream.png)

  Of course the above instructions all have two operands, which is not the case for some instructions.

- **Operand Type**

  This is perhaps the center piece of the puzzle: The VM must be able to identify the **type** of the operands for executing and checking each instruction.

  The actual types are simple, we already know pretty much all of them on our way here:

  |Code|Name|
  |----|----|
  |  0 |Integer Literal|
  |  1 |Floating-Point Literal|
  |  2 |<span style="color:blue">String Literal Index</span>|
  |  3 |<span style="color:blue">Absolute Stack Index</span>|
  |  4 |<span style="color:blue">Relative Stack Index</span>|
  |  5 |<span style="color:blue">Instruction Index</span>|
  |  6 |<span style="color:blue">Function Index</span>|
  |  7 |<span style="color:blue">Host API Call Index</span>|
  |  8 |Register|

  Note that **Instruction Index** is used for resolving Labels. **Absolute Stack Index** is easy, but **Relative Stack Index** involves some indirection. Remember that in XVM Assembly sometimes we can use a *variable* as an array index? Yeah becasue that variable cannot be resolved in compiler time, it has to wait for runtime. Thus the only thing we can do is to feed the **base index** of the array and at runtime VM will **offset** the base address with the value of variable to get the **Absolute Stack Index**.

  Basically, for array element with variable X as index, we need to give VM 1) the **base index** of the array and 2) the **index** of X, so that VM can calculate the **absolute stack index** of that element at runtime.

  In summary, the VM **only cares about the Absolute Index Address** becasue that's the only thing it needs to resolve pretty much everything.

#### The String Table

  The string table is simple.

  <span style="color:blue">

  My understanding about these binary structures is: Always add a header about the length or number of elements ahead of the real data.

  </span>

  The String Table is no exception. There is a four-byte header for the **String Table**, which contains the number of strings the table stores. After that it comes the **String Structures**. Again, yeah you probably guessed it, each **String Structure** has its own four-byte header showing the length of the string (thus **no need for null terminator**), prefixing the real string.

  ![Chap09 06 String Table](Images/Script/chap09_06_String_Table.png)

#### Function Table

  The function table is similar to the String table, but describing the functions instead of strings.

  To be expected, the **Function Table** has its own **header**, which is a four-byte integer showing the number of functions. This is of course followed by the actual data, which is maybe an array of **Function Structure**.

  >Function Table Header + a bunch of Function Structures

  This **Function Structure** consists of the following information:

  |Name of Field|Size of Field|Description|
  |-------------|-------------|-----------|
  |Entry Point  |4 Bytes      |Index of first instruction|
  |Parameter Count| 1 Byte    |Number of parameters|
  |Local Data Size| 4 Bytes   |Total size of local var|

  The **Function Structure** of each function is used by the VM to compose its *stack frame*.

  Please note that the `_Main()` function is also in the **Function Table** and is **always stored at index zero**(means it's always the first **Function Structure**) if there is one. Recall that we can assess the existence of `_Main()` by fetching the header of `.XSE`. `_Main()` also does not accept any parameters so its **Parameter Count** field will always be zero.

  A sample **Function Table**:

  ![Chap09 07 Function Table](Images/Script/chap09_07_Function_Table.png)

#### Host API Call Table

  Recall that we must save the string of the API function name we would like to call into `.XSE`, because there is no way to get into the API. So essentially the **Host API Call Table** is exactly the same as the **String Table**. You have a four-byte header decribing the number of Strings(Host API Function Names), and followed by the **Host API Function Structure**, which simply consists of a header of four-byte showing the length of the string, and the string of API function name itself.

  **Host API Call Table** = 

  >Host API Call Table Header + a bunch of Host API Call Structure

  **Host API Call Structure** =

  >Host API Call Structure Header + String of name of API function

#### Final review of .XSE file structure

  ![Chap09 08 X S E Format2](Images/Script/chap09_08_XSE_Format2.png)

### Implementing the Assembler

  The author will cover the bottom-up method first and then use the top-down method to finish the code.

#### Basic Lexing/Parsing Theory

  What **Lexer** does first is to turn *character stream* into *lexemes stream*. **Lexemes** are *words* that the language understands:

  ![Chap09 09 Lexer Intro](Images/Script/chap09_09_Lexer_Intro.png)

  It does the following:

  **Stripping off whitespaces and comments**
  
  These are very useful to humans, but the compiler doesn't really care about them. We also want to allow users to have their own coding styles.

  After this the **Lexer** continues to converts the *Lexemes stream* into *Token stream*.

  `Mov  Sum, X`   <- Before Lexer

  `MOV SUM , X`   <- After converting to *Lexemes Stream*

  `TOKEN_TYPE_INSTR TOKEN_TYPE_IDENT TOKEN_TYPE_COMMA TOKEN_TYPE_IDENT` <- After converting to *Token Stream*

  **Tokens** are not strings, but integers (and usually `Enum`) that the VM understands to be the *type* of the lexemes. For example, `MOV` is an instruction token, `SUM` is an identifier (for a variable) token, the comma `,` is, well, a comma token, and `X` is again an identifier token.

  <span style="color:blue">

  Apparently, dealing with token *types* is useful for validating the code. For example, `MOV` as we know, allows the `Target` operand to be a memory address (index on the stack AFAIK), and the `Source` to be pretty much anything. So it's obviously a syntax error if we see a `TOKEN_TYPE_INTEGER` (assuming this is the type for integer literals) for the `Target`.

  </span>

  After the Lexer does its job, we get a bunch of tokens. Now it's the **Parser*'sjob to find the meaning of all these:

  ![Chap09 10 Parser Intro](Images/Script/chap09_10_Parser_Intro.png)

  The author takes a *brute-force* approach here, taking in **one token** at a time and trys to group several in a logically meaningful way.

  Consider the following XVM Assmbly source code:

  `Func MyFunc {`

  This is obviously afunction declaration. We cut in short just to show the idea.

  Converted to *token stream*, it has three elements:

  ```
  TOKEN_TYPE_FUNC
  TOKEN_TYPE_IDENT
  TOKEN_TYPE_OPEN_BRACKET
  ```

  Now we can use the following algorithm to validate...

  ```C++
  auto nextToken = GetNextToken();
  Switch nextToken
  {
    //  ignore other cases
    case TOKEN_TYPE_FUNC:
    {
      if (GetNextToken() != TOKEN_TYPE_IDENT)
      {
        std::cout << "Syntax Error!" << std::endl;
      }
      else
      {
        std::string funcName = GetCurrLexem();
        nextToken = GetNextToken();
        if (nextToken != TOKEN_TYPE_OPEN_BRACKET)
        {
          std::cout << "Syntax Error!" << std::endl;
        }
      }
      break;
    }
  }
  ```

  Note that **Tokens** and **Lexems** are two different items. A **Token** is an id, usually an `int`, to represent the *type* of the **Lexems**, while the **Lexem** is a `std::string` containing the real meat.

#### Basic String Processing

  First batch of programs:

  **Single Character Classification**

  These are five programs that identifies the type of incoming characters.

  ```C++
  bool IsCharNumeric(char ch) {} // Is ch numeric?
  bool IsCharAlphabetic(char ch)  {} // Is ch Alphabetic?
  bool IsCharWhitespace(char ch)  {} // Is ch whitespace or tab?
  bool IsCharIdent(char ch) {} // Is ch Identifier?
  bool IsCharDelim(char ch) {} // Is cha demiliter?
  ```

  **String Classification**

  These are four functions that identifies the type of incoming string.

  ```C++
  bool IsStringInt(std::string str) {} // Integer?
  bool IsStringFloat(std::string str) {} // Float?
  bool IsStringWhitespace(std::string str) {} // Whitespace?
  bool IsStringIdent(std::string str) {} // Identifier?
  ```

#### Assembler's Framework

  The author uses Linked List to maintain tables (String Table, Function Table, etc.). For our purpose a `std::vector` is better for non-dictionary arrays, and `std::unordered_map` is better for dictionary arrays.

  **Step by Step Guide**

  Step 1: **Load designated source file**

  Users may name any source file as the first argument and name output file as the second argument:

  `XASM MySource.XASM MyExec.MSE`

  The second argument is optional. The file extensions are also optional.

  <span style="color:blue">

  My idea is to load the source file into a `std::vector<std::string>`. 

  </span>

  Step 2: **Convert source file into special strct**

  Recall that the file structure of the .XSE file is:

  ![Chap09 08 X S E Format2](Images/Script/chap09_08_XSE_Format2.png)

  We can create a master-class to wrap all fields. For example the **Main Header** is just a 4-byte integer. Of course we also need sub-classes to wrap up **Individual Instructions**, **String Table**, **Function Table** as well as **Host API Call Table**.

  The only tricky part is how to present the *data* of each operand. We can use an `enum` to present the *type* of the operands, but we must use another variable to hold the *value* of the operands. For example, in `MOV X, 16.56`, we know that `X` is an index on the stack, and naturally we want to use an `int` to store `X`. However the other operand, `16.56`, is not an `int`, and we must use a `float` to store it.

  The problem is that we only have **one** struct for operands, and we need to store potentially **more than one** types of value in `struct _operand`.

  The solution, given by the author, is to use a `union`. 

  We have the following data that may be held in the union:

  >Stack Index, Instruction Index, Function Table Index, String Table, Host API Call Index, Integer Literal, Float Literal, String Literal, Register code.

  **Stack Index** is special. Remember that the VM only cares about **Absolute** stack index? So we need to add an *offset* in case some array elements are indexed by **variables** instead of **literal integers**.

#### Tokenizer

The tokenizer uses the following algorithm:

**Step 1:**
  Read lexeme until it reaches a delimiter

**Step 2:**
  Analyze the lexeme and return `TOKEN_TYPE_*`