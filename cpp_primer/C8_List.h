#pragma once
#include <vector>
#include "C8_IList.h"

template <typename T>
class vList : public IList<T>
{
public:
	vList() : ls(new T[DEFAULT_CAPACITY]), maxCapacity(DEFAULT_CAPACITY), size(maxCapacity) {}
	vList(const std::initializer_list<T>& ls);
	~vList() { delete[] ls; }

	T& operator[](std::size_t n);
	const T& operator[](std::size_t n) const;

	bool isEmpty();
	int getLength() const;
	bool push_back(const T& item);
	bool insert(int pos, const T& item);
	bool insert(int pos, std::initializer_list<T> itemList);
	bool remove(int pos);
	void clear();
	T getEntry(int pos) const;
	void setEntry(int pos, const T& item);
	void display();
private:
	static const int DEFAULT_CAPACITY = 0;
	T* ls;
	int size;
	int maxCapacity;
	void removeHelper(int pos);
	void resize(int newSize);
};

template <typename T>
vList<T>::vList(const std::initializer_list<T>& ls)
{
	for (auto item : ls)
	{
		push_back(item);	// push_back should take care of increasing size and maxCapacity
	}
}

template <typename T>
T& vList<T>::operator[](std::size_t n)
{
	if (n >= size)
	{
		throw "Out of range";
	}
	else
	{
		return ls[n];
	}
}

template <typename T>
const T& vList<T>::operator[](std::size_t n) const
{
	if (n >= size)
	{
		throw "Out of range";
	}
	else
	{
		return ls[n];
	}
}

template <typename T>
bool vList<T>::isEmpty()
{
	return (size == 0);
}

template <typename T>
int vList<T>::getLength() const
{
	return size;
}

template <typename T>
bool vList<T>::push_back(const T& item)
{
	if (size >= maxCapacity)
	{
		resize(maxCapacity + 1);
		ls[maxCapacity-1] = item;
		size += 1;
		return true;
	}
	return false;
}

template <typename T>
void vList<T>::resize(int newSize)
{
	if (size == newSize)
	{
		return;
	}
	else if (size < newSize)
	{
		T* newList = new T[newSize];
		maxCapacity = newSize;
		for (auto i = 0; i <= size - 1; i += 1)
		{
			newList[i] = ls[i];
		}
		delete[] ls;	// Since ls is private, it should be OK to delete this
		ls = newList;
	}
}

template <typename T>
bool vList<T>::remove(int pos)
{
	if (pos > size - 1)
	{
		throw "Out of range";
		return false;
	}
	else
	{

	}
	return true;
}

template <typename T>
void vList<T>::removeHelper(int pos)
{

}

template <typename T>
void vList<T>::clear()
{
	
}

template <typename T>
T vList<T>::getEntry(int pos) const
{
	return ls[pos];
}

template <typename T>
bool vList<T>::insert(int pos, const T& item)
{
	//	Assuming pos starts from 0, i.e. same as in array[]
	if (pos > size || pos < 0)
	{
		return false;
	}
	if (pos == size)
	{
		return (push_back(item));
	}
	else
	{
		//	Need deep copy
		T* newList = new T[size + 1];
		for (auto i = 0; i <= pos - 1; i++)
		{
			newList[i] = ls[i];
		}
		newList[pos] = item;
		for (auto i = pos; i <= size - 1; i++)
		{
			newList[i + 1] = ls[i];
		}
		delete[] ls;
		ls = newList;
		size += 1;
		return true;
	}
}

template <typename T>
bool vList<T>::insert(int pos, std::initializer_list<T> itemList)
{
	for (auto item : itemList)
	{
		if (!insert(pos, item))
		{
			throw "Insert error!";
		}
		pos += 1;
	}
}

template <typename T>
void vList<T>::setEntry(int pos, const T& item)
{
	
}

template <typename T>
void vList<T>::display()
{
	if (size <= 0)
	{
		return;
	}
	for (auto i = 0; i <= size - 1; i++)
	{
		std::cout << ls[i] << ", ";
	}
	std::cout << std::endl;
}


