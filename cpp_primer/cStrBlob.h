#pragma once

#include <vector>
#include <string>
#include <memory>
#include <exception>

class cStrBlob
{
public:
	typedef std::vector<std::string>::size_type vstr_type;
	cStrBlob() : pStrVec(std::make_shared<std::vector<std::string>>()) {}
	//	Can directly initialize with the initialize_list
	cStrBlob(std::initializer_list<std::string> list) : pStrVec(std::make_shared<std::vector<std::string>>(list)) {}
	vstr_type size() const { return pStrVec->size(); }
	bool isempty() const { return pStrVec->empty(); }

	//	Add and remove
	void push_back(const std::string& str);
	void pop_back();

	//	Access
	std::string& front();
	const std::string& front() const;
	std::string& back();
	const std::string& back() const;
private:
	std::shared_ptr<std::vector<std::string>> pStrVec;
	void check(vstr_type i, const std::string& msg) const;
};

void cStrBlob::push_back(const std::string& str)
{
	if (pStrVec == nullptr)
	{
		pStrVec = std::make_shared<std::vector<std::string>>();
	}
	
	pStrVec->push_back(str);
}

void cStrBlob::pop_back()
{
	check(0, "Back is not available");
	pStrVec->pop_back();
}

std::string& cStrBlob::front()
{
	check(0, "Front is not available");
	return pStrVec->front();
}

const std::string& cStrBlob::front() const
{
	check(0, "Front is not available");
	return pStrVec->front();
}

std::string& cStrBlob::back()
{
	check(0, "Back is not available");
	return pStrVec->back();
}

const std::string& cStrBlob::back() const
{
	check(0, "Front is not available");
	return pStrVec->back();
}

void cStrBlob::check(cStrBlob::vstr_type i, const std::string& msg) const
{
	if (i >= pStrVec->size())
	{
		throw std::out_of_range(msg);
	}
}