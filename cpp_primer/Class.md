## Class

### This Pointer
- Member functions access the object on which they were called through an extra, implicit parameter named **this**
	
	Let's say we have an object ```Goblin``` of type ```cMonster``` which has a member function ```GetID()```.
	When we call Goblin.GetID(), 
	the compiler implicitly passes the **this pointer** as if we are calling ```cMonster::GetID(&Goblin)```
- Because *this* always points to the object itself, i.e. it contains the address and so cannot be modified. It is a ```const``` pointer
- When to use **this pointer** explicitly
  - There is only one case that I found to be really useful
	```
	class Bar {
    int x;
    int y;
	public:
		Bar () : x(1), y(2) {}
		void bar (int x = 3) {
			int y = 4;
			std::cout << "x: " << x << std::endl;
			std::cout << "this->x: " << this->x << std::endl;
			std::cout << "y: " << y << std::endl;
			std::cout << "this->y: " << this->y << std::endl;
		}
	};
	```
	Must use ```this->x``` explicitly to get member ```x```, otherwise you get the argument ```x```
- **this** is of type ```cMonster* const```, i.e. a ```const``` pointer to a ```cMonster``` object.
  - Recall that we cannot bind a ```non-const``` to a ```const``` pointer.
	```
	const int* constip;
	const int consti = 100;
	int nonconsti;
	constip = &consti;
	constip = nonconsti;	//	Wrong! Cannot assign
	```
  - If we want to bind **this** pointer with a ```const```, the only way is to add ```const``` after parameter list:
	```
	std::string GetID() const;
	std::string cMonster::GetID() const
	{
		return m_ID;
	}
	```
	This is equivalent to:
	```
	std::string cMonster::GetID(const cMonster* const)
	{
		return this->m_ID;
	}
	```
  - If a ```const``` pointer/reference points to a ```const``` object, it may **only** call ```const``` member functions
	
	However, why can **this** pointer access virtually everything? Because it does not point to a ```const``` object

- We can also return **this** pointer
	```
	cMonster* Berserk();
	cMonster* cMonster::Berserk()
	{
		iHP += 10;
		dDamage *= 2;
		return this;
	}
	```
  - Usually it is used to mimic an operator, e.g. ```+=```
  - I think I saw some usage in overloading the operators becuase we need to return a lvalue

### Constructors & Destructors
- The compiler can default-initialize pointers/smart pointers to build-in types.

	However this does not mean that there will be no runtime errors.

	Consider this:
	```<C++>
	template <typename T>
	class Blob
	{
	public:
		Blob() : data(std::make_shared<std::vector<T>>()) {}
	private:
		std::shared_ptr<std::vector<T>> data;
	}
	```
	You cannot even use ```= default``` for the constructor without initializing it.

	The compiler is OK with this, but it will give you a ```nullptr```, so you must initialize it by yourself.

	For example if you want to ```push_back``` then it's a runtime error.

- Synthesized Default Constructor
  - Compiler provides a **Synthesized Default Constructor** if we do not declare one
  - Take no argument
  - Member data initialization:
    - Use in-class initializer if there is one
    - Otherwise default initialize the member
    - Some members e.g. arrays and pointers will have **undefined** value when default initialized
    - Sometimes default initialization cannot initialize members that have a class type but **without a default constructor**
	```
	class cInventory
	{
	private:
		int iNum;
		std::string strName;
	public:
		// cInventory();
		cInventory(int num, std::string name)
		{
			iNum = num;
			strName = name;
		}
	};

	class cMonster
	{
	private:
		int iX;
		int iY;
		std::string strID;
		double dDamage;
		int iHP;
		cInventory inv;
	public:
		std::string GetID() const;
		cMonster* Berserk();
	};
	```
	Note that in the above example, cInventory explicitly declares a non-default constructor to block the generation of synthesized default ctor.
	
	cMonster does not declare a ctor, so the compiler declares one for it.

	The compiler tries to default-declare ```cInventory inv```, but there is no default ctor for it. This denies the generation of ```cMonster``` 's synthesized default ctor

	Now suppose we declare a cMonster object like this:
	```cMonster Goblin```
	The compiler will immediately complains that **The default of cMonster cannot be referenced**
  - We can explicitly ask the compiler to generate a synthesized default ctor by using ```= default```
	```
	cInventory() = default;
	```	
	If ```= default``` appears outside of class definition, then it is treated as **User defined**:
	```
	cInventory::cInventory() = default;
	```

- Ctor Initializer list
  - Remember that we can use a list to initialize the members:
	```
	class cMonster
	{
	private:
		int iX;
		int iY;
		std::string strID;
		double dDamage;
		int iHP;
		cInventory inv;
	public:
		cMonster(int x, int y, std::string id, double damage, int hp) : iX(x), iY(y), strID(id), dDamage(damage), iHP(hp) {}
	```
	Notice that we do not pass a ```cInventory``` object.

	When a member is omitted from the ctor initializer list, it is **implicitly initialized using the same process as is used by the synthesized default ctor**.

- Initialization and Assignment
  - Members that are ```const``` or **references** must be initialized, i.e. cannot omit them in ctor initializer
    - First, definitely you cannot assign to a ```const```, this is illegal everywhere
    - Second, for the reference:
      - There is no null value for a reference, so you must initialize it
      - It seems that you cannot assign to it either, although I do not know the reason, check here: https://stackoverflow.com/questions/1701416/initialization-of-reference-member-requires-a-temporary-variable-c
  
  - Members are initialized in the order that **they appear in the class definition**
    - This may lead to some strange errors:
	```
	class Foo
	{
	private:
		int i;		//	i will be the first to initialize
		int j;
	public:
		Foo(int blah): j(blah), i(j)	{}
	};
	```
	It seems to be fine as we initialize ```j``` with ```blah``` and then initialize ```i``` with ```j```

	However, in reality the compiler **always** initialize ```i``` first, and this leads straight to a compiler error

- Delegating Ctor
  - Ctors that delegate the work to other Ctors:
	```
	cInventory() = default;
	cInventory(int num, std::string name)
	{
		iNum = num;
		strName = name;
	}
	
	//	Delegating ctor
	cInventory() : cInventory(10, "My Inventory") {}
	cInventory(int num = 0) : cInventory(num, "My Inventory") { std::cout << "Delegated" << std::endl; }	
	```
	You can make the default Ctor as a delegating Ctor, as the first example under the comment;

	You can also delegate delegating Ctors, as the last example shows

### Access Control and Encapsulation
- Friend
  - Add ```Freind``` before the function and class and then they are able to access the non-public members.
	```
	class cMonster
	{
	friend std::istream& load(std::string filename);
	private:
		//...
	};
	```
	load(), as a **friend function**, will be able to access the non-public members of cMonster
  - **Friend** declaration must appear in the class body, but there is no restriction of where they should be

- *Mutable* data member
  - A ```Mutable``` is **never** a ```const```.
	```
	mutable size_t access_ctr;
	```
  - We may change a ```Mutable``` even in a ```const``` object
	```
	//	inside a class
	mutable int iHP;
	void DemoMutable() const;
	//	Definition of DemoMutable()
	void cMonster::DemoMutable() const
	{
		iHP += 10;
	}
	
	//	in Main.cpp
	cMonster Goblin(10, 10, "Goblin", 2.5, 20);
	Goblin.DisplayHP();	//	Shows 20
	Goblin.DemoMutable();
	Goblin.DisplayHP();	//	Shows 30 even if DemoMutable is const
	```
	The above is even true if ```Goblin``` itself is ```const```
	```
	const cMonster Goblin(10, 10, "Goblin", 2.5, 20);
	```

- Retuning *this* pointer
  - The book talks about retuning *this* pointer in a member function that modifies some members:
	```
	cMonster& cMonster::Berserk()
	{
		iHP += 10;
		dDamage *= 2;
		return *this;
	}

	cMonster& cMonster::Teleport()
	{
		iX += 2;
		iY += 2;
		return *this;
	}
	```
	The good part is that we can do this:
	```
	cMonster Goblin(/* Usually initialization */);
	Goblin.Berserk().Teleport();
	```
	Because each function returns the object itself.

	If instead we simply use a ```void``` return type then we need to write two lines instead of two.

  - Returning *this* pointer from a ```const``` member function is tricky
    - ```const``` member function must return a ```const``` self object:
	```
	const cMonster& DisplayID() const;
	```
	However we are not able to put it into the chain action:
	```
	Goblin.DisplayID().Berserk().Teleport();
	```
	Because ```DisplayID()``` returns a ```const``` object, but ```Berserk()``` needs to modify the object
	
	Note that it's OK to put DisplayID() at the end.

    - **What if ```const``` member funtion returns void?**
	```
	const cMonster& DisplayID() const;
	cMonster& DisplayID();
	void Do_DisplayID() const;
	//	Definitions
	const cMonster& cMonster::DisplayID() const
	{
		Do_DisplayID();
		return *this;
	}

	cMonster& cMonster::DisplayID()
	{
		Do_DisplayID();
		return *this;
	}

	void cMonster::Do_DisplayID() const
	{
		std::cout << strID << std::endl;
	}	
	```
	Now if the object is a ```const```, will use the overloaded ```const``` one

- Friend Class
  - Must be declared before any private and public members
  - May only declare a member function as ```Friend```
	```
	Friend void cFile::LoadMonster(std::string def);
	```
  - Must declare for each overloaded function if you want to make them ```Friend```

### Scope
- Name lookup and Class Scope
  - For a **member function**, the compiler follows a two-phase process:
    - The member declarations are compiled
    - Function bodies are compiled only after **the entire class has been seen**
  - This can be shown in the following example:
	```
	std::string sName = "Name outside of class";
	class Foo
	{
	public:
		std::string GetName()
		{
			return sName;
		}
	private:
		std::string sName;
	};
	//	In Main.cpp
	Foo foo("Name Inside class");
	std::cout << foo.GetName() << std::endl;
	```
	Because the compiler already knew the entire class before moving on to compile the function body,

	the program will only show "Name Inside class".

### Static Class Members
- ```static``` class members are independent of any class object:
  - Think about something that is *universal* to a class and you may want to change it for each object of that class.
  - They do **not** have a *this* pointer. Because they do not belong to any individual class object.
  - ```static``` member **functions** cannot be ```const```, ```virtual``` or ```volatile```
    - The reason is, when you apply a ```const``` qualifier to a member function, you implicitly affect the *this* pointer (converts to ```const``` pointer)
    - However, for ```static``` member functions, they simply do not have *this* pointer

- Access of ```static``` member:
  - Directly with a class scope operator:
	```
	std::string version;
	version = cMonster::GetVersion();	//	static std::string GetVersion()
	```

  - Directly without a class scope operator for a member function:
	```
	class cMonster
	{
	private:
		static std::string sVersion;
	public:
		bool CmpVersion(std::string ver)	{ return ver = sVersion; }
	}
	```

  - A ```static``` member function can be called **even without an object**

- Define a ```static``` member
  - ```static``` data members are **not** initialized by ctors
  - May **not** initialize ```static``` data members inside the class
  - Must **define** and **initialize** ```static``` data members outside the class body
    - **Exception**
      - **Must** initialize ```constexpr``` ```static``` integral data members:
		```
		class cMonster
		{
		private:
			static std::string sVersion;
			//	Bad example but it's OK just to show
			static constexpr int static_fov = 5;
		};	
		```
 	  - **May** initialize **integral** type (int, char, bool, etc.) directly in declaration:
		```
		class cMonster
		{
		private:
			static bool isBeta = 0;
		};
		```
    - For other situations, **must** decalre in header file and initialize ```static``` in source file, otherwise linker complains *defined in multiple source files*