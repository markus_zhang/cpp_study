#pragma once
#include <vector>
#include "C4_Node.h"

template <typename T>
class pDLinkedList
{
public:
	pDLinkedList(): size(0)
	{
		dummy = new pNode<T>();
	}
	pDLinkedList(const pDLinkedList& anDLL);
	~pDLinkedList()
	{
		clear();
	}

	pNode<T>* getDummy() const { return dummy; }
	
	void add(const T& newEntry);
	void display() const;
	void remove(const T& anEntry);
	bool isEmpty() const;
	void clear();
	std::vector<T> toVector() const;
	bool contains(const T& anEntry) const;
	int getFrequencyOf(const T& anEntry) const;

	// Operator overloading
	pDLinkedList& operator=(std::initializer_list<T> il);	// Add the items in the list into pDLinkedList

protected:
	pNode<T>* dummy;
	int size;
};

template <typename T>
pDLinkedList<T>::pDLinkedList(const pDLinkedList& anDLL) : pDLinkedList()
{
	// Deep copy
	if (!anDLL.isEmpty())
	{
		pNode<T>* node = (anDLL.getDummy())->getNext();	// because argument is const pDLinkedList&, can only call const functions on it
		while (node != anDLL.getDummy())
		{
			add(node->getItem());
			node = node->getNext();
		}
	}
}

template <typename T>
void pDLinkedList<T>::add(const T& newEntry)
{
	pNode<T>* node = new pNode<T>(newEntry);
	node->setNext(dummy);
	node->setPrev(dummy->getPrev());
	dummy->getPrev()->setNext(node);
	dummy->setPrev(node);
	size += 1;
}

template <typename T>
void pDLinkedList<T>::display() const
{
	if (isEmpty())
	{
		std::cout << "List is empty" << std::endl;
		return;
	}
	pNode<T>* node = dummy;
	while (node->getNext() != dummy)
	{
		std::cout << node->getNext()->getItem() << ", ";
		node = node->getNext();
	}
	std::cout << std::endl;
}

template <typename T>
void pDLinkedList<T>::remove(const T& anEntry)
{
	if (isEmpty())	return;
	pNode<T>* node = dummy->getNext();
	pNode<T>* nextnode = dummy;	// placeholder
	while (node != dummy)
	{
		if (node->getItem() == anEntry)
		{
			nextnode = node->getNext();	// Save
			node->getPrev()->setNext(node->getNext());
			node->getNext()->setPrev(node->getPrev());
			delete node;
			node = nextnode;	// Load
			size -= 1;
		}
		else
		{
			node = node->getNext();
		}
	}
}

template <typename T>
bool pDLinkedList<T>::isEmpty() const
{
	return size == 0;
}

template <typename T>
void pDLinkedList<T>::clear()
{
	if (isEmpty())	return;
	pNode<T>* node = dummy->getNext();
	pNode<T>* nextnode = dummy;	// placeholder
	while (node != dummy)
	{
		nextnode = node->getNext();	// Save
		node->getPrev()->setNext(node->getNext());
		node->getNext()->setPrev(node->getPrev());
		delete node;
		node = nextnode;	// Load
		size -= 1;
	}
}

template <typename T>
std::vector<T> pDLinkedList<T>::toVector() const
{
	std::vector<T> vec;
	if (isEmpty())	return vec;
	pNode<T>* node = dummy->getNext();
	while (node != dummy)
	{
		vec.push_back(node->getItem());
		node = node->getNext();
	}
	return vec;
}

template <typename T>
bool pDLinkedList<T>::contains(const T& anEntry) const
{
	if (isEmpty())	return false;
	pNode<T>* node = dummy->getNext();
	while (node != dummy)
	{
		if (node->getItem() == anEntry)
		{
			return true;
		}
		else
		{
			node = node->getNext();
		}
	}
	return false;
}

template<typename T>
int pDLinkedList<T>::getFrequencyOf(const T & anEntry) const
{
	int counter = 0;
	pNode<T>* node = dummy->getNext();
	while (node != dummy)
	{
		if (node->getItem() == anEntry)
		{
			counter += 1;
		}
		node = node->getNext();
	}
	return counter;
}

template<typename T>
pDLinkedList<T>& pDLinkedList<T>::operator=(std::initializer_list<T> il)
{
	for (auto i : il)
	{
		add(i);
	}
	return *this;
}
