#pragma once

#include "C8_IList.h"
#include <vector>
#include <algorithm>

template <typename T>
class vSortedList : public IList<T>
{
public:
	vSortedList() : size(0), ls(nullptr) {}
	vSortedList(const vSortedList& otherList);
	vSortedList(const vSortedList& list, std::size_t posStart, std::size_t posEnd);
	~vSortedList() { delete[] ls; }

	T& operator[](std::size_t n);
	const T& operator[](std::size_t n) const;

	bool isEmpty() { return size == 0; }
	int getLength() const { return size; }
	int getCapacity() const { return capacity; }

	bool insert(int pos, const T& item) { return true; }
	void insert(const T& item);
	std::size_t getPosition(const T& item);
	bool remove(int pos) { return removeByPosition(pos); }
	bool removeByPosition(int pos);
	void removeByValue(const T& item);
	void merge(const vSortedList& list);
	void merge(const std::initializer_list<vSortedList>& initialList);
	void clear() {}
	T getEntry(int pos) const { return ls[0]; }
	void setEntry(int pos, const T& item) {}
	void reverse();
	void display();

private:
	T* ls;
	int size;
	std::size_t capacity;			// serves as a check against size, should be equal to size after each call to insert()
	
	void swap(int pos1, int pos2);
	bool insertHelper(int pos, const T& item);
	int getPositionHelper(const T& item, int posStart, int posEnd);
	bool validate();
};

template <typename T>
const T& vSortedList<T>::operator[](std::size_t n) const
{
	if (n >= size)
	{
		throw "Out of Bound";
	}
	return ls[n];
}

template <typename T>
T& vSortedList<T>::operator[](std::size_t n)
{
	if (n >= size)
	{
		throw "Out of Bound";
	}
	return ls[n];
}

template <typename T>
vSortedList<T>::vSortedList(const vSortedList& otherList)
{
	T* newList = new T[otherList.getLength()];
	for (auto i = 0; i <= otherList.getLength() - 1; i += 1)
	{
		newList[i] = otherList[i];
	}
	ls = newList;
	size = otherList.getLength();
	capacity = otherList.getCapacity();
}

template <typename T>
vSortedList<T>::vSortedList(const vSortedList& list, std::size_t posStart, std::size_t posEnd)
{
	//	All pos in this class are as in array, i.e. from 0 ~ size - 1
	if (posEnd >= size)
	{
		throw "Out of Maximum Bound";
	}
	else if (posEnd < posStart)
	{
		throw "Max Bound smaller than Min Bound";
	}

	T* newList = new T[posEnd - posStart + 1];
	capacity = posEnd - posStart + 1;
	for (auto i = 0; i <= posEnd - posStart; i += 1)
	{
		ls[i] = list[i];
	}
	size = posEnd - posStart + 1;
	if (!validate())
	{
		throw "Size not equal to Capacity";
	}
}

template <typename T>
void vSortedList<T>::merge(const vSortedList& list)
{
	for (auto i = 0; i <= list.getLength() - 1; i += 1)
	{
		insert(list[i]);
	}
	if (!validate())
	{
		throw "Size not equal to Capacity";
	}
	return;
}

template <typename T>
void vSortedList<T>::merge(const std::initializer_list<vSortedList<T>>& initialList)
{
	for (auto i : initialList)
	{
		merge(i);
	}
	return;
}

template <typename T>
void vSortedList<T>::insert(const T& item)
{
	int pos = getPosition(item);
	if (!insertHelper(pos, item))
	{
		throw "Insert Error!";
	}
	return;
}

template <typename T>
std::size_t vSortedList<T>::getPosition(const T& item)
{
	if (size <= 0)
	{
		return 0;
	}
	else if (item <= ls[0])
	{
		return 0;
	}
	else if (item >= ls[size - 1])
	{
		return size;
	}
	return getPositionHelper(item, 0, size - 1);
}

template <typename T>
int vSortedList<T>::getPositionHelper(const T& item, int posStart, int posEnd)
{
	//	Binary Search
	if (posEnd == posStart + 1)
	{
		return posStart + 1;	// Instead of posEnd as sometimes posStart = posEnd
	}
	if (item == ls[(posStart + posEnd) / 2])
	{
		return (posStart + posEnd) / 2;
	}
	else if (item > ls[(posStart + posEnd) / 2])
	{
		return getPositionHelper(item, (posStart + posEnd) / 2, posEnd);
	}
	else
	{
		return getPositionHelper(item, posStart, (posStart + posEnd) / 2);
	}
}

template <typename T>
bool vSortedList<T>::insertHelper(int pos, const T& item)
{
	T* newList = new T[size + 1];
	
	for (auto i = 0; i <= pos - 1; i++)
	{
		newList[i] = ls[i];
	}
	newList[pos] = item;

	for (int i = pos; i <= size - 1; i++)
	{
		newList[i + 1] = ls[i];
	}
	delete[] ls;
	ls = newList;
	capacity += 1;
	size += 1;
	return validate();
}

template <typename T>
bool vSortedList<T>::removeByPosition(int pos)
{
	bool result = false;
	if (pos >= size)
	{
		throw "Out of Bound";
	}

	T* newList = new T[size - 1];
	for (auto i = 0; i <= pos - 1; i += 1)
	{
		newList[i] = ls[i];
	}
	for (auto i = pos + 1; i <= size - 1; i += 1)
	{
		newList[i - 1] = ls[i];
	}
	return result;
}

template <typename T>
void vSortedList<T>::removeByValue(const T& item)
{
	//	ls = std::remove(std::begin(ls), std::end(ls), item);	// Cannot as must pass actual array not pointer to std::begin and std::end
	std::vector<int> marker;

	for (auto i = 0; i <= size - 1; i += 1)
	{
		if (ls[i] == item)
		{
			marker.push_back(i);
		}
	}

	if (!marker.empty())
	{
		T* newList = new T[size - marker.size()];
		int j = 0;
		for (auto i = 0; i <= size - 1; i += 1)
		{
			if (std::find(marker.begin(), marker.end(), i) == marker.end())
			{
				newList[j] = ls[i];
				j += 1;
			}
		}
		delete[] ls;
		ls = newList;
		capacity = size - marker.size();
		size = capacity;
	}

	return;
}

template <typename T>
void vSortedList<T>::display()
{
	if (size <= 0)
	{
		return;
	}
	for (auto i = 0; i <= size - 1; i++)
	{
		std::cout << ls[i] << ", ";
	}
	std::cout << std::endl;
}

template <typename T>
bool vSortedList<T>::validate()
{
	return (size == capacity);
}

template <typename T>
void vSortedList<T>::reverse()
{
	int i = 0;
	int j = size - 1;
	while (j - i >= 1)
	{
		swap(i, j);
		i += 1;
		j -= 1;
	}
}

template <typename T>
void vSortedList<T>::swap(int pos1, int pos2)
{
	//	Only to be used in reverse()
	if (pos1 >= 0 && pos1 <= size - 1 && pos2 >= 0 && pos2 <= size - 1)
	{
		T temp;	// Requires proper constructor
		temp = ls[pos1];
		ls[pos1] = ls[pos2];
		ls[pos2] = temp;
	}
}
