**This notebook contains all practical tricks useful in production**


```SQL
--Trick 0x01:	Generate a series of integers

CREATE FUNCTION dbo.ufnGenerateIntSeries
(
	@min INT,
	@max INT
)
RETURNS @int_table TABLE
(
	Number INT
)
AS
BEGIN
	WHILE @min <= @max
	BEGIN
		INSERT INTO @int_table
		SELECT @min
		SET @min = @min + 1
	END
	RETURN;
END

--How to use

SELECT
	*
FROM
	dbo.ufnGenerateIntSeries(100, 110) AS FuncGenerateSeries
```

```SQL
--Trick 0x02:	Generate a series of dates

CREATE FUNCTION dbo.ufnGenerateDateSeries
(
	@basedate DATE,
	@days INT
)
RETURNS @date_table TABLE
(
	DateSeries DATE
)
AS
BEGIN
	WHILE @days >= 0
	BEGIN
		INSERT INTO @date_table
		SELECT DATEADD(day, @days, @basedate)

		SET @days = @days - 1
	END
	RETURN;
END

--How to use

SELECT
  FuncGenerateDaySeries.DateSeries
FROM
  dbo.ufnGenerateDateSeries('2018-01-01', 100) AS FuncGenerateDaySeries
ORDER BY
  FuncGenerateDaySeries.DateSeries ASC
```

```SQL
--	Trick 0x03: Outputs top N orders for each customer
--	Use OUTER APPLY to grab customers who did not order anything

DECLARE @N INT;
SET @N = 3;

SELECT
	C.CustomerID AS 'Customer',
	SOH.OrderDate
FROM
	Sales.Customer AS C
CROSS APPLY
	(
		SELECT
			TOP (@N)
			S.CustomerID,
			S.OrderDate
		FROM
			Sales.SalesOrderHeader AS S
		WHERE S.CustomerID = C.CustomerID
		ORDER BY S.OrderDate DESC
	) AS SOH
ORDER BY C.CustomerID ASC;
```

```SQL
--	Trick 0x04: Use a table function (input: SubCategoryID and N, both as INT)
--	to output Top N most expensive products in selected SubCategoryID
--	Then use a CROSS APPLY to find Top 3 most expensive products for each SubCategory

--	PART I: Table Function (easy)

DROP FUNCTION IF EXISTS dbo.ufnGetTopNProduct;
GO
CREATE FUNCTION dbo.ufnGetTopNProduct
(
	@subCategoryID AS INT,
	@N AS INT
)
RETURNS TABLE
AS
RETURN
	SELECT
		TOP (@N)
		P.ProductSubcategoryID AS 'Sub_Category_ID',
		P.Name,
		P.ListPrice AS 'List_Price'
	FROM
		Production.Product AS P
	WHERE P.ProductSubcategoryID = @subCategoryID
	ORDER BY List_Price DESC;
GO

--	PART II: Write a query using CROSS APPLY (difficult)
--	The key is to realize that, CROSS APPLY is similar to CROSS JOIN
--	in the way that each row from the left side will be repeated for K times
--	while K is the number of rows returned by right side for each row from left side
--	So basically you will want only DISTINCT rows from left side

SELECT
	TopN.Sub_Category_ID,
	TopN.Name,
	TopN.List_Price
FROM
(
	SELECT
		DISTINCT Prod.ProductSubcategoryID AS ID
	FROM
	Production.Product AS Prod
) AS P
CROSS APPLY
(
	SELECT
		*
	FROM
		dbo.ufnGetTopNProduct(P.ID, 3) AS ufn
) AS TopN
```

```SQL
--	Trick 0x05: Recursively generate Fibonacci numbers

;WITH Fibo_Series AS
(
	SELECT
		0 AS Para1,
		1 AS Para2,
		1 AS Result
		
	UNION ALL
	SELECT
		Para2,
		Result,
		Result + Para2
	FROM
		Fibo_Series
	--	Must put limit here to avoid error
	WHERE Fibo_Series.Result <= 1000
)
SELECT
	*
FROM
	Fibo_Series;
```