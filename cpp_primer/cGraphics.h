#pragma once

#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "SDL2main.lib")
#pragma comment(lib, "SDL2_image.lib")

#include <memory>
#include <unordered_map>
#include <string>
#include "SDL.h"
#include "SDL_image.h"

class cGraphics
{
public:
	//	Creator functions
	std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> Create_Window(int xWin, int yWin);
	std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> Create_Renderer();
	bool Create_TextureMap(std::string texid, std::string texres, int r, int g, int b);			//	Using helper function Create_Texture()

	//	ctor & dtor
	
	cGraphics(int xWin, int yWin, std::string texid, std::string texres);
	~cGraphics();

	//	Rendering
	void ClearScreen();
	bool SetRendererDrawColor(int r, int g, int b, int opaque);
	void RenderTexture(std::string texres, int xDes, int yDes);
	bool Show();
private:
	std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> m_Window;
	std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> m_Renderer;
	std::unordered_map<std::string, std::shared_ptr<SDL_Texture>> m_TextureMap;

	//	Private default ctor
	cGraphics() : m_Window(nullptr, SDL_DestroyWindow), m_Renderer(nullptr, SDL_DestroyRenderer) {}

	//	Creator helper
	SDL_Texture* CreateTextureRawPtr(std::string texres, int r, int g, int b);
	std::shared_ptr<SDL_Texture> CreateTexture(std::string texres, int r, int g, int b);
};