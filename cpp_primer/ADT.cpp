#include <memory>
#include <cstdlib>
#include "I2_binding.h"
#include "C4_DoubleLinkedList.h"
#include "C4_Ex8.h"
#include "C8_List.h"
#include "C12_SortedArrList.h"
#include "C16_Tree.h"

void dll();
void c4ex8();
void c8();
void c12();
void c122();
void c16();

//int main(int argc, char* args[])
//{
	//	dll();
	// c4ex8();
	// c8();
	// c12();
	// c122();
	//c16();
	//return 0;
//}

void c16()
{
	pBSTree<int> iBST;
	iBST.insert(30);
	iBST.insert(20);
	iBST.insert(40);
	iBST.insert(10);
	iBST.insert(60);
	iBST.insert(2);
	iBST.insert(15);
	iBST.insert(50);
	iBST.insert(80);
	iBST.insert(12);
	iBST.insert(18);
	iBST.insert(55);
	iBST.insert(11);
	iBST.insert(14);

	iBST.displayTree();
	std::cout << iBST.getHeight() << std::endl;
	iBST.remove(40);
	iBST.displayTree();
	std::cout << iBST.getHeight() << std::endl;
	return;
}

void c122()
{
	vSortedList<std::string> list;
	list.insert("Albeit");
	list.insert("Babylon");
	list.insert("Abstract");
	list.insert("Zeus");
	list.insert("Zork");
	list.insert("Jensen");
	list.insert("Rogers");
	list.insert("Hayk");
	list.insert("Macy");
	list.display();
}

void c12()
{
	vSortedList<int> list;
	vSortedList<int> list2;
	vSortedList<int> list3;
	list.insert(100);
	list.insert(200);
	list.insert(400);
	list.insert(800);
	list.insert(300);
	list.insert(500);
	list.insert(500);
	list.insert(500);
	list.insert(600);
	list.insert(700);
	list.insert(900);
	list.insert(100);
	list.insert(100);
	list.insert(300);
	list.insert(1000);
	list.insert(100);
	list.insert(200);
	list.insert(400);
	list.insert(800);
	list.insert(300);
	list.insert(500);
	list.insert(500);
	list.insert(500);
	list.insert(600);
	list.insert(700);
	list.insert(900);
	list.insert(100);
	list.insert(100);
	list.insert(300);
	list.insert(1000);
	list.insert(300);
	list.insert(500);
	list.insert(500);
	list.insert(500);
	list.insert(600);
	list.insert(700);
	list2.insert(100);
	list2.insert(501);
	list2.insert(-100);
	list2.insert(100001);
	list2.insert(300);
	list2.insert(999);
	list3.insert(10001);
	list2.insert(50121);
	list2.insert(-1000);
	list2.insert(1000);
	list2.insert(3002);
	list2.insert(999);
	list.merge({ list2, list3 });
	list.display();
	list.removeByValue(100);
	std::cout << std::endl;
	list.display();
	std::cout << std::endl;
	list.reverse();
	list.display();
}

void c8()
{
	/*int* arr = new int[10];
	arr[0] = 100;
	std::cout << arr[0] << std::endl;
	arr[9] = 900;
	std::cout << arr[9] << std::endl;*/

	vList<int> ls;
	ls.push_back(100);
	ls.push_back(200);
	ls.push_back(400);
	ls.display();
	ls.insert(2, 300);
	ls.display();
	ls.insert(2, { 301, 302, 303 });
	ls.display();
	ls[0] = 1000;
	ls.display();
	std::cout << ls[100] << std::endl;	// Should throw an error
}

void c4ex8()
{
	std::string blah = "abcd efg12 4";
	pCharDLinkedList chardll(blah);
	chardll.display();
	//chardll.append({ " blah", "ttttt" });
	//chardll.display();
	pCharDLinkedList chardll2(blah);
	chardll2.append(chardll);
	chardll2.display();
	std::cout << chardll2[1] << std::endl;
	chardll2[1] = '*';
	std::cout << chardll2[1] << std::endl;
	chardll2.display();
	std::cout << chardll2.subString(0, 10) << std::endl;
	std::vector<int> occ(128, -1);
	std::cout << occ['M'] << std::endl;	// implicit converted from char to int
	std::cout << chardll.containStr(" ef") << std::endl;
	std::string str_blah = " ef";
	std::cout << (str_blah.substr(0, str_blah.length() - 1)).rfind('e') << std::endl;
}

void dll()
{
	// Binding
	std::cout << "This is late binding using smart pointer" << std::endl;
	std::shared_ptr<base> pfoo = std::make_shared<derived>();
	pfoo->func();

	std::cout << "This is late binding using reference" << std::endl;
	derived foo2 = derived();
	base& rfoo2 = foo2;
	rfoo2.func();

	std::cout << "This is early binding" << std::endl;
	base foo = derived();
	foo.func();

	//	Linked List
	std::cout << "Linked List test" << std::endl;
	pDLinkedList<int> dll;
	dll.add(100);
	dll.add(50);
	dll.add(50);
	dll.add(100);
	dll.add(50);
	dll.add(50);
	std::vector<int> vecint = dll.toVector();
	std::cout << dll.contains(50) << std::endl;
	std::cout << dll.contains(80) << std::endl;
	std::cout << dll.getFrequencyOf(50) << std::endl;
	std::cout << dll.getFrequencyOf(100) << std::endl;
	std::cout << dll.getFrequencyOf(80) << std::endl;

	dll.display();
	dll.clear();
	dll.display();
	dll.clear();
	dll.display();
	for (auto i : vecint)
	{
		std::cout << i << ", ";
	}
	std::cout << std::endl;

	pDLinkedList<int> dll2;
	dll2 = { 100, 100, 50, 60, 80, 80, 80, 50 };
	dll2.display();
	pDLinkedList<int> dll3(dll2);
	dll3.display();
	
}