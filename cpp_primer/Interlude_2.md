- Early Binding and Late Binding

*Early binding* is resolved during compilation, while *Late Binding* is resolved during runtime

*Early Binding* is related to objects.

An example of *Early Binding*:
```C++
class base
{
public:
	base() : i(100) {}
	virtual void func()
	{
		std::cout << "i is " << i << std::endl;
	}
	virtual ~base() {}
protected:
	int i;
};

class derived : public base
{
public:
	derived() = default;
	void func() override
	{
		std::cout << "i is " << base::i * 2 << std::endl;
	}
};

// In Main.cpp
std::cout << "This is early binding" << std::endl;
base foo = derived();
foo.func();	// Outpus "i is 100"
```

*Late Binding* in inheritance is always related to references or (smart) pointers.
To take advantage of *Late Binding*, we do not want out objects to be in an activation record on the **runtime stack**.

An example of *Late Binding*:
```C++
class base
{
public:
	base() : i(100) {}
	virtual void func()	// Must be virtual
	{
		std::cout << "i is " << i << std::endl;
	}
	virtual ~base();
private:
	int i;
};

class derived : public base
{
public:
	derived() = default;
	void func() override
	{
		std::cout << "i is " << i * 2 << std::endl;
	}
};

// Using smart ponter
std::shared_ptr<base> pfoo = std::make_shared<derived>();
pfoo->func();	// outputs "i is 200"

// Using reference
derived foo2 = derived();
base& rfoo2 = foo2;
rfoo2.func(); // outputs "i is 200"
```