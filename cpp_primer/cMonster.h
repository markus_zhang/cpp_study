#pragma once

#include <string>
#include <vector>
#include <iostream>

class cInventory
{
private:
	int iNum;
	std::string strName;
public:
	cInventory() = default;
	cInventory(int num, std::string name)
	{
		iNum = num;
		strName = name;
	}
	
	//	Delegating ctor
	//	cInventory() : cInventory(10, "My Inventory") {}	// Comment out otherwise cMonster complains about ambigous call
	//	cInventory(int num = 0) : cInventory(num, "My Inventory") { std::cout << "Delegated" << std::endl; }	//	Comment out
};

class cMonster
{
	friend void Frd(const cMonster& mon);
private:
	int iX;
	int iY;
	std::string strID;
	double dDamage;
	mutable int iHP;
	cInventory inv;

	//	Static members
	static const bool isBeta = 0;	//	const integral (const int, const bool, const char, etc.) can be initialized in class
public:
	//	Following line blocks the generation of synthesized default ctor
	//	Thus compiler complains if we do not put to a default ctor and write e.g.:
	//	cMonster Goblin;
	
	cMonster(int x, int y, std::string id, double damage, int hp) : iX(x), iY(y), strID(id), dDamage(damage), iHP(hp) {}
	cMonster() = default;
	std::string GetID() const;	

	//	We may change a mutable member in a const function
	void DemoMutable() const;
	void DisplayHP() const;

	//	Return this pointer
	cMonster& Berserk();
	cMonster& Teleport();
	const cMonster& DisplayID() const;
	cMonster& DisplayID();
	void Do_DisplayID() const;

	//	Can I call friend?
	void CallFrd() const;
};

std::string cMonster::GetID() const
{
	return this->strID;
}

cMonster& cMonster::Berserk()
{
	iHP += 10;
	dDamage *= 2;
	return *this;
}

cMonster& cMonster::Teleport()
{
	iX += 2;
	iY += 2;
	return *this;
}

const cMonster& cMonster::DisplayID() const
{
	//	std::cout << strID << std::endl;
	Do_DisplayID();
	return *this;
}

cMonster& cMonster::DisplayID()
{
	//	std::cout << strID << std::endl;
	Do_DisplayID();
	return *this;
}

void cMonster::Do_DisplayID() const
{
	std::cout << strID << std::endl;
}

void cMonster::DemoMutable() const
{
	iHP += 10;
}

void cMonster::DisplayHP() const
{
	std::cout << iHP << std::endl;
}

void Frd(const cMonster& mon)
{
	std::cout << mon.iHP << std::endl;
}

void cMonster::CallFrd() const
{
	Frd(*this);
}

std::string sName = "Name outside of class";
class Foo
{
public:
	Foo(std::string name) : sName(name) {}

	std::string GetName()
	{
		return sName;
	}
private:
	std::string sName;
};

//	Initialization vs. Assignment
class init_assign
{
private:
	const int const_i;
	std::string& ref_str;
public:
	init_assign(int i, std::string str) : const_i(i), ref_str(str) {}	//	Initialization for const and ref is good
	/*
	init_assign(const int i, std::string& str)
	{
		const_i = i;		//	Wrong, cannot assign to const
		ref_str = str;		//	Wrong
	}
	*/
};