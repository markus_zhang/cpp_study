//#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <unordered_map>
//#include "cGraphics.h"
//#include "cJSON.h"
//#include "oop.h"
//#include "oop2.h"
//#include "Template.h"
//
//int main(int argc, char* args[])
//{
//	/* Class.md material
//
//	const cMonster Goblin(10, 10, "Goblin", 2.5, 20);
//	Goblin.DisplayHP();
//	Goblin.DemoMutable();
//	Goblin.DisplayHP();
//
//	//	Returning *this
//	std::cout << "Returning *this" << std::endl;
//	cMonster Goblin2(10, 10, "Goblin", 2.5, 20);
//	Goblin2.Berserk().Teleport();
//	Goblin2.Berserk().DisplayID();	//	This is OK, DisplayID() returns a const object
//	//	Goblin2.Berserk().DisplayID().Teleport();	//	THis is NOT OK, Teleport() cannot take a const object
//
//	//	Returning *this - const and non-const
//	std::cout << "Returning *this - const and non-const" << std::endl;
//	Goblin.DisplayID();
//	Goblin2.DisplayID();
//	//	Goblin.DisplayID().Berserk();
//
//	//	Friends
//	std::cout << "Friend function and class" << std::endl;
//	Frd(Goblin2);		//	const cMonster& can take cMonster& and do an implicit conversion
//	Goblin.CallFrd();	//	Goblin is const, and CallFrd must be a const function, see Class.md line 37 for explanation
//
//	//	Class Scopes
//	std::cout << "Class scope" << std::endl;
//	Foo foo("Name Inside class");
//	std::cout << foo.GetName() << std::endl;
//
//	*/
//
//	/*	Memory.md material
//
//	auto p = std::make_shared <int>(42);
//	auto q(p);
//	std::cout << p.use_count() << " " << q.use_count() << std::endl;
//
//	auto t = std::make_shared <int>(50);
//	std::cout << p.use_count() << " " << q.use_count() << " " << t.use_count() << std::endl;
//	t = p;
//	std::cout << p.use_count() << " " << q.use_count() << " " << t.use_count() << std::endl;
//
//	int i = 1000;
//	int* pi = &i;		//	A raw pointer
//	{
//		auto spi = std::make_shared<int>(i);	//	A shared pointer
//		std::cout << spi.use_count() << std::endl;	//	shared_ptr does NOT count other types of pointers
//	}	//	out of scope, spi deleted, BUT the object stays
//	auto spi = std::make_shared<int>(i);	//	A shared pointer
//	std::cout << spi.use_count() << std::endl;	//	shared_ptr does NOT count other types of pointers
//	std::cout << *pi << std::endl;
//
//	std::shared_ptr<int> pcopy = use_factory(42);
//	std::cout << "pcopy: " << pcopy.use_count() << std::endl;
//	*/
//
//	/*
//	std::unordered_map<std::string, int>* blah = new std::unordered_map<std::string, int>;
//	blah->emplace("Key1", 100);
//	std::unique_ptr<std::unordered_map<std::string, int>> pblah(blah);
//	pblah->emplace("Key2", 200);
//
//	cGraphics graphics(800, 600, "", "");
//
//	if (!graphics.SetRendererDrawColor(255, 0, 0, 0xff))
//	{
//		std::cout << "SetRenderer wrong!" << std::endl;
//	}
//	graphics.ClearScreen();
//	graphics.Show();
//	SDL_Delay(1000);
//
//	if (!graphics.SetRendererDrawColor(0, 255, 0, 0xff))
//	{
//		std::cout << "SetRenderer wrong!" << std::endl;
//	}
//	graphics.ClearScreen();
//	graphics.Show();
//	SDL_Delay(1000);
//
//	if (!graphics.SetRendererDrawColor(0, 0, 255, 0xff))
//	{
//		std::cout << "SetRenderer wrong!" << std::endl;
//	}
//	graphics.ClearScreen();
//	graphics.Show();
//	SDL_Delay(1000);
//	*/
//
//	/*
//	//---------------------
//	//	JSON parser and smart pointers
//	//---------------------
//	std::ifstream t("C:\\json.txt");
//	std::stringstream buffer;
//	buffer << t.rdbuf();
//	std::string sJson = buffer.str();
//	long index = 0;
//	std::shared_ptr<JSONObjectNode> root = ParseObject("root", sJson, index);
//	std::cout << std::endl;
//
//	//	Incorrect example 1: Convert from base to derived
//	//	JSONNode base(node_type::jArray, "XXX");
//	//	JSONArrayNode* pDerived = &base;			//	No type conversion from base to derived
//
//	//	Incorrect example 2: Convert from base pointer to derived pointer
//	//	JSONNode* pBase = &base;
//	//	JSONArrayNode* pDerived = pBase;			//	Cannot even convert from base pointer to derived pointer
//
//	//	Incorrect example 3: Convert from derived object to base object (albeit OK at compile time)
//	JSONArrayNode derived("Derived");
//	JSONNode base(derived);							//	We only get a stripped down version of derived
//
//	//	What we usually do called Polymorphism
//	JSONArrayNode pDerived("Derived");
//	JSONNode* pGeneraltype;
//	pGeneraltype = &pDerived;
//	pGeneraltype->test();						//	Will call the derived version because this is virtual
//	*/
//
//	//--------------
//	//	OOP Stuff - check oop.h
//	//--------------
//	/*
//	derived_private dp_foo;
//	//	dp_foo.memfunc_public();	//	this base public function becomes private as this class is derived privately from base
//	//	std:cout << dp_foo.memfunc_protected() << std::endl;	//	Because it's protected
//	dp_foo.padd();		//	This is possible as padd() isn defined in derived_private
//
//	derived_protected dpro_foo;
//	//	dpro_foo.memfunc_public();	//	Again this base public function becomes protected as this class is derived protected from base
//	dpro_foo.padd();
//
//	friend_base fb;
//	std::cout << "-----------------------" << std::endl;
//	fb.call_friend_derived();
//
//	std::cout << "---------Virtual/Hidden Test----------" << std::endl;
//	derived_virtual_test vt;
//	derived_virtual_test* pvt = &vt;
//	//	pvt->virtual_test();	// Must add base scope
//	pvt->base::virtual_test();
//
//	//----------------
//	// More OOP Stuff (check oop2.h)
//	//----------------
//
//	std::cout << "Call Virtual Function in CTOR" << std::endl;
//	derived2 d2;
//	*/
//
//	
//	//-------------------
//	//	Template
//	//-------------------
//	std::cout << compare("Hi", "Blah") << std::endl;
//	char foo1[5] = { 'H', 'i', 'B', 'C', '\0' };
//	char foo2[4] = { 'Z', 'B', 'C', '\0' };
//	std::cout << compare(foo1, foo2) << std::endl;
//
//	std::cout << "Template Blob class" << std::endl;
//	Blob<int> bint;
//	bint.push_back(1);
//	bint.push_back(2);
//	bint.push_back(3);
//	bint[0] = 100;
//	std::cout << bint[0] << std::endl;
//
//	std::cout << "Template Boo class" << std::endl;
//	Boo<int> booint{ 21 };
//	Boo<double> booint2{ 34.66 };
//
//	//	Boo<std::string> boostr{ "HAHAHAHA" };
//	ffunc3(booint);
//	ffunc3(booint2);
//
//	//booint.f();
//	// ffunc2(booint, booint2);
//	//	Boo2 b2;
//
//	std::cout << "Non-template class with template class friends" << std::endl;
//	C ctest;
//	Pal<C> palctest;
//	Pal<int> palinttest;
//
//	palctest.test(ctest);
//	// palinttest.test(ctest); // Not possible
//	Pal2<int> pal2inttest;
//	pal2inttest.test(ctest);
//
//	// pal2inttest.badtest(ctest.k); // Users cannot access, only the class itself can access in {}
//
//	std::cout << "Static members in template class" << std::endl;
//	
//
//	statclass<int> st1;
//	statclass<double> st2;
//	statclass<int>::k = 100;
//	statclass<double>::k = 999;
//	st1.DisplayK();
//	st2.DisplayK();
//	
//	return 0;
//}