### Notes on implementing Sorted List

- Rule of three:

In our Sorted List class, we use a pointer to hold a dynamic array:

```C++
T* ls;
```

Assume that we do **not** implement a copy constuctor,
i.e. the compiler will create a default copy constructor
for us.

The problem is, the default copy constructor will only do
shallow copy for pointers.

This is going to be a speed train to double deletion:

Consider this piece of code:
```C++
list.merge({ list2, list3 });
```
The `merge` function takes a `std::initializer_list` as
the parameter:

```C++
template <typename T>
void vSortedList<T>::merge(const vSortedList& list)
{
	for (auto i = 0; i <= list.getLength() - 1; i += 1)
	{
		insert(list[i]);
	}
	if (!validate())
	{
		throw "Size not equal to Capacity";
	}
	return;
}

template <typename T>
void vSortedList<T>::merge(const std::initializer_list<vSortedList<T>>& initialList)
{
	for (auto i : initialList)
	{
		merge(i);
	}
	return;
}
```

What's special about `std::initializer_list` is that
it always creates copies of elements. Since we are getting
copies of `vSortedList<T>`, and we can only rely on the
implicit copy constructor to make them.

Remember that the implicit copy contructors will never make
deep copies of pointer object, so it's easy to see that we
are very easy to get double deletion errors, especially when
we have a well defined desturctor:

```C++
delete[] ls;
```

This is called when the temporary object goes out of scope
and will almost surely cause double deletion error.

We need to implement a proper copy constructor to remove this error:

```C++
template <typename T>
vSortedList<T>::vSortedList(const vSortedList& otherList)
{
	T* newList = new T[otherList.getLength()];
	// Deep copy
	for (auto i = 0; i <= otherList.getLength() - 1; i += 1)
	{
		newList[i] = otherList[i];
	}
	ls = newList;
	size = otherList.getLength();
	capacity = otherList.getCapacity();
}
```

- Remove elements from a C array

We can **only** use `std::remove()` for fixed size arrays,
with the help of `std::begin()` and `std::end()`:

```C++
ls = std::remove(std::begin(ls), std::end(ls), item);
```

However this doesn't work for us as `ls` is a dynamic array.