#pragma once
#include <utility>

template <typename T>
class pTriNode
{
public:
	pTriNode() : leftChild(nullptr), middleChild(nullptr), rightChild(nullptr) {}
	pTriNode(const pTriNode& node);
	pTriNode(const T& small);
	pTriNode(const T& small, const T& big);
	pTriNode<T>& operator=(const pTriNode<T>& node);
	pTriNode<T>& operator=(pTriNode<T> node);		// Another way to overload operator=
	T getSmallItem() const { return smallItem; }
	T getBigItem() const { return bigItem; }
	void setSmallItem(const T& small) { smallItem = small; }
	void setBigItem(const T& big) { bigItem = big; }
	pTriNode<T>* getLeftChild() const { return leftChild; }
	pTriNode<T>* getMiddleChild() const { return MiddleChild; }
	pTriNode<T>* getRightChild() const { return RightChild; }
	pTriNode<T>*& getLeftChild() { return leftChild; }
	pTriNode<T>*& getMiddleChild() { return MiddleChild; }
	pTriNode<T>*& getRightChild() { return RightChild; }

	bool hasLeftChild() const { return (leftChild != nullptr); }
	bool hasMiddleChild() const { return (middleChild != nullptr); }
	bool hasRightChild() const { return (rightChild != nullptr); }

	void displaySmall() const;
	void displayBig() const;

	bool isLeaf() const;
	bool isTwoNode() const;
	bool isThreeNode() const;

public:
	T smallItem, bigItem;
	pTriNode<T>* leftChild;
	pTriNode<T>* middleChild;
	pTriNode<T>* rightChild;
};

template <typename T>
pTriNode<T>& pTriNode<T>::operator=(pTriNode<T> node)
{
	//	Need a swap function
	//	https://stackoverflow.com/questions/3279543/what-is-the-copy-and-swap-idiom
}

template <typename T>
pTriNode<T>& pTriNode<T>::operator=(const pTriNode<T>& node)
{
	if (this != &node)
	{
		pTriNode<T>* newLeft = nullptr;
		pTriNode<T>* newMiddle = nullptr;
		pTriNode<T>* newRight = nullptr;

		try
		{
			newLeft = new pTriNode<T>(*node.leftChild);
			newMiddle = new pTriNode<T>(*node.middleChild);
			newLeft = new pTriNode<T>(*node.rightChild);
		}
		catch
		{
			delete newLeft;
			delete newMiddle;
			delete newRight;
			throw;
		}

		smallItem = node.getSmallItem();
		bigItem = node.getBigItem();
		std::swap(leftChild, newLeft);
		std::swap(middleChild, newMiddle);
		std::swap(rightChild, newRight);

		delete newLeft;
		delete newMiddle;
		delete newRight;

		return *this;
	}
}

template <typename T>
pTriNode<T>::pTriNode(const pTriNode& node)
{
	pTriNode<T>* newLeft = nullptr;
	pTriNode<T>* newMiddle = nullptr;
	pTriNode<T>* newRight = nullptr;

	try
	{
		newLeft = new pTriNode<T>(*node.leftChild);
		newMiddle = new pTriNode<T>(*node.middleChild);
		newLeft = new pTriNode<T>(*node.rightChild);
	}
	catch
	{
		delete newLeft;
		delete newMiddle;
		delete newRight;
		throw;
	}

	smallItem = node.getSmallItem();
	bigItem = node.getBigItem();
	std::swap(leftChild, newLeft);
	std::swap(middleChild, newMiddle);
	std::swap(rightChild, newRight);

	delete newLeft;
	delete newMiddle;
	delete newRight;
}

template <typename T>
pTriNode<T>::pTriNode(const T& small) : pTriNode()
{
	smallItem = small;	// Need assignment operator for T
}

template <typename T>
pTriNode<T>::pTriNode(const T& small, const T& big) : pTriNode()
{
	smallItem = small;	// Need assignment operator for T
	bigItem = big;	// Need assignment operator for T
}

template <typename T>
bool pTriNode<T>::isLeaf() const
{
	bool temp = false;
	if (leftChild == nullptr && middleChild == nullptr && rightChild == nullptr)
	{
		temp = true;
	}
	return temp;
}

template <typename T>
bool pTriNode<T>::isTwoNode() const
{
	bool temp = false;
	if (leftChild != nullptr && middleChild != nullptr && rightChild == nullptr)
	{
		temp = true;
	}
	return temp;
}

template <typename T>
bool pTriNode<T>::isThreeNode() const
{
	bool temp = false;
	if (leftChild != nullptr && middleChild != nullptr && rightChild != nullptr)
	{
		temp = true;
	}
	return temp;
}

template <typename T>
void pTriNode<T>::displaySmall() const
{
	std::cout << smallItem << ", ";
}

template <typename T>
void pTriNode<T>::displayBig() const
{
	std::cout << bigItem << ", ";
}