#pragma once
#include <vector>
#include "cJSONNode.h"

class JSONObjectNode : public JSONNode
{
public:
	JSONObjectNode(const std::string& k) : JSONNode(node_type::jObject, k) {}
	const std::shared_ptr<JSONNode>& operator[](const std::string& key);
	node_type GetType() const { return type; }

	//	Operations
	void push_back(std::shared_ptr<JSONNode> node);
private:
	std::vector<std::shared_ptr<JSONNode>> jsonObject;
};