#pragma once
#include <memory>
#include <iostream>

enum class node_type : char
{
	jValue = 0,
	jArray = 1,
	jObject = 2
};

class JSONNode
{
public:
	JSONNode(node_type t, std::string k) : type(t), key(k) {}
	virtual node_type GetType() const { return type; }
	virtual void test() { std::cout << "This is base"; }
protected:
	const node_type type;
	std::string key;		//	Used for JSONObjectNode to avoid using complex containers
};

