## Function

### Basics
- You cannot return a pointer/reference to a local object.
	- Local objects get destroyed when they go out of scope
    - It's OK to create objects on Heap with **new** and then return a reference/pointer to it
    - Objects created on Heap are kept until the end of program
    - **Static** local objects are also kept till the end of the program

### Argument passing
- C++ compiler will implicitly convert variables to the type of parameters
	```
	double d = 2.46;
	char c = 'S';
	int i = add(d, c);
	```
	With add() defined as:
	```
	int add(int a, int b);
	```
	C++ will do an iimplicit conversion to match the parameters

- Passing a pointer as parameter is **By Value**
  - It's easy to get confused about this, but pointers are not references.
  - When we copy a pointer, the value of the pointer, i.e. the address of the pointee is copied

- Passing by Reference
  - Use Const Reference whenever you do not need to modify the value
  - Const Reference can take literals, non-const variables and const ones
  - Non-const Reference can only take non-const variables

- More about ```constness```
  - **Top Level** ```const``` applies to the object itself
	```
	const int i1 = 42;	//	This is top level
	int i2 = 42;
	const int* j = &i2;	//	j is a pointer to const int, not top level
	```
  - We can initiate a ```const``` variable, including reference and pointer, with ```non-const``` ones
  - However we cannot initiate ```non-const``` ones with ```const``` ones

- Array parameters
  - Since we cannot copy arrays, we cannot pass arrays **by value**
  - C++ compilers usually convert arrays to pointer to arrays
  - Pass an array to a function = Pass **a pointer to the first element** of that array
  - C++ doesn't check bound so we need to be careful:
    - Some arrays, e.g. char*, natually contains an end null character
    ```
	//	chArray is a char*
	if (chArray)
	{
		std::cout << *chArray ++;	//	Display *chArray and move pointer forward
	}
	```
	- Use std\::begin() and std::end(), or pass ```const int* beg``` and ```const int* end```
    - Manually pass a ```size_t size``` parameter into the function
  - Recall that we cannot have arrays of references, but we can have references **to** arrays
  - However it's pretty limited, we need to specify the dimension for pointer/reference to arrays
  ```
  int iarr2[][4] = { {1,2,3,4},{5,6,7,8},{9,10,11,12} };
  int(*piarr2)[3][4] = &iarr2;
  ``` 
  In above example, it would be wrong to omit the dimensions. piarr2 is a pointer to a 2d array

- Passing unknown number of parameters
  - If they are of the same type we can use an **initializer_list**
	```
	std::string para1 = "Michael";
	std::string para2 = "2016/07";
	initializer_list<std::string> init;
	init.push_back(para1);
	init.push_back(para2);
	foo(init);		//	void foo(initializer_list<std::string> list)
	```
  - Under the hood C++ copy construct an array for the initializer_list
  - If they are of different types then use **variadic template**

### Return Types
- Reference returns are lvalue
  - This means that you can assign them new values
  - E.g. GetPriceByTag("1Z3F45B") = 4.50;
  - double& GetPriceByTag(std::string)
- Other returns are rvalue
  - Note that Pointer returns are actually rvalue
- Return a pointer to an array
  - You must fix the dimension
  - Read the declaration from inside to outside
  - std::string (*func(int i, double d))[100];
    - This is a function taking in two parameters
    - This is a function that returns a pointer, thus the *
    - This is a pointer to an std::string array with capacity of 100
    - TBH I prefer **std::vector*** or **std::vector&**
  - C++11 allows **trailing return type**
    - Very interesting as we can specify the return type
    - E.g. the above function declaration will be written as:
    - **auto func(int i, double d) -> std::string(*)[100];**

### Function Overloading
- Must have different types **or** different type of parameters
- Differ only in term of return types **DOES NOT WORK**
  - E.g. These are legal overloading:
    - int foo(int i, double d);
    - int foo(int i);
    - std::string foo(int i, double d, std::string s);
    - int foo(const int& i);
  - E.g. These are illegal overloading:
    - double foo2(int i, double d);
    - const int& foo2(int i, double d);
  - It's interesting to note that difference in **returns** does not implement overloading, while difference in **parameters** does
- More about ```constness```
  - Parameters with **top-level** constness are indistinguishible with ones without
    - int foo(blah bl);
    - int foo(const blah bl);   // C++ allows this, but if you put the mouse on top of the two delcarations they show the same thing
  - Const reference creates **Real** overloading (I'd like to call the above **Fake**)
    - int foo(bleh bl);
    - int foo(const bleh& bl);  // Real overloading, try that with your mouse!
  - More confusing, these are **Real** overloading because the ```const``` is not top-level
    - int foo(bleh* bl);
    - int foo(const bleh* bl);
    - The second one is new because ```const bleh* bl``` is **not** top-level ```const``` for bl
   - ```const_cast```
     - Recall the ```const_cast``` is to cast away the ```constness``` from **references** and **pointers** that refer to something that **is not const**
     - ```const_cast``` cannot const away the ```constness``` of a top-level one (e.g. ```const int i```)
     - One trick is to overload a function that returns **const reference** to one that returns **non-const reference**
     ```
	 const int& foo(const int& i);
     int& foo(int& i)
     {
		// Instead of rewriting the whole function
       auto &r = foo(const_cast<const int&>(i);		// cast to const
       return const_cast<int&>(r);                 // cast away const 	
     }	
	 ``` 

### Features for Specialized Use
  - Default Argument
    - Declaration is tricky:
      - This is OK ```void defpara(int i = 10, double d = 2.4, char ch = 'K');```
      - The second function declaration triggers compilation error:
		```
		void defpara(int i = 10, double d = 2.4, char ch = 'K');
		void defpara(int i, double d, char ch = 'L'); // Replaced by first one
		```
   	  - You cannot omit default parameters on left side
        - E.g. ```void defpara(int i = 10, char ch = 'K');```
            - call ```defpara('L')``` is equivalent to ```defpara('L', 'K')```
            - Because 'L' can be implicitly converted to its ASCII and fits in parameter ```i```
            - You must call ```defpara(10, 'L')```
      - It's legal to declare a function multiple times in the same scope
      - But **each parameter can have its default specified only once in the given scope**
      - I'm not going to dwel on this topic any further as I'm getting problems with examples in book
  - Use \_\_func__ for the name of the function it resides in
    - std\::cout << \_\_func__ << std::endl;
  - Use \_\_FILE__ to get the name of the file (returns a string literal)
  - Use \_\_LINE__ to get the current line umber (returns an integer literal)
  - Use \_\_TIME__ to get the time the file was **compiled** (returns a string literal)
  - Use \_\_DATE__ to get the date the file was **compiled** (returns a string literal)

### Function Matching
  - It's not easy to see which overloaded version gets the call
    - E.g. Consider the following functions:
      - void f(int);
      - void f(double, double = 3.14);
    - Which one gets the call if I write f(5.6)?
      - Recall that technically, **implicit conversion** makes it possible that f(int) gets the call and 5.6 is converted to 5   
      - Recall that with default arguments, f(double, double = 3.14) also matches the call
  - **The closer the types of the argument and parameters are to each other**, the better the match.
    - Take the last example:
		f(5.6)
		void f(int);
		void f(double, double = 3.14)
	- Now, to match ```void f(int)```, the compiler must make an implicit conversion
    - No conversion needed for ```void f(double, double = 3.14)```
    - So the second function is the best match
    - Now consider the following example:
		f(24, 5.6)
		void f(int, int);
		void f(double, double = 3.14)
	- Notice that both need a conversion, from double/integer to integer/double
  - **General Rule of function matching**
    - The match for each argument is no worse than the match required by any other viable function
    - There is at least one argument for which the match is better than the match provided by any other viable function
    - Again, take the previous exmaple:
 		```
		f(24, 5.6)
		void f(int, int);
		void f(double, double = 3.14)
		```
	- For the first argument, the first function is a better match
    - For the second argument, the second function is a better match
    - In this case, the compiler throws an error of **ambigous call**
  - **Detailed Rule of Conversion Ranking**
    - TBH I do not want to go into details here
    - Exact Match (including adding/removing a top-level ```const```) > Match through a ```const``` conversion > Match through a promotion > Match through an arithmetic/pointer conversion > Match through a class-type conversion
    - Example of **Promotion**:
      - Small integral types always promote to ```int``` or to a larger integral type
        - This also means that if we have tow functions, one takes ```int``` and one takes ```short```, the ```short``` one will only be called when we pass an explicit ```short```
        - Example:
		```
		void funcmatch1(int);			//	std::cout << "This is an integer";
		void funcmatch1(short);		//	std::cout << "This is a short";
		char chfm = 'A';
		short sfm = 10;
		funcmatch1(chfm);			//	Calls the int version even when conversion to short is possible
		funcmatch1(sfm);			//	Calls the short version
		``` 
    - **All arithmetic cnversions are treated as equivalent to each other**
      - This means ```int``` to ```unsigned int``` is equal with ```int``` to ```double```
      - Consider the following example
		```
		void funcmatch2(int);
		void funcmatch2(short);
		funcmatch2(2.34);
		```
	  - On the above example, it is **arithmetic** conversion instead of **type** conversion.
      - ```double``` to ```int``` is equivalent to ```double``` to ```short```
      - Compiler complains about ambiguous call.

### Pointers to Functions
  - A function pointer points to a particular type, which is **determined by the function's return type and the types of its parameters.**
  - Example:
	```
	SDL_Texture* GetTextureByTag(const string& strTexture);
	```		
  - If we ignore the name it's simple a function that takes a ```const string&``` as parameter and returns a ```SDL_Texture*```
  - The function pointer can be written as:
	```
	SDL_Texture* (*pf)(const string&);
	```
	Again if you read from inside to ouside, it's a pointer to a function taking ```const string&``` as parameter and returns ```SDL_Texture*```
  - To use the function pointer, we must assign it to **the address** of a function of that type
    - Fortunately, C++ will do the implicit conversion for us if we only use the name of the function, so both are good:
		```
		pf = GetTextureByTag;
		pf = &GetTextureByTag;
		```
	- And we don't even need to dereference the pointer! These are equivalent:
		```
		SDL_Texture* texWorld = pf("\Resource\Texture\World.png");
		SDL_Texture* texWorld = (*pf)("\Resource\Texture\World.png");
		```
  - There is **no type conversion** for function pointers.
    - For overloaded functions, we must declare explicitly the return type and argument type for the function pointer
  - Use Function Pointers to pass functions as parameters
    - Again we can simply use the name of the function pointer with deferencing it:
		```
		void Draw(int iScreenX, int iScreenY, pf(const string& tex));
		//	To use Draw()
		Draw(x, y, GetTextureByTag);
		```
  - Return a Function Pointer
    - This is very messy, example:
		```
		//	Taking int as arguments and returning a pointer to a function that takes int and int* as arguments and returns an integer
		int (*func(int))(int*, int);
		//	To save your eyes
		using pf = int(*)(int*, int);		//	pf is a pointer to a function that takes int* and int and returns int
		pf func(int);		//	Equivalent to int(*func(int))(int*, int)
		```
	- Read from inside to outside:
      - ```*func(int)``` -> This definitely takes in an integer and returns a pointer to a function
      - ```int () (int*, int)``` -> the returned pointer points to a function that takes ```int*``` and ```int``` and returns an ```int```
	- Using ```auto```
		```
		auto func(int) -> int(*)(int*, int);
		```
