https://www.hackerrank.com

#### 1 - Binary Tree Node

![01 01](Images/SQL_Scenario/Hacker/01_01.png)

![01 02](Images/SQL_Scenario/Hacker/01_02.png)

![01 03](Images/SQL_Scenario/Hacker/01_03.png)

**Research:**

Easy question. If P is NULL then it definitely is the *root*. Then we check if P is in column N, if so then it's *Inner*, otherwise it's a *leaf*.

**Solution:**

```SQL
SELECT
    N,
    CASE WHEN P IS NULL THEN 'Root'
        WHEN N IN
            (SELECT
                DISTINCT B1.N
            FROM BST AS B1
                JOIN BST AS B2 ON B1.N = B2.P) THEN 'Inner'
            ELSE 'Leaf' 
    END AS NodeType
FROM BST
ORDER BY N ASC;
```

#### 2-The Report

![02 01](Images/SQL_Scenario/Hacker/02_01.png)

![02 02](Images/SQL_Scenario/Hacker/02_02.png)

![02 03](Images/SQL_Scenario/Hacker/02_03.png)

![02 04](Images/SQL_Scenario/Hacker/02_04.png)

**Research**

There are two key points to take care in this problem:

1. Need to show names as `NULL` if Grade < 8
2. How to `JOIN` the two tables to get the Grade.

**Solution**

```SQL
SELECT
    CASE
        WHEN G.Grade >= 8 THEN Students.Name ELSE NULL END AS Name,
    G.Grade,
    Students.Marks
FROM
    Students
        INNER JOIN
            Grades AS G
                ON Students.Marks >= G.Min_Mark AND Students.Marks <= G.Max_Mark
ORDER BY G.Grade DESC, Students.Name ASC
```

#### 3 - Contest Leader Board

![03 01](Images/SQL_Scenario/Hacker/03_01.png)

![03 02](Images/SQL_Scenario/Hacker/03_02.png)

![03 03](Images/SQL_Scenario/Hacker/03_03.png)

![03 04](Images/SQL_Scenario/Hacker/03_04.png)

**Research**

Basically the requirement consists of two parts:

- You have to grab the Maximum score of each hacker_id obtained for each challenge_id. There could be multiple attempts and you only want to grab the maximum score.
- Then you will sum all these scores for each hacker_id, display them in DESC (hacker_id as secondary order ASC if same total score), and ignore those having zero score.

To simpify the logic I'm using a sub query:

**Solution**

```SQL
SELECT
    SubQ.hacker_id,
    SubQ.name,
    SUM(SubQ.score_max) AS 'Total Score'
FROM
(
    SELECT
        H.hacker_id,
        H.name,
        MAX(S.score) AS score_max,
        S.challenge_id
    FROM
        Hackers AS H INNER JOIN Submissions AS S
            ON H.hacker_id = S.hacker_id
    GROUP BY S.challenge_id, H.hacker_id, H.name
) AS SubQ

GROUP BY SubQ.hacker_id, SubQ.name
HAVING SUM(SubQ.score_max) <> 0
ORDER BY SUM(SubQ.score_max) DESC, hacker_id ASC
```

**Note**

- Any non-aggregate part in `SELECT` must be present in the `GROUP BY`.

  So an immediate question is how to `SELECT` non-aggregate columns if we must use a `GROUP BY` for some aggregate functions. This is a general question and not limited to this question.

  I think a common solution is:

  **First:**

  `GROUP BY` whatever columns you need, e.g. imagine that we have the following table:

  id | name | gender | level | salary 
--|
  1  | Michael.E | M | 1 | 50000
  2  | Amy.T | F | 2 | 60000
  3  | Mike.K | M | 1 | 50000
  4  | Tommy.J | M | 1 | 45000
  5  | John.D | M | 3 | 80000
  6  | Holly.E | F | 2 | 65000
  7  | Fred.K | M | 2 | 55000

  Let's say we want to `GROUP BY` level. This will trigger an error:

  ```SQL
  SELECT
	name,
	MAX(salary) AS Highest_Salary
  FROM
	table
  GROUP BY level
  ```
  Because name is not in `GROUP BY`. However it would work if you put level in `SELECT`:

  ```SQL
  SELECT
	level,
	MAX(salary) AS Highest_Salary
  FROM
	table
  GROUP BY level
  ```

  This will return something like this:

  level | Highest_Salary
--|
  1 | 50000
  2 | 65000
  3 | 80000

  **Next:**

  Use the above result as a sub query, and `JOIN` the original table `ON` level and salary

```SQL
SELECT
  Table.name,
  Table.level,
  Table.salary
FROM
  Table INNER JOIN
  (
	SELECT
	  level,
	  MAX(salary) AS Highest_Salary
	FROM
	  table
	GROUP BY level	
  ) AS subq
ON Table.salary = subq.Highest_Salary AND Table.level = subq.level
```

http://bernardoamc.github.io/sql/2015/05/04/group-by-non-aggregate-columns/

In the above link you can find an alternative:

```SQL
SELECT g1.name, g1.publisher, g1.price, g2.price, g1.released_at
FROM games AS g1
LEFT OUTER JOIN games AS g2
ON g1.released_at = g2.released_at AND g1.price < g2.price;
```

The original table is:

name        |   publisher    | price | released_at
--|
 Metal Slug Defense | SNK Playmore   |    30 | 2015-05-01
 Project Druid      | shortcircuit   |    20 | 2015-05-01
 Chroma Squad       | Behold Studios |    40 | 2015-04-30
 Soul Locus         | Fat Loot Games |    30 | 2015-04-30
 Subterrain         | Pixellore      |    40 | 2015-04-30

And the joined table is:

name | publisher | price | price | released_at
--|
 Metal Slug Defense | SNK Playmore   |    30 | (null) | 2015-05-01
 Project Druid      | shortcircuit   |    20 |     30 | 2015-05-01
 Chroma Squad       | Behold Studios |    40 | (null) | 2015-04-30
 Soul Locus         | Fat Loot Games |    30 |     40 | 2015-04-30
 Soul Locus         | Fat Loot Games |    30 |     40 | 2015-04-30
 Subterrain         | Pixellore      |    40 | (null) | 2015-04-30

Can you figure out the reason?
Anyway, add `WHERE price IS NOT NULL` and you get the list you want