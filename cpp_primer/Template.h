#pragma once
#include <vector>
#include <memory>
#include <exception>

template <typename T>
int compare(const T& v1, const T& v2)
{
	if (v1 < v2)
	{
		return -1;
	}
	else
	{
		if (v1 > v2)
		{
			return 1;
		}
	}
	return 0;
}

//	Non-type parameters
//	Must be one of the five:
//	1) Integral or enumeration type
template <int N, int M>
int compare(const char (&p1)[N], const char (&p2)[M])	// Cannot write const char& p2[M] because this is array of char references, which is NOT allowed
{
	return strcmp(p1, p2);
}

template <int N, int M>
int compare(char (&p1)[N], char (&p2)[M])
{
	return strcmp(p1, p2);
}

//	2) pointer to object or pointer to function
template <std::string* blah>
void foo()
{

}

//	Smart pointers do not work
//template <std::shared_ptr<std::string> blah>
//void foo2()
//{
//
//}

//	3) lvalue reference to object/function
template <std::string& blah(int)>
void foo3()
{

}

//	4) pointer to members

//	5) std::nullptr_t
template<std::nullptr_t blah>
void foo4()
{

}

//---------------------
//	Class Template (1)
//---------------------
template <typename T>
class Blob
{
public:
	typedef T value_type;
	typedef typename std::vector<T>::size_type size_type;

	Blob() : data(std::make_shared<std::vector<T>>()) {}
	//	Blob() = default;
	Blob(std::initializer_list<T> il);

	size_type size() const
	{
		return data->size();
	}

	bool empty() const
	{
		return data->empty();
	}

	//	Add and remove element
	void push_back(const T& t)
	{
		data->push_back(std::move(t));
	}

	void pop_back();

	//	Element access
	T& front();
	T& back();

	//	Operator overloading
	T& operator[](size_type i);

private:
	std::shared_ptr<std::vector<T>> data;
	//	std::vector<T>* data;
	//	Throw message if data[i] is not valid
	void check(size_type i, const std::string& msg) const;
};

template <typename T>
void Blob<T>::check(size_type i, const std::string& msg) const
{
	if (i >= data->size())
	{
		throw std::out_of_range(msg);
	}
}

template <typename T>
T& Blob<T>::front()
{
	check(0, "Empty vector");
	return data->front();
}

template <typename T>
T& Blob<T>::back()
{
	check(0, "Empty vector");
	return data->back();
}

template <typename T>
T& Blob<T>::operator[](size_type i)
{
	check(i, "Out of range");
	return (*data)[i];
}

//-------------------
//	Template and Friend
//-------------------

template <typename T>
class Boo;

template <typename T>
void ffunc3(T&);

template <typename T>
class Boo
{
	friend void ffunc3<T>(T&);

	template <typename U>
	friend bool IsEqual(const Boo<T>&, const Boo<U>&);
public:
	Boo() = default;
	Boo(T input)
	{
		t = input;
	}
	
public:
	T t;
	//std::shared_ptr<std::vector<T>> data;
};

template <typename T, typename U>
bool IsEqual(const Boo<T>& boo1, const Boo<U>& boo2)
{
	return boo1.t == boo2.t;
}

void ffunc(int i)
{
	std::cout << i << std::endl;
}

template <typename T>
void ffunc1(T)
{
	std::cout << "Size is: " << sizeof(T) << std::endl;
}

template <typename T>
void ffunc3(T& t1)
{
	std::cout << t1.t << std::endl;
}

template <typename T>
class Boo2
{
public:
	Boo2() = default;
	Boo2(T input)
	{
		t = input;
	}
private:
	//std::shared_ptr<std::vector<T>> data;
	T t;
};

//--------------------------------------------
//	Non-template class with template class as friends
//--------------------------------------------

//	Forward declaration
template <typename T>
class Pal;

class C
{
	friend class Pal<C>;	// Only Pal instantiated with type C can be friend of C

	template <typename T>	//	All instantiation of Pal2 may access C
	friend class Pal2;
public:
	C() : k(100) {}
private:
	int k;
};

template <typename T>
class Pal
{
public:
	void test(T t)
	{
		std::cout << t.k << std::endl;
	}
};

template <typename T>
class Pal2
{
public:
	void test(C c)
	{
		std::cout << c.k << std::endl;
	}
	void badtest(int i)
	{
		std::cout << i << std::endl;
	}
};

//----------------------
//	Static members
//----------------------
template <typename T>
class statclass
{
public:
	static int k;
public:
	void DisplayK()
	{
		std::cout << k << std::endl;
	}
};

template <typename T>
int statclass<T>::k;