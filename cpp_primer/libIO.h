#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


namespace libIO
{
	std::string LoadSource(std::string source)
	{
		std::ifstream sourceFile(source);
		if (sourceFile.fail())
		{
			std::cout << "Failed to load Source file!" << std::endl;
			return "";
		}

		std::stringstream buffer;
		buffer << sourceFile.rdbuf();
		std::string sourceCode(buffer.str());

		std::cout << sourceCode << std::endl;
		return sourceCode;
	}

	void SeperateSource(const std::string& loadedSource, std::vector<std::string>& target)
	{
		//	This should come right after LoadSource()
		if (loadedSource.length() <= 0)
		{
			return;
		}

		std::string temp = loadedSource;

		while (temp != "")
		{
			target.push_back(libString::getLine(temp));
		}
	}
}