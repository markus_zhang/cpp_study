#pragma once
#include <iostream>

class base
{
	friend class friend_base;
private:
	int mem_private;
public:
	base() : mem_private(255)	{}
	void memfunc_public()
	{
		std::cout << "I'm public" << std::endl;
	}
	virtual void virtual_test() {}
protected:
	double memfunc_protected()
	{
		return 0.02 * 5.06;
	}
};

class derived_public : public base
{
private:
	int dp_private;
protected:
	int dp_protected;
public:
	void padd()
	{
		//	std::cout << mem_private << std::endl;	// not possible as it's private
		std::cout << memfunc_protected() << std::endl;
		memfunc_public();
	}

	void derived_public_test()
	{
		std::cout << "Derived Public Test" << std::endl;
	}
};

class derived_private : private base
{
public:
	void padd()
	{
		//	std::cout << mem_private << std::endl;	// not possible as it's private
		std::cout << memfunc_protected() << std::endl;
		memfunc_public();
	}
};

class derived_protected : protected base
{
public:
	void padd()
	{
		//	std::cout << mem_private << std::endl;	// not possible as it's private
		std::cout << memfunc_protected() << std::endl;
		memfunc_public();
	}
};

class friend_base
{
public:
	void call_friend()
	{
		base b;
		std::cout << b.mem_private;	// May access a private member
	}

	void call_friend_derived()
	{
		derived_public d;
		d.memfunc_public();	// May access the base members of a derived class (note that friend_base is NOT a friend of class derived_public)
		d.derived_public_test(); // Can also access the derived class' public members
		int i = d.mem_private;	//	Can also access the private member of base class through the derived class
		//	std::cout << d.dp_private;		// CANNOT access the derived class' private members
		//  std::cout << d.dp_protected;	// CANNOT access the derived class' protected members

		std::cout << "Privately inherited class" << std::endl;
		derived_private dp;
		//	dp.memfunc_public();	// Compile error
	}
};

class friend_derived : public friend_base
{
public:
	void call_friend()
	{
		base b;
		//	std::cout << b.mem_private;	// Friend CANNOT be inherited
	}
};

class derived_virtual_test : public base
{
	void virtual_test(int a, int b)
	{
		std::cout << "This is NOT an overloaded virtual_test" << std::endl;
	}
};