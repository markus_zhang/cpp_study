#### SELECT Scenarios

- **Scenario 1**
	
  Get the makers who produce only one product type and more than one model. Output: maker, type.

  >The database scheme consists of four tables:
	
  >Product(maker, model, type)

  >PC(code, model, speed, ram, hd, cd, price)
	
  >Laptop(code, model, speed, ram, hd, screen, price)

  >Printer(code, model, color, type, price)

  *Solution:*

  ```SQL
  SELECT maker, type
  FROM
  (
	  SELECT DISTINCT maker, type
	  FROM Product
  ) AS X

  WHERE maker IN
  (
	  SELECT maker
	  FROM
	  (
		  SELECT DISTINCT maker, model
		  FROM Product
	  ) AS Y
	  GROUP BY maker
	  HAVING COUNT(model)>1
  )
  GROUP BY maker
  HAVING COUNT(type)=1
  ```

  Points to mentions:
	
  1) Must use `WHERE` **before** `GROUP BY` and `HAVING`

  2) Analysis of the case:

	  We are targetting a single table;

	  We want the `INTERSECT` between two sub queries

	  For sub query No.1 we want somehow to count the number of `type`, grouped by `maker`, and only show makers that have 1 type of products. That is, we need to transform the original table:

	  ![Scen01 00](Images/SQL_Scenario/Scen01_00.png)

	  to:

	  ![Scen01 01](Images/SQL_Scenario/Scen01_01.png)

	  ```SQL
	  SELECT maker, type
	  FROM
	  (
		  SELECT DISTINCT maker, type
		  FROM Product
	  ) AS X


	  GROUP BY maker
	  HAVING COUNT(type)=1
	  ```

	  For sub query No.2 we want to count the number of `model`, again grouped by `maker`, and only show makers that make at least 2 models of any product. That is, we need to get this:

	  ![Scen01 02](Images/SQL_Scenario/Scen01_02.png)

	  ```SQL
	  SELECT maker
	  FROM
	  (
		  SELECT DISTINCT maker, model
		  FROM Product
	  ) AS Y
	  GROUP BY maker
	  HAVING COUNT(model)>1

	  ```

	  Then we need an intersection of both sub queries. Because MySQL does not support `INTERSECT`, we will use a `WHERE IN` to bind the two sub queries:

	  ```SQL
	  SELECT maker, type
	  FROM
	  (
		  SELECT DISTINCT maker, type
		  FROM Product
	  ) AS X

	  WHERE maker IN
	  (
		  SELECT maker
		  FROM
		  (
			  SELECT DISTINCT maker, model
			  FROM Product
		  ) AS Y
		  GROUP BY maker
		  HAVING COUNT(model)>1
	  )
	  GROUP BY maker
	  HAVING COUNT(type)=1
	  ```

  3) `WHERE` maker `IN` must be followed by a subquery that `SELECT` exactly the same column, that is `maker`. If we `SELECT` more then it causes an error:

	  ```SQL
	  SELECT maker, type
	  FROM
	  (
		  SELECT DISTINCT maker, type
		  FROM Product
	  ) AS X

	  WHERE maker IN
	  (
		  SELECT maker, model	#ERROR! Cannot SELECT more than maker
		  FROM
		  (
			  SELECT DISTINCT maker, model
			  FROM Product
		  ) AS Y
		  GROUP BY maker
		  HAVING COUNT(model)>1
	  )
	  GROUP BY maker
	  HAVING COUNT(type)=1
	  ```

  4) Note that `DISTINCT` will only check the fields after `SELECT`		
- **Scenario 2**

  Get all ship classes of Russia. If there are no Russian ship classes in the database, display classes of all countries present in the DB.

  Result set: country, class.
	
  Solution:

  ```SQL
  SELECT country, class
  FROM Classes
  WHERE
  (
	  SELECT COUNT(country)
	  FROM Classes
	  WHERE country = 'Russia'
  ) = 0

  UNION

  SELECT country, class
  FROM Classes
  WHERE country = 'Russia'
  ```
  The trick here is to use `COUNT()` to count the number of rows returned, and use that as a condition. If the condition is satisfied, then the whole table will be outputed, then `UNION` the second result which actually returned nothing. If the condition is not satisfied, then the first `SELECT` will not show anything, and `UNION` shows the second `SELECT`, which is exactly what we want.