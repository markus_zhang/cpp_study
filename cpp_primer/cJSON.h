#pragma once

#include "cJSONValueNode.h"
#include "cJSONArrayNode.h"
#include "cJSONObjectNode.h"

std::shared_ptr<JSONObjectNode> ParseObject(const std::string& k, const std::string& jsontext, long& index);

std::string ParseKey(const std::string& jsontext, long& index);

std::shared_ptr<JSONValueNode> ParseValue(const std::string& k, const std::string& jsontext, long& index);

std::shared_ptr<JSONArrayNode> ParseArray(const std::string& k, const std::string& jsontext, long& index);