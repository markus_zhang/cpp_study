#include <iostream>
#include "libString.h"
#include "libIO.h"
#include "tokenizer.h"

void Run(std::string);
void TestTokenizer(const std::string&);

int main(int argc, char* args[])
{
	//	std::cout << strlib::IsStringFloat(".-9909");
	//	Run("c:/test.xsm");
	//	int offset = 0;
	//	std::cout << "**" << libString::GetNextToken("	 Mov	X, 80	", offset) << "**";
	std::string source = libIO::LoadSource("c:/test.xsm");
	TestTokenizer(source);
	return 0;
}

void Run(std::string source)
{
	//	Entry point for the loop
	std::vector<std::string> instrctionStream;
	std::string test = libIO::LoadSource(source);
	std::cout << "Number of lines: " << libString::getNumLines(test) << std::endl;
	libIO::SeperateSource(test, instrctionStream);
	for (auto str : instrctionStream)
	{
		libString::CreateInStructFromLine(str);
	}
}

void TestTokenizer(const std::string& source)
{
	tokenizer tz;
	while (true)
	{
		_tokenType tokenType = tz.GetNextTokenTypeNew(source);
		if (tokenType == END_OF_TOKEN_STREAM)
		{
			std::cout << "End of Stream" << std::endl;
			break;
		}
		switch (tokenType)
		{
		case TOKEN_TYPE_INT:
			std::cout << "Integer" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_FLOAT:
			std::cout << "Float" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_STRING:
			std::cout << "String" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_QUOTE:
			std::cout << "Quote" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_IDENT:
			std::cout << "Identifier" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_COLON:
			std::cout << "Colon" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_OPEN_BRACKET:
			std::cout << "Open Bracket" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_CLOSE_BRACKET:
			std::cout << "Close Bracket" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_COMMA:
			std::cout << "Comma" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_OPEN_BRACE:
			std::cout << "Open Brace" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_CLOSE_BRACE:
			std::cout << "Close Brace" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_NEWLINE:
			std::cout << "New Line" << std::endl;
			break;
		case TOKEN_TYPE_INSTR:
			std::cout << "Instruction" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_SETSTACKSIZE:
			std::cout << "SetStackSize" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_VAR:
			std::cout << "Var" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_FUNC:
			std::cout << "Func" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_PARAM:
			std::cout << "Param" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_REG_RETVAL:
			std::cout << "RetVal" << " | " << " ( " << tz.GetCurrTokenLexeme() << " ) ";
			break;
		case TOKEN_TYPE_INVALID:
			std::cout << "Invalid" << " | ";
			break;
		default:
			std::cout << "Should be an error" << " | ";
			break;
		}
	}
}