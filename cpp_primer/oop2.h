#pragma once
#include <iostream>

class base2
{
public:
	base2()
	{
		v();
	}

	virtual void v()
	{
		std::cout << "Base Virtual Function" << std::endl;
	}
};

class derived2 : public base2
{
public:
	derived2()
	{
		v();
	}
	
	void v()
	{
		//std::cout << "Derived Virtual Function" << std::endl;
	}
};
