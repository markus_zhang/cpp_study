### Designing a Procedual Scripting Language

#### XTremeScript Language Overview

	**Important Features Overview:**

1) Typeless
2) Built-in suppport for strings
3) **no** Pointers
4) **no** `Union` or `Struct`
5) Basic preprocessor support

I. **Data Structure**

	Only single variables and one-dimentional arrays are supported.

	1. Variables are **typeless**, meaning that we can do this:

	```C
	X = true; // Boolean (true and false are built-in XtremeScript keywords)
	X = 16; // Integer
	X = 3.14159; // Floating-Point
	X = "Hello!"; // String
	```
	Variables must be **declared before being used**.
	
	2) **Strings**
	
	Strings can be accessed as both a variable and an array:

	```C
	X = "ABC";
	Y = "DEF";
	Y = X [ 1 ]; // Y now equals "B"
	```

	3) **Arrays**
    
	Very similar to `C` arrays

	Must be declared as array to be used:

	```C
	var X;
	X [ 3 ] = "Hello!"; // Not allowed, X was never declared as an array
	var Y [ 16 ];
	Y [ 3 ] = "Hello!"; // No problem, X was declared as an array
	```

	Because we can put any variable into an array, it's easy to simulate a `C` `Struct` with an `ExtremeScript` array:

	```C
	// We have a C struct
	struct MyStruct
	{
	bool X;
	int Y;
	float Z;
	}
	
	MyStruct Foo;
	
	Foo.X = true;
	Foo.Y = 32;
	Foo.Z = 3.14159;
	```
	In XTremeScript, we use an array:
	```C
	MyArray[3];
	MyArray[0] = true;
	MyArray[1] = 32;
	MyArray[2] = 3.14159
	```
II. **Operator and Expressions**

	Arithmetic Operators

	Bitwise Operators

	Logical and Relational

1) Precedence

	![Chap07 00 Operator Precedence](Images/Script/Chap07_00_Operator_Precedence.png)

III. **Code block**

Like `C`, no need of curly brackets if `if` and `while` only contain one line of code.

However, must use curly brackets for functions regardless of # of lines.

IV. **Control Structures**

	Branching:

	```C
	if ( Expression )
	{
		// True
	}
	else
	{
		// False
	}
	```

	Iteration:

	```C
	while ( Expression )
	{
		// Loop
	}
	```
	```C
	for ( Initializer; Terminating-Condition; Iteration)
	{
		// Loop
	}
	```

	We also need `break` and `continue`

V. **Functions**

	Functions are declared with `func` keyword:

	```C
	func Add ( X, Y )
	{
	return X + Y;
	}
	```

	Remember that XTremeScript is *typeless*, and there is no return type. There is also no need to declare the type of parameters.

	Instead of decalring *Function Prototypes* (like in `C++`), or completely forbidding using functions before their declaration (like in `C`), XTremeScript uses multiple passes.

	The first pass will build a list of functions. The second pass performs the compilation and safely allow any function in the list to be called.

VI. **Escape Sequences** and **Comments**

	We only have two escapes:

	To escpae double quote and to escape backslash itself

	As in C++ and C, we support `//` as well as `/* */`

VII. **Preprocessor**

  We support the following preprocessor:

  `#include`

  `#define`: Watered down version, only used to define variables.

#### Reserved Word List

  Similar to C/C++ but much shorter:

  ![Chap07 01 Reserved Word](Images/Script/Chap07_01_Reserved_Word.png)