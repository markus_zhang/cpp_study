#include "cJSONObjectNode.h"

void JSONObjectNode::push_back(std::shared_ptr<JSONNode> node)
{
	jsonObject.push_back(node);
}

const std::shared_ptr<JSONNode>& JSONObjectNode::operator[](const std::string& key)
{
	return jsonObject[0];	//	Placeholder
}