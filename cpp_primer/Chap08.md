### Assembly Language Primer

#### How Assembly Works

![Chap08 00 A S M Sample](Images/Script/Chap08_00_ASM_Sample.png)

Note that **Instruction** and **Operands** are equally important.

- Use `JMP` and other jumps to simulate branching and iteration:

  1) Branch
  
  ```C++
  if (X > Y)
  {
    // Do LabelA
  }
  else
  {
    // Do LabelB
  }
  ```

  In Assembly:

  ```ASM
    CMP X, Y
    JLE LabelB
  LabelA:
    //...
    JMP SkipB
  LabelB:
    //...
  SkipB:
  ```

  2) Iteration

  ```C++
  for (i = 1; i <= 100; i += 1)
  {
    // Loop
  }
  ```

  In Assembly:

  ```ASM
    MOV i, 1
  Loop:
    CMP i, 100
    JG  Exit
    //  Other stuffs
    ADD i, 1
    JMP Loop
  Exit:
    //  Exit loop
  ```

  A little more efficient:

  ```ASM
    MOV i, 1
  Loop:
    //  Other stuffs
    ADD i, 1
    CMP i, 100
    JLE Loop
  Exit:
    //  Exit loop
  ```

  However the second code block executes at least one loop while the first one doesn't if i > 100 at the beginning.

- **Mnemonics versus Opcodes**

  A few problems:

  First, need to convert mnemonics instructions into opcodes:

  For example:

  ```Assembly
  Mov X, Y
  Add X, Z
  Div Z, 2
  ``` 

  The instructions are converted to Opcodes:

  ```
  0	  X, Y
  1	  X, Z
  4	  Z, 2
  ```

  Next, since we already know the number of variables in compilation, so we can assign numeric values to these variables as well:

  ```
  0	  0, 1
  1	  0, 2
  4	  2, 2
  ```

  Note that the variable Z and literal 2 shares the same number. How to distinguish these will be solved later.

  In the final step, we remove comma and space, they are just for humans:

  `001102422`

  This is actually the **byte code** we talked about.

- **Registers** and **Stack**

  We only have one Register that we will talk about later.

  The *runtime stack* is closely related to function calls. Imagine that we have a function A that calls function B, when B finishes its job, it has to go back to function A, and we push the address of the next insturction *after* the function call into the runtime stack. When the function call completes, this *return address* will be popped from the runtime stack and get executed.

  We use `Call` to call functions and `Ret` to return to callers. They will take care of the push-pop job related to runtime stack.

  Next we need to consider the parameters and return value. How shoud we *pass* parameters to callee functions and returned value to caller functions?

  **How to push parameters and return address onto runtime stack**

  Assuming that we call the following function:

  `Func MyFunc(X, Y, Z)`

  We can push the parameters and return address in the following order:

  ```
  1st -> Parameter X
  2nd -> Parameter Y
  3rd -> Parameter Z
  4th -> Return Address
  ```

  Remember that the runtime stack is a **FILO** data structure, so if we look at the stack we will see this:

  ```
  Return Address  <- Top
  Parameter Z
  Parameter Y
  Parameter X     <- Bottom
  ```

  Now we have a problem: **We cannot pop the stack**. We cannot access the elements of the runtime stack and pop, as we usually do with stacks, because **the return address is at the top** and we do not want to lose it until the end of the function call.

  So we use *relative stack indices* to fetch elements that are NOT at the top. The callee function will read the parameters in order Z, Y and X, thus the importance of left-right/right-left parameter passing.

  At the end of function call, we need to access the *return address* and pop off it and three parameters. We let `Ret` to do all the work:

  `Ret 3`
  
  This instruction tells the caller to access the return address and pop off 3 parameters. (TBF it's 4 in total as we also need to pop the return address)

  **How to return value to caller function**

  We forgot one thing, i.e. the return value. Of course a function may return nothing, but what if they must.

  It's difficult to push the return value onto the runtime stack, because we have to push it **after** the parameters and the return address, which makes it difficult to access the return address at the end of function call. Remember that **the return address is always the first data we want to fetch** at the end of function call.

  Instead, we will use a **register** for this issue, similar to what 80X86 does. And we will use a `MOV` to fill the register with the return value.

- **Stack Frame**

  To wrap up, we need to keep every piece of information for a function call:

  - Return Address
  - Parameters
  - Return value
 
  To keep everything clean, we will use a special data structure to hold the information. This is called **stack frame**, or **activation record**.

- **Local Variables and Scope**

  We will also store the local variables on the runtime stack as well. The *Stack Frame* now will hold pretty much everything about the function.

  Because we only have access to the locals **after** we call the callee function, natually we push them **after** we push the parameters and return address (remember that we push the return address after the parameters so it is at the top of the stack frame, **if we ignore the locals**), and natually they are closer to the top, in reverse order (compared to order of push).

  ```
  Func MyFunc(Param0, Param1)
  {
    var Local0;
    var Local1;
    var LocalArray[4];
  }
  ```

  The complete *stack frame* of the above function will look similar to this:

  Note the difference of indices between counting from Top and from Bottom. We usually count from the top, because we pop from the top. However we will count from Bottom for *Globals*.

  Also note that parameters are pushed **from right to left**, so that when we access them, we access them in the **same order** as in the original program, first `Param0` and next `Param1`.

  ```
  LocalArray[3]   <-- -1 (Top)    <-- 9
  LocalArray[2]   <-- -2          <-- 8
  LocalArray[1]   <-- -3          <-- 7
  LocalArray[0]   <-- -4          <-- 6
  Local1          <-- -5          <-- 5
  Local0          <-- -6          <-- 4
  Return Address  <-- -7          <-- 3
  Param0          <-- -8          <-- 2
  Param1          <-- -9 (Bottom) <-- 1
  ```

  The last piece of puzzle is the **Globals**, and we will store them at the **bottom** of the stack.

  We can see that **Variables** are just symbolic names that are easier for humans to read and write, but in reality they are just **indices relative to the Top/Bottom of the runtime stack**.

#### Introduction to XVM Assembly

- XVM Assembly is **Typeless**

  Because XTremeScript is typeless, XVM Assembly will be typeless. You will see code like this:

  ```ASM
  Mov MyInt, 16384
  Mov MyFloat, 123.456
  Mov MyString, "The terrible secret of space!"
  ```

  This is definitely more flexible than real life Assembly language.

- XVM supports array indexing
  
  Note that we do **NOT** support dynamically allocated memory. Array indexing is like this:

  ```
  MOV X, MyArray[Y]
  ```

  This is also different than real life Assembler Languages.

- **XVM Instruction Set**

1) **Memory**

    `Mov  Destination, Source`

    **Notes**: It's actually a `Copy`, rather than `Move`. So this instruction will result in two instances of `Source`.

    **Operands**: `Destination` must be a memory reference, as we cannot assign a literal to another literal. `Source` can be anything from a literal to a memory reference. `Destination` is essentially a **L-Value** in C++.

2) **Arithmetic**

    ```Assembly
    Add Destination, Source
    Sub Destination, Source
    Mul Destination, Source
    Div Destination, Source
    Mod Destination, Source
    Exp Destination, Power
    Neg Destination
    Inc Destination
    Dec Destination
    ```
  
    Again, `Source` could be anthing, literal or memory reference, but `Destination` must be a memory reference.

    `Inc` is similar to `++` and `Dec` is similar to `--` in C++.

3) **Bitwise**

    ```Assembly
    And Destination, Source
    Or  Destination, Source
    Xor Destination, Source
    Not Destination, Source
    ShL Destination, ShiftCount
    ShR Destination, ShiftCount
    ```

    `ShL` is shift left and `ShR` is shift right

4) **String Processing**

    ```Assembly
    Concat  String0, String1
    GetChar Destination, Source, Index
    SetChar Index, Destination, Source
    ```

    XTremeScript has built-in support for strings. Note that `SetChar` sets the character in `Destination` and `Index` to `Source`.

5) **Conditional Branching**

    ```MASM
    Jmp Label
    JE  Op0, Op1, Label
    JNE Op0, Op1, Label
    JG  Op0, Op1, Label
    JL  Op0, Op1, Label
    JGE Op0, Op1, Label
    JLE Op0, Op1, Label
    ```

    We do not need `Cmp` as we put the comparison into each instruction. For example:

    `JGE Op0, Op1, Label`

    This instruction will jump to `Label` if `Op0` >= `Op1`.

6) **Stack Interface**

    ```
    Push  Source
    Pop   Destination
    ```

    The *runtime stack* is vital to the VM. In addition to serving function calls and storing locals and globals, it will also hold temporary values when evaluating expressions such as:

    `(X + Y) * (X ^ 4) - Cos(Z)`

    Please note that `Pop` not only removes the top element from the runtime stack, it also **stores it in `Destination`**.   

7) **Function Interface**

    ```
    Call      FunctionName
    Ret
    CallHost  FunctionName
    ```

    The `Call` instruction **pushes the return address onto the runtime stack**, the makes a jump to the function's entry point.

    `Ret` is actually **optional** in most of the cases and we will show it later.

    `CallHost` is to call the host API functions and this is how XTremeScript interact with host API.

8) **Miscellaneous**

    ```
    Pause Duration
    Exit  Code
    ```

    `Duration` is in milliseconds. `Pause` will **Not** stop the host application (I don't know why though).

    In `Exit`, we supply the host application a numeric return, `Code`. Again, like `Ret`, `Exit` **is not needed** in most of the cases.

#### XASM Directives

   The Directives provides the Assembler with extra information to help it complete its job. For example the compiler still needs to know the **size of the runtime stack**, the **name of variables and functions**, etc.

- **Stack and Data** 

  `SetStackSize 1024`

  `var MyVar`

  `var MyArray[100]`

  Remember that we do not allow dynamic arrays.

  Also, variables and arrays declared **outside of any function** are considered to be **global**.

- **Functions**

  Here is a sample of function:

  ```
  Func Add
  {
    Param Y
    Param X
    Var Sum
    Mov Sum, X
    Add Sum, Y
    Mov _RetVal, Sum
  }
  ```

  There are a few observations:

  1) The only way to declare *parameters* is by using `Param`. Unlike XTremeScript, XVM Assembly does not allow syntaxes like `Func foo(Param01)`. Using `Param` we know the *name* as well as the *number* of parameters pushed onto the runtime stack.

  2) We use curly brackets to contain the function, which is same as in XTremeScript.

  3) We use `_RetVal`, the register to store the return value.

  4) We do **NOT** use `Ret`. We only need to use `Ret` when we deliberately want it, e.g. in a branch.

- **Escape Sequences**

  We use backlash to escape double quotes (e.g. for quoted conversations):

  ```
  Mov Quote, "General:\"Troops! Mobilize!\""
  ```

  We also use backlash to escape itself (e.g. for file path):

  ```
  Push  "C:\\Gamedev\\Resources\\Troop.bmp"
  ```

- **Comments**

  Unlike in XTremeScript, we use `;` for commenting in XVM Assembly.