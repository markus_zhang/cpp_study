/*

//#include <SDL.h>
#include <iostream>
#include <vector>

void func1(const int i);
void func2(int i);
void func3(const int& i);
void func4(int& i);
void arrfunc1(int* arr);
void arrfunc2(int arr[]);
void arrfunc3(const int arr[]);
//	void arrfunc4(const int (&arr)[]);	//	Must have known bound
void arrfunc4(const int* arr);
void arrfunc5(int (*arr)[3][4]);				//	Two dimensional arrays
std::vector<std::string> example();				//	List initialized return

//	Function overloading
struct blah {};
int foo(int i, double d);
int foo(int i);
int foo(const int& i);
int foo(const int i);							//	Legal but same as const is top-level
int foo(int* i);
int foo(const int* i);							//	Real overloading as const is NOT top-level
//	std::string foo(int i, double d);			//	Differ ONLY in return type does not work
//	const int* foo(int i);						//	Same error
std::string foo(int i, double d, std::string s);
std::string foo(int i, double d, char* s);
int foo(blah bl);
int foo(const blah bl);							//	Legal but same to the above
int foo(const blah& bl);						//	Legal and NOT same to the above two

//	Defualt parameters
//	void defpara(int, double, char ch = 'K');
//	void defpara(int, double d = 2.4, char);

int new_sz() { return 42; }

//	Function Matching
void funcmatch1(short i);
void funcmatch1(int i);


int main(int argc, char* args[])
{
	//	Const in parameters
	std::cout << "Const in parameters" << std::endl;
		//	1) const int i parameter with literal value
		func1(100);
		//	2) const int i parameter with variable
		int i = 200;
		func1(i);						//	it can take a non-const as parameter
		std::cout << i << std::endl;
		//	3) const int i parameter with const variable
		const int j = 1024;
		func1(j);
		//	4) const int i parameter with variable of other numerical types
		double k = 1.24;
		func1(k);						//	it can also take double and do implicit conversion
		//	5) const int i parameter with non-numerical types
		char p = 'K';
		func1(p);						//	p is indeed an ASCII value
		std::string s = "1234";
		//	func1(s);					//	ERROR: No conversion can be made
	//	Non-const parameters
	std::cout << "Non-const in parameters" << std::endl;
		//	1) can take literal arithmetic values
		func2(100);
		func2(100.23);
		func2('F');
		func2('A' + 'D');				//	even this is OK
		//	2) can take variables
		func2(p + i);					//	p and i are of different types, still OK
		//	3) can take const variables
		func2(j);						//	just make a copy so it's OK
		const char c1 = 'L';
		func2(c1);
	//	Const reference parameters - best to use when the function does not modify the value
		std::cout << "Const reference parameters" << std::endl;
		//	1) can take non-const variables
		func3(i);						//	It works, because func3 creates a const reference to the mutable i
										//	https://stackoverflow.com/questions/9127168/c-function-pass-non-const-argument-to-const-reference-parameter
		//	2) can take literals
		func3(1000);
		//	3) can take const variables
		func3(c1);
	//	Non-const reference parameters - only use this when the function needs to modify the value
		std::cout << "Non-const reference parameters" << std::endl;
		//	1) cannot take literals (rvalue)
		//	func4(100);					//	Cannot take r-value
		//	2) can take non-const variables
		func4(i);
		//	func4(i + k);				//	Not possible as i + k is r-value e.g. we cannot write x + y = 4
		//	3) cannot take const variables
		//	func4(c1);					//	Not possible

	//----------------------------------------------------------------------------------------------------
	//	Array as parameter
		std::cout << "Array parameters" << std::endl;
		//	Array as pointer
		int iarr[] = { 1,2,3,4 };
		arrfunc1(iarr);
		std::cout << iarr[0] << std::endl;

		//	Non const Array
		arrfunc2(iarr);
		std::cout << iarr[0] << std::endl;

		//	Const reference/pointer to array

		//	Multiple Dimension Arrays
		int iarr2[][4] = { {1,2,3,4},{5,6,7,8},{9,10,11,12} };
		int(*piarr2)[3][4] = &iarr2;
		arrfunc5(piarr2);
		std::cout << iarr2[0][0] << std::endl;

	//	Return a list initialized object
		std::vector<std::string> vs = example();

	//--------------------------------------------------------
		std::cout << "Default parameters" << std::endl;
		//defpara(10, 2.4);

	//	Constexpr
		
		const int cfoo = new_sz();
	//	Function Matching
		char chfm = 'A';
		short sfm = 10;
		double dfm = 2.34;
		funcmatch1(chfm);
		funcmatch1(sfm);
		//	funcmatch1(dfm);		//	Compiler complains about ambiguous call

	//	Functional Pointer
		std::cout << "Pointer to Function" << std::endl;
		void(*pf)(const int);
		void(*pf2)(const int);
		pf = func1;
		pf2 = &func1;				//	Equivalent to previous line
		pf(100);
		pf2(100);

		

	return 0;
}

void func1(const int i)
{
	std::cout << i + 1 << std::endl;
}

void func2(int i)
{
	i += 300;
	std::cout << i + 1 << std::endl;
}

void func3(const int& i)
{
	//	i += 300;						//	Not possible
	std::cout << i + 1 << std::endl;	//	OK as i + 1 does not assign a new value to i
	//	std::cout << i++ << std::endl;	//	Not possible as i== actually changes i
}

void func4(int& i)
{
	i += 300;
	std::cout << i + 1 << std::endl;
}

void arrfunc1(int* arr)
{
	arr[0] = 100;
}

void arrfunc2(int arr[])
{
	arr[0] = 100;						//	Implicitly converts to pointer
}

void arrfunc3(const int arr[])
{
	//	arr[0] = 100;					//	Not possible
}

void arrfunc4(const int* arr)
{
	//	arr[0] = 100;					//	Again not possible
}

void arrfunc5(int(*arr)[3][4])
{
	(*arr)[0][0] = 100;
}

std::vector<std::string> example()
{
	return{ "yes", "no", "null" };
}

void funcmatch1(short i)
{
	std::cout << "This is a short" << std::endl;
}

void funcmatch1(int i)
{
	std::cout << "This is an integer" << std::endl;
}

*/