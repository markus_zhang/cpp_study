#pragma once
#include <algorithm>
#include <fstream>
#include <unordered_map>
#include "header.h"
#include "libString.h"

enum _tokenizer_error
{
	TOKENIZER_ERROR_NONE,
	TOKENIZER_ERROR_MISSING_END_QUOTE
};

class tokenizer
{
public:
	tokenizer() : inString(false), newLine(false), offsetBegin(0), offsetEnd(0), hasError(false), error(TOKENIZER_ERROR_NONE), ignoreEndingQuote(false), isEndingQuote(false) {}

	_tokenType GetNextTokenType(const std::string& input);
	_tokenType GetNextTokenTypeNew(const std::string& input);
	void SkipWhiteSpace(const std::string& input);
	int ContinueInString(const std::string& input);
	std::string GetCurrTokenLexeme();
	bool IsInstruction(const std::string& input);
	void DisplayError();
private:
	_token token;
	std::unordered_map<std::string, int> insTable;
	bool inString;
	bool newLine;
	bool ignoreEndingQuote;
	bool isEndingQuote;	//	This is to capture the ending quote, otherwise there is no way to distinguish
	int offsetBegin;
	int offsetEnd;
	bool hasError;
	_tokenizer_error error;

	//	Private functions
	void InitialInsTable(const std::string filename);
	void DisplayInsTable();
};

void tokenizer::InitialInsTable(const std::string filename)
{
	//	Read filename and dump the content into insTable
	//	Sample:
	//	MOV 0
	//	ADD 1
	std::ifstream infile("c:/inslist.txt");
	std::string insName;
	int insNumber;
	while (infile >> insName >> insNumber)
	{
		insTable.emplace(insName, insNumber);
	}
}

void tokenizer::DisplayInsTable()
{
	for (auto insPair : insTable)
	{
		std::cout << insPair.first << " " << insPair.second << std::endl;
	}
}

_tokenType tokenizer::GetNextTokenType(const std::string& input)
{
	if (offsetEnd > input.length() - 1)
	{
		return END_OF_TOKEN_STREAM;
	}

	token.tokenType = TOKEN_TYPE_INVALID;
	token.tokenLexeme = "";
	offsetBegin = offsetEnd;

	//	Skip whitespaces (e.g. the whitespace after the comma in MOV X, 12345) to reach real meat
	//	ONLY when we are NOT in a string!!
	while (offsetEnd <= input.length() - 1)
	{
		if (libString::IsCharWhitespace(input[offsetEnd]) & inString == false)
		{
			offsetEnd += 1;
			offsetBegin = offsetEnd;
		}
		else
		{
			break;
		}
	}

	//	Loop until a token has been extracted, or at end of file
	//	At the end, under any condition, offsetEnd should be ata position that it's correct to ignore the char it points to
	//	That is, for every break in the loop, we need to make sure that the Lexeme contains the characters
	//	from offsetBegin (included), to offsetEnd (excluded)

	while (offsetEnd <= input.length() - 1)
	{
		//	Fuck let's take care the string case first
		//	To complicated to scatter the logic everywhere

		if (inString)
		{
			while (input[offsetEnd] != '"')
			{
				offsetEnd += 1;
			}
		}

		//	We need to take care of the line break case first
		//	Then we will take care of the delimiter cases (multiple)

		//	Line break case: If we are in string, it's an error
		if (libString::IsCharLineBreak(input[offsetEnd]))
		{
			if (inString)
			{
				//	ERROR: Should expect a QUOTE
				error = TOKENIZER_ERROR_MISSING_END_QUOTE;
				//	DisplayError();	// Move to the caller function, tokenizer should only extract tokens
				return TOKEN_TYPE_INVALID;	//	The only non-break case because we found an error
			}
			else
			{
				//	If not in string, check if we are parsing a token
				if (offsetEnd - offsetBegin >= 1)
				{
					//	This token is fully extracted, the lexeme should already be consumed (since we consume by char)

					//	Note that offsetEnd is at line break, so next Token will be the line break itself
					break;
				}
				else
					//	Not in string, Not parsing a token, just a line break
					//	In this unique case we need to move the offsets, otherwise we cannot continue
				{
					//	token.tokenType = TOKEN_TYPE_NEWLINE;
					offsetEnd += 1;
					offsetBegin = offsetEnd;
					return TOKEN_TYPE_NEWLINE;	//	Need to test this line...
					break;
				}
			}
		}

		//	Delimiter cases, very similar to line break as line break IS a delimiter but we singled it out
		//	Note that we do not need to keep lexemes for delimiters, no one needs them

		//	Damn I might need to re-write this part because whitespace is also related to inString
		//	If it's a delimiter but we are parsing a string, we should keep going, right?

		if (libString::IsCharDelim(input[offsetEnd]))
		{
			//	If in string, then check if it's a quote
			if (input[offsetEnd] == '"')
			{
				if (inString)
				{
					//	Lexeme was already grabbed by both offsets
					//	token.tokenType = TOKEN_TYPE_STRING;
					//	inString = false;	// We will set it after we grab the string, otherwise how to identify?

					//	Note that offsetEnd is at the QUOTE, so next TOKEN should be the QUOTE itself
					//	However the tricky part is, we need a way to identify this QUOTE as ENDING QUOTE
					ignoreEndingQuote = true;
					break;
				}
				else
				{
					if (ignoreEndingQuote)
					{
						//	This is the ending quote, we are NOT taking a string but just the QUOTE itself
						//	token.tokenType = TOKEN_TYPE_QUOTE;
						ignoreEndingQuote = false;
						//	Reason we need isEndingQuote is, when we finish parsing the string
						//	We will come back to parse the ending quote.
						//	So inString == false and ignoreEndingQuote == false
						//	However if we look at the part that we parse a QUOTE
						//	If we do not have isEndingQuote, and by ignoreEndingQuote alone
						//	We would recognize the ending quote as the opening quote (because NOT ignore ending quote)
						//	So we need another flag to tell us this is the ending quote
						isEndingQuote = true;
						offsetEnd += 1;	//	offsetEnd may move to something meaningful but we always ignore the last char
						break;
					}
					else
					{
						//	This is an opening quote, we are just taking the QUOTE itself
						//	token.tokenType = TOKEN_TYPE_QUOTE;
						//	inString = true;	// Only difference from the other branch
						offsetEnd += 1;	//	offsetEnd may move to something meaningful but we always ignore the last char
						break;
					}
				}
			}
			else
			{
				//	Not a QUOTE and not a line break
				//	Three cases:
				//	Case 0: Parsing a string, whatever should be included in the string
				//if (inString)
				//{
				//	//	Fuck I don't want to use a continue here, but I cannot break
				//	//	If we break then it won't collect all characters of the string
				//	//	Need to figure out a way to do this cleanly


				//}
				//	Case 1: we are parsing a token, like "myVAR,"
				if (offsetEnd - offsetBegin >= 1)
				{
					if (!inString)
					{
						break;
					}
				}
				else
				//	Case 2: we are parsing the delimiter, like " ,"
				{
					offsetEnd += 1;
					if (!inString)
					{
						break;
					}
				}
			}
		}
		else
		{
			//	Move forward but should not break because we haven't met a delimiter
			offsetEnd += 1;
		}
	}

	//	Now offsetEnd has been located, we need to extract
	//	Remember to include offsetBegin and exclude offsetEnd
	//	No need to move offsetBegin because by the next time this func is called
	//	offsetBegin will be automatically set to offsetEnd

	token.tokenLexeme = input.substr(offsetBegin, offsetEnd - offsetBegin);

	//	First off let's get the string case done, because we might have a string that looks like a delimiter
	if (inString)
	{
		//	Regardless of size
		token.tokenType = TOKEN_TYPE_STRING;
		inString = false;
		return token.tokenType;
	}

	//	For 1 char Lexeme, check for delimiters, and remember to set tokenType because we didn't do so
	if (token.tokenLexeme.length() == 1 && libString::IsCharDelim(token.tokenLexeme[0]))
	{
		switch (token.tokenLexeme[0])
		{
		case '"':
			//	If we are taking an opening quote, we need to set isString here
			//if (!ignoreEndingQuote)
			//{
			//	//	This is the opening QUOTE
			//	token.tokenType = TOKEN_TYPE_QUOTE;
			//	inString = true;
			//}
			//else
			//{
			//	//	This is the ending QUOTE
			//	token.tokenType = TOKEN_TYPE_QUOTE;
			//}
			if (isEndingQuote)
			{
				token.tokenType = TOKEN_TYPE_QUOTE;
			}
			else
			{
				//	This is the opening QUOTE
				token.tokenType = TOKEN_TYPE_QUOTE;
				inString = true;
			}
			break;
		case ',':
			token.tokenType = TOKEN_TYPE_COMMA;
			break;
		case '\n':
			token.tokenType = TOKEN_TYPE_NEWLINE;
			break;
		case ':':
			token.tokenType = TOKEN_TYPE_COLON;
			break;
		case '[':
			token.tokenType = TOKEN_TYPE_OPEN_BRACKET;
			break;
		case ']':
			token.tokenType = TOKEN_TYPE_CLOSE_BRACKET;
			break;
		case '{':
			token.tokenType = TOKEN_TYPE_OPEN_BRACE;
			break;
		case '}':
			token.tokenType = TOKEN_TYPE_CLOSE_BRACE;
			break;
		case ' ':
			//	This is for whitespace
			//	offsetEnd += 1;	// This is already done in Case 2: passing the delimiter
			offsetBegin = offsetEnd;
			break;
		default:
			break;
		}
		return token.tokenType;
	}
	else
	{
		//	We still miss some types here most importantly all the Directives
		if (libString::IsStringInt(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_INT;
			return token.tokenType;
		}
		if (libString::IsStringFloat(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_FLOAT;
			return token.tokenType;
		}
		if (libString::IsStringIdent(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_IDENT;
			return token.tokenType;
		}
	}
}

void tokenizer::SkipWhiteSpace(const std::string& input)
{
	while (libString::IsCharWhitespace(input[offsetEnd]) & inString == false)
	{
		offsetEnd += 1;
		offsetBegin = offsetEnd;
	}
}

int tokenizer::ContinueInString(const std::string& input)
{
	//	0:		Not in string
	//	255:	In string and hits a quote
	//	-255:	In string but hits newline error

	int returnValue = 0;
	if (inString)
	{
		while (true)
		{
			if (input[offsetEnd] == '"')
			{
				//	Basically we need to put the code instead in the main loop
				//	ignoreEndingQuote = true;	//	Better to leave it for caller
				returnValue = 255;	//	Caller use this return value to break from the main loop
				break;
			}
			//	Put error detection code here instead of main loop
			if (input[offsetEnd] == '\n')
			{
				//	ERROR: Should expect a QUOTE
				error = TOKENIZER_ERROR_MISSING_END_QUOTE;
				hasError = true;
				//	DisplayError();	// Move to the caller function, tokenizer should only extract tokens
				returnValue = -255;	//	The only non-break case because we found an error
				break;
			}
			offsetEnd += 1;
		}
	}
	return returnValue;
}

_tokenType tokenizer::GetNextTokenTypeNew(const std::string& input)
{
	if (offsetEnd > input.length() - 1)
	{
		return END_OF_TOKEN_STREAM;
	}

	SkipWhiteSpace(input);

	token.tokenType = TOKEN_TYPE_INVALID;
	token.tokenLexeme = "";
	offsetBegin = offsetEnd;

	//	Loop until a token has been extracted, or at end of file
	//	At the end, under any condition, offsetEnd should be ata position that it's correct to ignore the char it points to
	//	That is, for every break in the loop, we need to make sure that the Lexeme contains the characters
	//	from offsetBegin (included), to offsetEnd (excluded)

	while ((offsetEnd <= input.length() - 1) && ContinueInString(input) >= 0)
	{
		if (ContinueInString(input) == 255)
		{
			ignoreEndingQuote = true;
			break;
		}
		//switch (ContinueInString(input))
		//{
		//case -255:
		//	return TOKEN_TYPE_INVALID;
		//	break;
		//case 255:
		//	//	Lexeme was already grabbed by both offsets
		//	//	token.tokenType = TOKEN_TYPE_STRING;
		//	//	inString = false;	// We will set it after we grab the string, otherwise how to identify?

		//	//	Note that offsetEnd is at the QUOTE, so next TOKEN should be the QUOTE itself
		//	//	However the tricky part is, we need a way to identify this QUOTE as ENDING QUOTE
		//	ignoreEndingQuote = true;
		//	break;
		//default:	//	0 can be ignored, 255 is already dealt inside ContinueInString
		//	break;
		//}

		//	We need to take care of the line break case first
		//	Then we will take care of the delimiter cases (multiple)

		//	Line break case: If we are in string, it's an error
		if (libString::IsCharLineBreak(input[offsetEnd]))
		{
			//	In String already dealt by ContinueInString(input)
			//	If not in string, check if we are parsing a token

			if (offsetEnd - offsetBegin >= 1)
			{
				//	This token is fully extracted, the lexeme should already be consumed (since we consume by char)

				//	Note that offsetEnd is at line break, so next Token will be the line break itself
				break;
			}
			else
				//	Not in string, Not parsing a token, just a line break
				//	In this unique case we need to move the offsets, otherwise we cannot continue
			{
				//	token.tokenType = TOKEN_TYPE_NEWLINE;
				offsetEnd += 1;
				offsetBegin = offsetEnd;
				return TOKEN_TYPE_NEWLINE;	//	Need to test this line...
				break;
			}
		}

		//	Delimiter cases, very similar to line break as line break IS a delimiter but we singled it out
		//	Note that we do not need to keep lexemes for delimiters, no one needs them

		//	Damn I might need to re-write this part because whitespace is also related to inString
		//	If it's a delimiter but we are parsing a string, we should keep going, right?

		if (libString::IsCharDelim(input[offsetEnd]))
		{
			//	If in string, then check if it's a quote
			if (input[offsetEnd] == '"')
			{
				//if (inString)
				//{
				//	//	Lexeme was already grabbed by both offsets
				//	//	token.tokenType = TOKEN_TYPE_STRING;
				//	//	inString = false;	// We will set it after we grab the string, otherwise how to identify?

				//	//	Note that offsetEnd is at the QUOTE, so next TOKEN should be the QUOTE itself
				//	//	However the tricky part is, we need a way to identify this QUOTE as ENDING QUOTE
				//	ignoreEndingQuote = true;
				//	break;
				//}
				//else
				//{

					//	inString already dealt by ContinueInString() so here we assume we are NOT in string
					
					if (ignoreEndingQuote)
					{
						//	This is the ending quote, we are NOT taking a string but just the QUOTE itself
						//	token.tokenType = TOKEN_TYPE_QUOTE;
						ignoreEndingQuote = false;
						//	Reason we need isEndingQuote is, when we finish parsing the string
						//	We will come back to parse the ending quote.
						//	So inString == false and ignoreEndingQuote == false
						//	However if we look at the part that we parse a QUOTE
						//	If we do not have isEndingQuote, and by ignoreEndingQuote alone
						//	We would recognize the ending quote as the opening quote (because NOT ignore ending quote)
						//	So we need another flag to tell us this is the ending quote
						isEndingQuote = true;
						offsetEnd += 1;	//	offsetEnd may move to something meaningful but we always ignore the last char
						break;
					}
					else
					{
						//	This is an opening quote, we are just taking the QUOTE itself
						//	token.tokenType = TOKEN_TYPE_QUOTE;
						//	inString = true;	// Only difference from the other branch
						offsetEnd += 1;	//	offsetEnd may move to something meaningful but we always ignore the last char
						break;
					}
				//}
			}
			else
			{
				//	Not a QUOTE and not a line break
				//	Three cases:
				//	Case 0: Parsing a string, whatever should be included in the string
				//if (inString)
				//{
				//	//	Fuck I don't want to use a continue here, but I cannot break
				//	//	If we break then it won't collect all characters of the string
				//	//	Need to figure out a way to do this cleanly


				//}
				//	Case 1: we are parsing a token, like "myVAR,"
				if (offsetEnd - offsetBegin >= 1)
				{
					if (!inString)
					{
						break;
					}
				}
				else
					//	Case 2: we are parsing the delimiter, like " ,"
				{
					offsetEnd += 1;
					if (!inString)
					{
						break;
					}
				}
			}
		}
		else
		{
			//	Move forward but should not break because we haven't met a delimiter
			offsetEnd += 1;
		}
	}

	//	Take care of error case first
	if (hasError)
	{

	}

	//	Now offsetEnd has been located, we need to extract
	//	Remember to include offsetBegin and exclude offsetEnd
	//	No need to move offsetBegin because by the next time this func is called
	//	offsetBegin will be automatically set to offsetEnd

	token.tokenLexeme = input.substr(offsetBegin, offsetEnd - offsetBegin);

	//	First off let's get the string case done, because we might have a string that looks like a delimiter
	if (inString)
	{
		//	Regardless of size
		token.tokenType = TOKEN_TYPE_STRING;
		inString = false;
		return token.tokenType;
	}

	//	For 1 char Lexeme, check for delimiters, and remember to set tokenType because we didn't do so
	if (token.tokenLexeme.length() == 1 && libString::IsCharDelim(token.tokenLexeme[0]))
	{
		switch (token.tokenLexeme[0])
		{
		case '"':
			//	If we are taking an opening quote, we need to set isString here
			//if (!ignoreEndingQuote)
			//{
			//	//	This is the opening QUOTE
			//	token.tokenType = TOKEN_TYPE_QUOTE;
			//	inString = true;
			//}
			//else
			//{
			//	//	This is the ending QUOTE
			//	token.tokenType = TOKEN_TYPE_QUOTE;
			//}
			if (isEndingQuote)
			{
				token.tokenType = TOKEN_TYPE_QUOTE;
			}
			else
			{
				//	This is the opening QUOTE
				token.tokenType = TOKEN_TYPE_QUOTE;
				inString = true;
			}
			break;
		case ',':
			token.tokenType = TOKEN_TYPE_COMMA;
			break;
		case '\n':
			token.tokenType = TOKEN_TYPE_NEWLINE;
			break;
		case ':':
			token.tokenType = TOKEN_TYPE_COLON;
			break;
		case '[':
			token.tokenType = TOKEN_TYPE_OPEN_BRACKET;
			break;
		case ']':
			token.tokenType = TOKEN_TYPE_CLOSE_BRACKET;
			break;
		case '{':
			token.tokenType = TOKEN_TYPE_OPEN_BRACE;
			break;
		case '}':
			token.tokenType = TOKEN_TYPE_CLOSE_BRACE;
			break;
		case ' ':
			//	This is for whitespace
			//	offsetEnd += 1;	// This is already done in Case 2: passing the delimiter
			offsetBegin = offsetEnd;
			break;
		default:
			break;
		}
		return token.tokenType;
	}
	else
	{
		//	We still miss some types here most importantly all the Directives
		if (libString::toLower(token.tokenLexeme) == "setstacksize")
		{
			token.tokenType = TOKEN_TYPE_SETSTACKSIZE;
			return token.tokenType;
		}
		if (libString::toLower(token.tokenLexeme) == "var")
		{
			token.tokenType = TOKEN_TYPE_VAR;
			return token.tokenType;
		}
		if (libString::toLower(token.tokenLexeme) == "func")
		{
			token.tokenType = TOKEN_TYPE_FUNC;
			return token.tokenType;
		}
		if (libString::toLower(token.tokenLexeme) == "setstacksize")
		{
			token.tokenType = TOKEN_TYPE_SETSTACKSIZE;
			return token.tokenType;
		}
		if (libString::toLower(token.tokenLexeme) == "param")
		{
			token.tokenType = TOKEN_TYPE_PARAM;
			return token.tokenType;
		}
		if (libString::toLower(token.tokenLexeme) == "_retval")
		{
			token.tokenType = TOKEN_TYPE_REG_RETVAL;
			return token.tokenType;
		}
		//	Instructions
		if (insTable.find(libString::toLower(token.tokenLexeme)) != insTable.end())
		{
			token.tokenType = TOKEN_TYPE_INSTR;
			return token.tokenType;
		}
		/*if (libString::IsStringInt(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_INT;
			return token.tokenType;
		}*/
		if (libString::IsStringFloat(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_FLOAT;
			return token.tokenType;
		}
		if (libString::IsStringIdent(token.tokenLexeme))
		{
			token.tokenType = TOKEN_TYPE_IDENT;
			return token.tokenType;
		}
	}
}


std::string tokenizer::GetCurrTokenLexeme()
{
	return token.tokenLexeme;
}