### Template
- Function Template
  - Template Type Parameters
	
    1) A type parameter can be used to name the **return type** or a function **parameter type**
    ```
	template <typename T> T foo(std::shared_ptr<T> p)
	{
		//Perform tasks
	}
	```

	2) You can have multiple type parameters, but each one must be preceded by the keyword *class* or *typename*
    ```
	template <typename T, class U>
	foo (const U& refu, std::shared_ptr<T> pt)
	{
		//Perform tasks
	}
	```

  - Template Non-type Parameters
    
	**Must be one of the following:**
	1) integral or enumeration type
    ```
	// N and M are integral types
	template <int N, int M>
	int compare(const char (&p1)[N], const char (&p2)[M])
	{
		return strcmp(p1, p2);
	}
	```

	2) pointer to object or pointer to function
    ```
	template <std::string* blah>
	void foo()
	{

	}
	```
	Note that smart pointers do not work:
	```
	template <std::shared_ptr<std::string> blah>
	void foo2()
	{

	}
	```

	3) lvalue reference to object/function
    ```
	template <std::string& blah(int)>
	void foo3()
	{

	}	
	```

	4) pointer to member

	5) ```std::nullptr_t```
	```
	template<std::nullptr_t blah>
	void foo4()
	{

	}
	```
	
  - Why must keep implementation of template functions in header file:
	See this back at home:
	https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file

	Basically the reason is that the compile needs to create a new class when instantiating (e.g. when it knows which type you need)

	Because it needs all info to create a new class, including all the member functions, it has to know everything about the template class

	Thus it is generally good practice to put definition in the header file as well

	A side effect is that most of the errors won't be know until instantiation, i.e. in linker

- Class Template
  - Remember to add <T> after class name when implementing the member functions
	```
	void Blob<T>::check()
	{
	}
	```

  - Template and Friend
	I don't quite get this topic, maybe check:
	https://web.mst.edu/~nmjxv3/articles/templates.html
	And also here:
	https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Making_New_Friends
	And here:
	https://stackoverflow.com/questions/18792565/declare-template-friend-function-of-template-class
	
	From my understanding the usual approach is to declare the class and function first, and ```friend``` the function inside of the class with a <T> signature:
	```
	template <typename T>
	class Boo;

	template <typename T>
	void ffunc3(T&);

	template <typename T>
	class Boo
	{
		friend void ffunc3<T>(T&);
	public:
		Boo() = default;
		Boo(T input)
		{
			t = input;
		}

	public:
		T t;
	};

	template <typename T>
	void ffunc3(T& t1)
	{
		std::cout << t1.t << std::endl;
	}	
	```

	Also notice that for operator overloading it's important to get specialized friend:

	```
	template <typename T>
	class Boo
	{
		friend void ffunc3<T>(T&);
	public:
		Boo() = default;
		Boo(T input)
		{
			t = input;
		}
		friend bool operator==<T> (const Boo&, const Boo&);
	public:
		T t;
	};
	```
	Now we definitely do **NOT** want to use ```==``` on two different type ```Boo``` objects

	For non-template class, we can also template function as friend.
	
	Let's say we only want the template function of **specialization of type C to access class C**
	```
	//	Forward declaration
	template <typename T>
	class Pal;

	class C
	{
		friend class Pal<C>;	// Only Pal instantiated with type C can be friend of C

		template <typename T>	//	All instantiation of Pal2 may access C
		friend class Pal2;
	public:
		C() : k(100) {}
	private:
		int k;
	};

	template <typename T>
	class Pal
	{
	public:
		void test(T t)
		{
			std::cout << t.k << std::endl;
		}
	};

	template <typename T>
	class Pal2
	{
	public:
		void test(C c)
		{
			std::cout << c.k << std::endl;
		}
	};	
	```
	Pal can only access C with type C instantiation

	Pal2 can access C regardless of its type
	```
	C ctest;
	Pal<C> palctest;
	Pal<int> palinttest;

	palctest.test(ctest);
	// palinttest.test(ctest); // Not possible
	Pal2<int> pal2inttest;
	pal2inttest.test(ctest);
	```

	Note that although Pal<C> and Pal2 can access C's private members,

	**Users of Pal\<C> and Pal2** cannot do so.
	```
	template <typename T>
	class Pal2
	{
		DisplayInt(const Int& i)
		{
			std::cout << i << std::endl;
		}
	}
	```
	And then we, as **Users** of class Pal2, tries to create a Pal2 object and do this:
	```
	Pal2 p2;
	p2.DisplayInt(c.k); // Not possible
	```

	**Summary**: also true for template class and template class friends

	Basically, if you want to restrict the type of template class friends, you need to:
	1) Forward declaration of the friend class
    2) Do **NOT** use ```template <typename T>``` inside of the befriended class
    
	If you want any type of the friend class to have access,
	1) Do **NOT** forward declaration
    2) Use a different ```T``` (for example use U) in ```template <typename T>``` inside of the befriended class

  - Static member in Template class
  1) Declaration in header file and post-declaration in header file
  2) define in .cpp
	```
	// in template.h
	template <typename T>
	class statclass
	{
	public:
		static int k;
	public:
		void DisplayK()
		{
			std::cout << k << std::endl;
		}
	};

	template <typename T>
	int statclass<T>::k;
	```
	And then in main.cpp
	```
	statclass<int> st1;
	statclass<double> st2;
	statclass<int>::k = 100;
	statclass<double>::k = 999;
	st1.DisplayK(); // display 100
	st2.DisplayK(); // display 999
	```
