#ifndef _HEADER
#define _HEADER

#include <vector>
#include <string>

//	Headers that mimic the structure of the output

//	Enumeration of operand types
enum _opType
{
	TYPE_LITERAL_INTEGER = 0,
	TYPE_LITERAL_FLOAT,
	TYPE_LITERAL_STRING,
	TYPE_INDEX_INSTRUCTION,
	TYPE_INDEX_STACK,
	TYPE_INDEX_STRING_TABLE,
	TYPE_INDEX_FUNCTION_TABLE,
	TYPE_INDEX_HOST_API_FUNCTION,
	TYPE_REGISTER
};

enum _tokenType
{
	TOKEN_TYPE_INT,				//	An integer literal
	TOKEN_TYPE_FLOAT,			//	A floating - point literal
	TOKEN_TYPE_STRING,			//	A string literal value, not including the surrounding quotes. Quotes are considered separate tokens.
	TOKEN_TYPE_QUOTE,			//	A double quote "
	TOKEN_TYPE_IDENT,			//	An identifier
	TOKEN_TYPE_COLON,			//	A colon :
	TOKEN_TYPE_OPEN_BRACKET,	//	An opening bracket[
	TOKEN_TYPE_CLOSE_BRACKET,	//	A closing bracket]
	TOKEN_TYPE_COMMA,			//	A comma,
	TOKEN_TYPE_OPEN_BRACE,		//	An opening curly brace{
	TOKEN_TYPE_CLOSE_BRACE,		//	A closing curly brace }
	TOKEN_TYPE_NEWLINE,			//	A line break
	TOKEN_TYPE_INSTR,			//	An instruction
	TOKEN_TYPE_SETSTACKSIZE,	//	The SetStackSize directive
	TOKEN_TYPE_VAR,				//	A Var directive
	TOKEN_TYPE_FUNC,			//	A Func directive
	TOKEN_TYPE_PARAM,			//	A Param directive
	TOKEN_TYPE_REG_RETVAL,		//	The _RetVal register
	TOKEN_TYPE_INVALID,			//	Error code for invalid tokens
	END_OF_TOKEN_STREAM,		//	The end of the stream has been reached
};

//	Main Header
struct _main_header
{
	std::string id;
	int version;
	int stackSize;
	int globalDataSize;
	bool hasMain;
	int indexMain;

	_main_header() : id("XSE0"), version(4), stackSize(1024), hasMain(false), indexMain(-1) {}
	_main_header(int size) : id("XSE0"), version(4), stackSize(size), hasMain(false), indexMain(-1) {}
};

//	Operand Structure, one instruction may have 0-N of these
struct _operand
{
	_opType operandType;
	int operandData;
	//	Need a union here
	union
	{
		int			indexStack;
		int			indexInstruction;
		int			indexStringTable;
		int			indexFunctionTable;
		int			indexAPICallFunction;
		int			literalInteger;
		float		literalFloat;
		int			reg;
	};
	//	Offset to help converting relative stack index to absolute stack index
	int stackIndexOffset;
};

//	Instruction structure
struct _instruction
{
	int opcode;
	int operandCount;
	std::vector<_operand> op;
};

//	For debugging only, in production we use _instruction
struct _psuedo_instruction
{
	std::string opcode;
	int operandCount;
	std::vector<std::string> op;
};

//	Instruction Stream, this is the big one
struct _instruction_stream
{
	_main_header header;
	std::vector<_instruction> stream;
};

//	Function struct to build stack frame
struct _function
{
	int index;
	int stackEntryPoint;	//	Entry point i.e. the stack index of first instruction of this function
	std::string name;
	int numParams;
	int sizeLocalDara;
};

//	Token
struct _token
{
	_tokenType tokenType;
	std::string tokenLexeme;
};

#endif