#pragma once
#include <iostream>

class base
{
public:
	base() : i(100) {}
	virtual void func()
	{
		std::cout << "i is " << i << std::endl;
	}
	virtual ~base() {}
protected:
	int i;
};

class derived : public base
{
public:
	derived() = default;
	void func() override
	{
		std::cout << "i is " << base::i * 2 << std::endl;
	}
};
