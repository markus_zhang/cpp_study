#include "cJSON.h"

std::shared_ptr<JSONObjectNode> ParseObject(const std::string& k, const std::string& jsontext, long& index)
{
	std::shared_ptr<JSONObjectNode> jObjectNode = std::make_shared<JSONObjectNode>(k);

	//	Check if current char is {
	//	If not then throw an error
	//	Also move index forward
	if (jsontext[index++] != '{')
	{
		throw "A JSON object must start with {";
	}

	//	Check if jsontext[index] is one of the following:
	//	1. }, empty object, just return
	//	2. ,, garbage char, move on
	//	3. Assuming no space/tab, then must be a Json key
	//	4. Must do all of these in a loop
	while(index < jsontext.length())
	{
		switch (jsontext[index])
		{
		case '}':
			return jObjectNode;
			break;
		case ',':
			index += 1;			//	Not sure why do we need this
			break;
		default:
			break;
		}

		//	Now jsontest[index] is not } and ,
		//	Must be a string, otherwise error, parse key
		std::string key = ParseKey(jsontext, index);
		//	ParseKey should automatically set index to the next char after the colon
		switch (jsontext[index])
		{
		case '{':
			jObjectNode->push_back(ParseObject(key, jsontext, index));
			break;
		case '[':
			jObjectNode->push_back(ParseArray(key, jsontext, index));
			break;
		default:
			//	Assuming no error, should be a json value
			jObjectNode->push_back(ParseValue(key, jsontext, index));
			//	ParseValue() put index AFTER the actual value string
			break;
		}
	}
}

std::string ParseKey(const std::string& jsontext, long& index)
{
	std::string key;
	index += 1;
	//	Double check to deal with empty key
	while (jsontext[index] != '\'')
	{
		key += jsontext[index++];
	}
	//	index poining to the last char before the ending double quote
	index += 2;		//	Skip ending double quote and colon
	return key;
}

std::shared_ptr<JSONValueNode> ParseValue(const std::string& k, const std::string& jsontext, long& index)
{
	std::string value = "";
	//	index pointing to beginning quote or numbers or decimal point
	if (jsontext[index] == '\'')
	{
		index += 1;
		//	Take everything between the quotes
		while (jsontext[index] != '\'')
		{
			value += jsontext[index];
			index += 1;
		}
		//	index points to the ending double quote
		index += 1;	//	index points to the char after the ending quote
		std::shared_ptr<JSONValueNode> jValueNode = std::make_shared<JSONValueNode>(value, k);
		return jValueNode;
	}
	else
	{
		//	So we are reading numbers here
		while ((jsontext[index + 1] >= '0' && jsontext[index] <= '9') || jsontext[index] == '.')
		{
			value += jsontext[index];
			index += 1;
		}
		//	index points to the first char passing the number
		std::shared_ptr<JSONValueNode> jValueNode = std::make_shared<JSONValueNode>(value, k);
		return jValueNode;
	}
	//	Make sure that for two different cases we both put index AFTER our string/number
}

std::shared_ptr<JSONArrayNode> ParseArray(const std::string& k, const std::string& jsontext, long& index)
{
	std::shared_ptr<JSONArrayNode> jArrayNode = std::make_shared<JSONArrayNode>(k);
	//	Index should be pointing at [
	//	Json Array may contain anything, strings, numbers, objects and arrays
	//	Items in JsonArray do not have keys (only items in JsonObjects have keys)
	index += 1;
	while (index <= jsontext.length())
	{
		switch (jsontext[index])
		{
		case ']':	//	Empty array
			index += 1;		//	Move index past ]
			return jArrayNode;
			break;
		case '{':
			jArrayNode->push_back(ParseObject("", jsontext, index));
			//	ParseObject() puts index at the ending quote
			index += 1;
			break;
		case ',':
			index += 1;
			break;
		default:
			//	In other situations, should be a JSONValue, " or a digit or a decimal point
			jArrayNode->push_back(ParseValue("", jsontext, index));
			//	No need to move index as ParseValue moves to the next char
			break;
		}
	}
}