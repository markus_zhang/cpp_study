#pragma once

// Raw pointer version
template <typename T>
class pNode
{
public:
	pNode();
	pNode(const T& anItem);
	pNode(const pNode& anNode);

	const T& getItem() const;
	T& getItem();
	pNode<T>* getNext() const;
	pNode<T>* getPrev() const;
	void setItem(const T& anItem);
	void setNext(pNode<T>* nextNodePtr);
	void setPrev(pNode<T>* prevNodePtr);

private:
	T item;
	pNode<T>* next;
	pNode<T>* prev;
};

template <typename T>
pNode<T>::pNode() : next(this), prev(this) {}

template <typename T>
pNode<T>::pNode(const T& anItem) : next(this), prev(this), item(anItem) {}

template <typename T>
pNode<T>::pNode(const pNode& anNode) : next(anNode.getNext()), prev(anNode.getPrev()), item(anNode.getItem()) {}

template <typename T>
const T& pNode<T>::getItem() const
{
	return item;
}

template <typename T>
T& pNode<T>::getItem()
{
	return item;
}

template <typename T>
pNode<T>* pNode<T>::getNext() const
{
	return next;
}

template <typename T>
pNode<T>* pNode<T>::getPrev() const
{
	return prev;
}

template <typename T>
void pNode<T>::setItem(const T& anItem)
{
	item = anItem;
}

template <typename T>
void pNode<T>::setNext(pNode<T>* nextNodePtr)
{
	next = nextNodePtr;
}

template <typename T>
void pNode<T>::setPrev(pNode<T>* prevNodePtr)
{
	prev = prevNodePtr;
}