#pragma once
#include <vector>
#include "cJSONNode.h"

class JSONArrayNode : public JSONNode
{
public:
	JSONArrayNode(std::string k) : JSONNode(node_type::jArray, k) {}
	const std::shared_ptr<JSONNode>& operator[](int index);
	node_type GetType() const { return type; }
	void test() { std::cout << "This is array"; }

	//	Operations
	void push_back(std::shared_ptr<JSONNode> node);
private:
	std::vector<std::shared_ptr<JSONNode>> jsonArray;
};