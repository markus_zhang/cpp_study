#pragma once
#include <string>
#include "C4_DoubleLinkedList.h"

//	Programming Problems 8
//	Specify and implement an ADT character string by using a linked chain of characters.Include typical operations
//	such as fi nding its length, appending one string to another, fi nding the index of the leftmost occurrence of
//	a character in a string, and testing whether one string is a substring of another.

class pCharDLinkedList : public pDLinkedList<char>
{
public:
	pCharDLinkedList() : pDLinkedList()	// Constructors are not inherited
	{

	}
	pCharDLinkedList(std::string anString);

	const char& operator[](std::size_t n) const;
	char& operator[](std::size_t n);

	int getLength() const;
	void append(const std::string& anString);
	void append(const pCharDLinkedList& anChDLL);
	void append(std::initializer_list<std::string> ls);
	void append(std::initializer_list<pCharDLinkedList> ls);

	std::string subString(std::size_t start, std::size_t end);
	bool containChar(const char& ch) const;
	std::size_t containStr(const std::string& str) const;
};

const char& pCharDLinkedList::operator[](std::size_t n) const
{
	if (n > size)
	{
		throw "Out of boundary!";
	}
	else
	{
		pNode<char>* node = dummy->getNext();
		for (auto i = 0; i < n; i++)	// i from 0 as we are starting from the first element
		{
			node = node->getNext();
		}
		return node->getItem();
	}
}

char& pCharDLinkedList::operator[](std::size_t n)
{
	if (n > size)
	{
		throw "Out of boundary!";
	}
	else
	{
		pNode<char>* node = dummy->getNext();
		for (auto i = 0; i < n; i++)	// i from 0 as we are starting from the first element
		{
			node = node->getNext();
		}
		return (node->getItem());
	}
}

pCharDLinkedList::pCharDLinkedList(std::string anString)
{
	for (auto i : anString)
	{
		add(i);
	}
}

int pCharDLinkedList::getLength() const
{
	return size;
}

void pCharDLinkedList::append(const std::string& anString)
{
	for (auto i : anString)
	{
		add(i);
	}
}

void pCharDLinkedList::append(const pCharDLinkedList& anChDLL)
{
	if (!anChDLL.isEmpty())
	{
		pNode<char>* node = (anChDLL.getDummy())->getNext();
		while (node != anChDLL.getDummy())
		{
			add(node->getItem());
			node = node->getNext();
		}
	}
}

void pCharDLinkedList::append(std::initializer_list<std::string> ls)
{
	for (auto str : ls)
	{
		append(str);
	}
}

std::string pCharDLinkedList::subString(std::size_t start, std::size_t end)
{
	std::string str = "";
	if (start <= end && end <= size)
	{
		for (auto i = start; i <= end; i++)
		{
			str += pCharDLinkedList::operator[](i);
		}
	}
	return str;
}

bool pCharDLinkedList::containChar(const char& ch) const
{
	bool bInStr = false;
	for (auto i = 0; i <= size-1; i++)
	{
		if (ch == pCharDLinkedList::operator[](i))
		{
			bInStr = true;
		}
	}
	return bInStr;
}

std::size_t pCharDLinkedList::containStr(const std::string& str) const
{
	// Horspool algorithm
	// Part 1 - Pre-processing
	std::vector<int> occ(128, -1);
	for (auto ch : str)
	{
		occ[ch] = (str.substr(0, str.length() - 1)).rfind(ch);	// Find rightmost position of ch in str, ignoring the last character of str
	}

	// Part 2 - Compare and shift
	int i = 0, j;
	while (i <= size - str.length())
	{
		j = str.length() - 1;
		while (j >= 0 && str[j] == pCharDLinkedList::operator[](i + j))
		{
			j -= 1;
		}
		if (j < 0)	// We have a match
		{
			return i;
		}
		i += str.length() - 1;
		i -= occ[pCharDLinkedList::operator[](i)];
	}
	return -1;
}