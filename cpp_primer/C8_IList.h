#pragma once

template <typename T>
class IList
{
public:
	virtual ~IList() {}
	virtual bool isEmpty() = 0;
	virtual int getLength() const = 0;
	virtual bool insert(int pos, const T& item) = 0;
	virtual bool remove(int pos) = 0;
	virtual void clear() = 0;
	virtual T getEntry(int pos) const = 0;
	virtual void setEntry(int pos, const T& item) = 0;
};
