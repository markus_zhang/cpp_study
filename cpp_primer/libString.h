#pragma once

#include <string>
#include <algorithm>
#include <iostream>
#include "header.h"

/*
	String classification functions
*/

namespace libString
{
	//	Single Character Classification Functions
	bool IsCharNumeric(char ch)
	{
		if (ch >= '0' && ch <= '9')
		{
			return true;
		}
		return false;
	}

	bool IsCharAlphabetic(char ch)
	{
		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
		{
			return true;
		}
		return false;
	}

	bool IsCharWhitespace(char ch)
	{
		if (ch == ' ' || ch == '\t')
		{
			return true;
		}
		return false;
	}

	bool IsCharLineBreak(char ch)
	{
		if (ch == '\n')
		{
			return true;
		}
		return false;
	}

	bool IsCharIdent(char ch)
	{
		if ((ch >= 'A' && ch <= 'Z') || 
			(ch >= 'a' && ch <= 'z') ||
			(ch >= '0' && ch <= '9') ||
			(ch == '_'))
		{
			return true;
		}
		return false;
	}

	bool IsCharDelim(char ch)
	{
		if (ch == ':' || ch == ',' || ch == '"' ||
			ch == '[' || ch == ']' ||
			ch == '{' || ch == '}' ||
			IsCharWhitespace(ch) || ch == '\n')
		{
			return true;
		}
		return false;
	}

	//	String Functions
	bool IsStringInt(std::string str)
	{
		bool result = true;
		if (str.length() <= 0)
		{
			return false;
		}
		if (str[0] == '-')
		{
			str = str.substr(1, str.length() - 1);
		}
		if (str.length() > 0)
		{
			for (auto ch : str)
			{
				if (ch < '0' || ch > '9')
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
		return result;
	}

	bool IsStringFloat(std::string str)
	{
		bool result = true;
		if (str.length() <= 0)
		{
			return false;
		}
		int numDecimal = 0;
		if (str[0] == '-')
		{
			str = str.substr(1, str.length() - 1);
		}
		if (str.length() > 0)
		{
			for (auto ch : str)
			{
				if (ch < '0' || ch > '9')
				{
					if (ch == '.')
					{
						if (numDecimal > 0)
						{
							return false;
						}
						else
						{
							numDecimal += 1;
						}
					}
					else
					{
						return false;
					}
				}
			}
		}
		else
		
		{
			result = false;
		}
		return result;
	}

	bool IsStringWhitespace(std::string str)
	{
		if (str.length() <= 0)
		{
			return false;
		}

		for (auto ch : str)
		{
			if (IsCharWhitespace(ch))
			{
				return false;
			}
		}
		return true;
	}

	bool IsStringIdent(std::string str)
	{
		if (str.length() <= 0)
		{
			return false;
		}

		if (str[0] >= '0' && str[0] <= '9')
		{
			return false;
		}

		for (auto ch : str)
		{
			if (!IsCharIdent(ch))
			{
				return false;
			}
		}
		return true;
	}

	bool IsStringInstruction(const std::string& input)
	{

	}

	std::string RemoveLineBreak(std::string input)
	{
		std::string output;
		for (auto ch : input)
		{
			if (!IsCharLineBreak(ch))
			{
				output.push_back(ch);
			}
		}
		return output;
	}

	std::string TrimLine(std::string input)
	{
		//	Remove whitespaces at beginning of the string
		input.erase(input.begin(), std::find_if(input.begin(), input.end(), [](int ch)
		{
			return !IsCharWhitespace(ch);
		}
		));

		//	Remove whitespaces at end of the string
		input.erase(std::find_if(input.rbegin(), input.rend(), [](int ch)
		{
			return !IsCharWhitespace(ch);
		}
		).base(), input.end());

		return input;
	}

	//	Helper functions to extract information from source file

	//	Get the number of lines from a std::string
	int getNumLines(const std::string& source)
	{
		if (source.length() <= 0)
		{
			return 0;
		}
		int numLines = 0;
		for (auto ch : source)
		{
			if (IsCharLineBreak(ch))
			{
				numLines += 1;
			}
		}
		return numLines + 1;
	}

	std::string getLine(std::string& subString)
	{
		//	subString is the sub string of source with the current line removed
		//	call getLine() until it reaches the end

		if (subString.length() <= 0)
		{
			return "";
		}
		std::string result;
		int eol = subString.find('\n');

		if (eol < 0)
		{
			result = subString;
			subString = "";
		}
		else
		{
			result = TrimLine(RemoveLineBreak(subString.substr(0, eol + 1)));
			subString = subString.substr(eol + 1, subString.length() - eol - 1);
		}
		return result;		
	}

	//	previous version tokenizer, please remove afterwards
	std::string GetNextTokenTest(const std::string& input, int& offset)
	{
		//	Get first token from begin
		//	Should be used after getLine, and called until the end
		if (input.length() <= 0)
		{
			return "";
		}
		//	Extract substr between two whitespaces
		std::string inputSubstr = input.substr(offset, input.length() - offset);
		int posLeftWhiteSpace = ((IsCharWhitespace(inputSubstr[0])) ? (input.find_first_not_of(" \t\n")) : (-1)) - 1;
		int posRightWhiteSpace = ((posLeftWhiteSpace < 0) ? (inputSubstr.find_first_of(" \t\n")) : (inputSubstr.substr(posLeftWhiteSpace + 1, inputSubstr.length() - posLeftWhiteSpace - 1).find_first_of(" \t\n") + posLeftWhiteSpace + 1));
		
		if (posLeftWhiteSpace < 0)
		{
			if (posRightWhiteSpace < 0)
			{
				offset = -1;	//	To stop the algo
				return inputSubstr;
			}
			else
			{
				offset += posRightWhiteSpace + 1;
				return inputSubstr.substr(0, posRightWhiteSpace);				
			}
		}
		else
		{
			if (posRightWhiteSpace < 0)
			{
				offset = -1;	//	To stop the algo
				return inputSubstr.substr(posLeftWhiteSpace + 1, inputSubstr.length() - posLeftWhiteSpace - 1);				
			}
			else
			{
				offset += posRightWhiteSpace;
				return inputSubstr.substr(posLeftWhiteSpace + 1, posRightWhiteSpace - posLeftWhiteSpace - 1);
			}
		}
	}

	void CreateInStructFromLine(std::string input)
	{
		//_psuedo_instruction output;
		//output.opcode = "NaN";

		if (input.length() <= 0)
		{
			//	return output;	//	"NaN" is for check
			return;
		}
		
		int offset = 0;
		while (offset < input.length() && offset >= 0)	// offset could be set to -1, read GetNextToken()
		{
			std::cout << "**" << GetNextTokenTest(input, offset) << "**" << std::endl;
		}
	}

	std::string toLower(const std::string& input)
	{
		std::string temp = input;
		std::transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
		return temp;
	}
}