//#include <SDL.h>
//#include <iostream>
//
//int main(int argc, char* args[])
//{
//	//	How to declare a pointer to an array
//	double dArray[20];				//	Array of Double with 20 elements
//	double* dpArray[20];			//	Array of 20 pointers to Double
//	double (*pdArray)[20];			//	Pointer to an Array of 20 Double elements
//	double(&rdArray)[20] = dArray;	//	Reference to an Array of 20 Double elements
//	pdArray = &dArray;				//	Points the pointer to the array
//
//	//double& rpArray[20];			//	Reference array is not allowed
//
//	//	More complicated declaration (trick is to read from inside to outside)
//	char* pchArray[100];						//	Array of 100 pointers to char
//	char *(&rpchArray)[100] = pchArray;			//	Reference to an Array of 100 pointers to char
//	//	char &(*prchArray)[100];				//	Array of reference is NOT allowed
//
//	//	Pointer and Array
//	double* pd = &dArray[0];
//	std::cout << pd;
//	pd = &dArray[1];
//	std::cout << pd;
//
//	//	More Pointer and Array
//	int iArray[] = { 1,2,3,4,5,6,7,8,9,10 };
//	auto iArray2(iArray);						//	iArray2 is a pointer pointing to iArray[0]
//	iArray2[2] = 100;
//	std::cout << std::endl << iArray[2];		//	Modifying iArray2 effectively modifies iArray, as the pointer points to same memory
//
//	decltype(iArray) iArray3;					//	decltype forces iArray3 to be an array instead of a pointer
//	iArray3[1] = 999;
//	std::cout << std::endl << iArray[1];		//	Stays with 2 instead of 999
//
//	return 0;
//}