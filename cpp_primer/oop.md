### OOP
- Base and Derived
  - Base class almost certainly use a *virtual* destructor
  - When we call a *virtual* function through **pointer** or **reference**, the call will be dynamically bound

- **Conversion between base and inherited**
  - *Static Type* is known at compile time; *Dynamic Type* is known at run time. An item, may have a static type and a dynamic type at the same time.
  - There is **no** implicit type conversion from base to derived. Because base class does not have the info of derived ones. However there **is** conversion from derived to base.
	```C++
	JSONNode base(node_type::jArray, "XXX");
	JSONArrayNode* pDerived = &base;		//	No type conversion from base to derived
	```
	It doesn't work if you try to point a pointer of derived class to a pointer of base class:
	```C++
	JSONNode base(node_type::jArray, "XXX");
	JSONNode* pBase = &base;
	JSONArrayNode* pDerived = pBase;	// Doesn't work
	```
  - We can convert reference or pointer of derived to base, but we cannot convert type (object) of derived to base
	With one exception (although it's probably wrong), we can use copy constructor to do the conversion:
	```C++
	JSONArrayNode derived("Derived");
	JSONNode base(derived);

  - It's very important to realize when the object will be **stripped down** to base class
    This may result in some nonsensical code:
	```
	class base
	{
	private:
		//	Whatever
	public:
		virtual SetType();
		int CalculateBasePrice()
		{
			//	Method 1
		}
	};

	class derived : public base
	{
	private:
		//	Whatever
	public:
		SetType() override;
		int CalculateDerivedPrice()
		{
			//	Method 2
		}
	};

	//	Now we can actually do this:
	derived foo;
	int price = foo.CalculateBasePrice();
	```	
  - **Derivation access specifier** and **In-class access specifier**
	```
	class base
	{
	private:
		int mem_private;
	public:
		base() : mem_private(255)	{}
		void memfunc_public()
		{
			std::cout << "I'm public" << std::endl;
		}
	protected:
		double memfunc_protected()
		{
			return 0.02 * 5.06;
		}
	};

	class derived_public : public base
	{
	public:
		void padd()
		{
			//	std::cout << mem_private << std::endl;	// not possible as it's private
			std::cout << memfunc_protected() << std::endl;
			memfunc_public();
		}
	};

	class derived_private : private base
	{
	public:
		void padd()
		{
			//	std::cout << mem_private << std::endl;	// not possible as it's private
			std::cout << memfunc_protected() << std::endl;
			memfunc_public();
		}
	};

	class derived_protected : protected base
	{
	public:
		void padd()
		{
			//	std::cout << mem_private << std::endl;	// not possible as it's private
			std::cout << memfunc_protected() << std::endl;
			memfunc_public();
		}
	};
	```
	The *private* and *protected* behind the colon in class declaration are **Derivation access specifiers**

	Basically it means any function from the *base class* will be *private* or *protected* accordingly, and cannot be used by the users of derived classes
	
	The *private* and *protected* inside of each class are **In-class access specifiers**

	It restricts the usage of these functions *vertically* i.e. within the inheritance hierachy

- Polymorphism
  - Static Polymorphism = Decided at compile time
    - Function overloading, operator overloading
    - implicit primitive type conversion e.g. in ```a+b``` a and b can be double or int or others
  
  - Dynamic Polymorphism = Decided at run time
  
  - The *virtual* function overrided in derived classes must have **the same parameter types** as the base-class function
  
  - With one exception, return types must also be the same
	i.e. the return type is itself related to certain inheritance relationship

  - Use the scope operator to access functions in base class

  - For **Abstract base class**, which contains at least one *pure virtual* function, we **cannot** directly create an object of that class
	Essentially, it is an *interface*, and is not intended to be used directly

- Friendship in class
  - You can make class A a friend of class B:
	```
	class A
	{
		friend class B;
	};
	```

  - You can also make a member function mf_A() a friend of class B:
	```
	class A
	{
		friend B::mf_A();
	};
	```

  - Friendship is neither *transitive* nor *inherited*
	E.g.
	
	1) class A is a friend of class B, class C is a friend of class A
    
		class A may access members of class B, but class C is **not** able to do that

	2) class A is a friend of class B, class C is inherited from class A
		
		class A may access members of class B, but class C is **not** able to do that

  - Friend of base class **can** access all members of base class through objects of derived classes, if they are **publicly** derived
 
	Friend of base class **can** access public members of derived classes even without Friend

	```
	class base
	{
		friend class friend_base;
	private:
		int mem_private;
	public:
		base() : mem_private(255)	{}
		void memfunc_public()
		{
			std::cout << "I'm public" << std::endl;
		}
	protected:
		double memfunc_protected()
		{
			return 0.02 * 5.06;
		}
	};

	class derived_public : public base
	{
	private:
		int dp_private;
	protected:
		int dp_protected;
	public:
		void padd()
		{
			std::cout << memfunc_protected() << std::endl;
			memfunc_public();
		}

		void derived_public_test()
		{
			std::cout << "Derived Public Test" << std::endl;
		}
	};

	class friend_base
	{
	public:
		void call_friend()
		{
			base b;
			std::cout << b.mem_private;	// May access a private member
		}

		void call_friend_derived()
		{
			derived_public d;
			d.memfunc_public(); // May access the base members of a derived class (note that friend_base is NOT a friend of class derived_public)
			d.derived_public_test(); // Can also access the derived class' public members
			std::cout << d.dp_private; // CANNOT access the derived class' private members
			std::cout << d.dp_protected; // CANNOT access the derived class' protected members
		}
	};
	
	```
	In the above example, **friend_base** may access *memfunc_public()*, which is a public member of base class

	In the above example, **friend_base** may access *derived_public_test()*, which is a public member of derived class

	friend_base is **NOT** a *friend* of derived class

	However, if it's a **privately** derived class object, then **friend_base** cannot access even public members of base class through it
	```
	class derived_private : private base
	{
	public:
		void padd()
		{
			//	std::cout << mem_private << std::endl;	// not possible as it's private
			std::cout << memfunc_protected() << std::endl;
			memfunc_public();
		}
	};	

	class friend_base
	{
	public:
		void call_friend()
		{
			base b;
			std::cout << b.mem_private;	// May access a private member
		}

		void call_friend_derived()
		{
			std::cout << "Privately inherited class" << std::endl;
			derived_private dp;
			dp.memfunc_public();	// Compile error: cannot access from privately inherited class
		}
	};

	```

  - Virtual functions in base and derived classes **must have the same parameter list**

	The reason is simple: We use polymorphism to call functions with same name in dynamic types (derived) **through ref/ptr to base class**
	```
	std::vector<std::shared_ptr<cBaseObject>> ObjectList;
	for (auto obj : ObjectList)
	{
		obj->Render();
	}
	```
	Now if we have different parameter lists for some of the derived classes, there is no way to achieve this.
	```
	class base
	{
	private:
		int mem_private;
	public:
		base() : mem_private(255)	{}
		virtual void virtual_test();
	};

	class derived_virtual_test : public base
	{
		void virtual_test(int a, int b)
		{
			std::cout << "This is NOT an overloaded virtual_test" << std::endl;
		}
	};   

	// In Main.cpp
	derived_virtual_test vt;
	derived_virtual_test* pvt = &vt;
	pvt->virtual_test(); // Error, hidden from dervied class
	pvt->base::virtual_test(); 
	```

  - Base class should have *virtual* destructor
	
	The reason is that if we are using Polymorphism, we are referencing **derived class** objects (or maybe base class as well) through a **base class** pointer/reference

	Now let's assume that one of the **derived class** object needs to be released:
	```
	delete baseptr;
	```
	Which destructor will be called? If the destructor is not *virtual*, then only the destructor of the **base class** will be called

	Result is that we have a *partially* deleted object.

  - Constructioin/Destruction and Virtual functions

	In construction, base first and derived next

	In destruction, derived first and base next

	This actually means a lot:

	1) In the first stage of construction, C++ treats the object as a base class object
    
	2) Because of this, the ctor of base class object will be called

	3) Assuming that base class ctor calls a virtual function that gets overloaded in derived classes:

	```
	class base2
	{
	public:
		base2()
		{
			v();
		}

		virtual void v()
		{
			std::cout << "Base Virtual Function" << std::endl;
		}
	};

	class derived2 : public base2
	{
	public:
		derived2()
		{
			v();
		}
	
		void v()
		{
			std::cout << "Derived Virtual Function" << std::endl;
		}
	};

	//	In Main.cpp
	derived2 d2;
	```
	Now remember that in construction, the first stage is about the base class object

	So we will see an output of text message "Base Virtual Function"

	Then it will start building the derived class object, and we will see another text message "Derived Virtual Function"