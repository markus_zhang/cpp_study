#include "cGraphics.h"

cGraphics::cGraphics(int xWin, int yWin, std::string texid, std::string texres) : cGraphics()
{
	SDL_Init(SDL_INIT_VIDEO);

	m_Window = Create_Window(xWin, yWin);
	m_Renderer = Create_Renderer();

	if (m_Window == nullptr || m_Renderer == nullptr)
	{
		throw "SDL_Window or SDL_Renderer not ready!";
	}

	/*if (m_Window == nullptr || m_Renderer == nullptr || !Create_TextureMap(texid, texres, 255, 255, 255))
	{
	throw "SDL_Window or SDL_Renderer not ready!";
	}*/
}

cGraphics::~cGraphics()
{
	IMG_Quit();
	SDL_Quit();
}

void cGraphics::ClearScreen()
{
	SDL_RenderClear(m_Renderer.get());
}

bool cGraphics::SetRendererDrawColor(int r, int g, int b, int opaque)
{
	/*SDL_Renderer* temprenderer = SDL_CreateRenderer(m_Window.get(), -1, 0);
	SDL_SetRenderDrawColor(temprenderer, r, g, b, opaque);
	m_Renderer.reset(temprenderer);*/
	SDL_SetRenderDrawColor(m_Renderer.get(), r, g, b, opaque);
	return true;
}

void cGraphics::RenderTexture(std::string texres, int xDes, int yDes)
{
	SDL_Rect g_SrcRect = { 0, 0, 32, 32 };			//	hard-coded for test
	SDL_Rect g_DesRect = { xDes, yDes, 32, 32 };
	SDL_RenderCopy(m_Renderer.get(), m_TextureMap[texres].get(), &g_SrcRect, &g_DesRect);
}

bool cGraphics::Show()
{
	SDL_RenderPresent(m_Renderer.get());
	return true;
}

std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> cGraphics::Create_Window(int xWin, int yWin)
{
	return std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>(SDL_CreateWindow("SDL Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, xWin, yWin, SDL_WINDOW_SHOWN), SDL_DestroyWindow);
}

std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> cGraphics::Create_Renderer()
{
	SDL_Renderer* temprenderer = SDL_CreateRenderer(m_Window.get(), -1, 0);
	SDL_SetRenderDrawColor(temprenderer, 0, 0, 0, 0xff);
	return std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)>(temprenderer, SDL_DestroyRenderer);
}


bool cGraphics::Create_TextureMap(std::string texid, std::string texres, int r, int g, int b)
{
	m_TextureMap.emplace(texid, CreateTexture(texres, r, g, b));
	return true;
}

SDL_Texture* cGraphics::CreateTextureRawPtr(std::string texres, int r, int g, int b)
{
	SDL_Surface* temp = IMG_Load(texres.c_str());
	//Set color key
	SDL_SetColorKey(temp, SDL_TRUE,
		SDL_MapRGB(temp->format, r, g, b));

	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(m_Renderer.get(), temp);

	SDL_FreeSurface(temp);
	return pTexture;
}

std::shared_ptr<SDL_Texture> cGraphics::CreateTexture(std::string texres, int r, int g, int b)
{
	return std::shared_ptr<SDL_Texture>(CreateTextureRawPtr(texres, r, g, b), SDL_DestroyTexture);
}