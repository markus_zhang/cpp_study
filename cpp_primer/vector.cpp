#include <SDL.h>
#include <vector>
#include <iostream>

/*
int main(int argc, char* args[])
{
	//	1 - A std::vector of char*, more preper way is to use a std::vector<std::vector<char>> or std::vector<std::string>
	std::vector<char*> v(10, "hi!");	//	You cannot put standard library containers e.g. char[] into std::vector!
	for (auto& i : v)
	{
		//std::cout << i << std::endl;
		i = "New";
	}
	for (auto i : v)
	{
		std::cout << i << std::endl;
	}
	std::cout << std::endl;

	//	2 - Presentation of incorrect syntax
	//	std::vector<char[]> x;				//	Although the compiler does not complain, this is incorrect
	//	char x1[] = { '1', '\0' };
	//	char x2[] = { '2', '\0' };
	//	x.push_back(x1);				//	Complier will complain!
	//	x.push_back(x2);				//	Compiler will complain!

	//	3 - One way to initiate a std::vector
	std::vector<char> x(10, 'a');		//	A vector of char is allowed
	for (auto& i : x)
	{
		//i *= i;
	}
	std::vector<char> w(x);
	for (auto i : w)
	{
		std::cout << i << ", ";
	}
	std::cout << std::endl;

	//	4 - std::vector of a user type
	struct foo
	{
		int m_amy;
		foo() : m_amy(100) {}
	};

	std::vector<foo> vfoo(10);		//	Automatically calls the constructor

	for (auto i : vfoo)
	{
		std::cout << i.m_amy << ", ";
	}
	

	//	5 - Demonstrate the remove/erase idiom
	//	Using the foo struct we just built
	
	return 0;
}
*/