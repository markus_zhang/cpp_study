#pragma once

template <typename T>
class pBSTreeNode
{
public:
	pBSTreeNode() : leftChild(nullptr), rightChild(nullptr) {}
	pBSTreeNode(const T& newItem);
	pBSTreeNode(const T& newItem, pBSTreeNode<T>* newLeft, pBSTreeNode<T>* newRight);
	~pBSTreeNode();

	bool isLeaf() const { return (leftChild == nullptr && rightChild == nullptr); }
	bool hasLeftChild() const { return leftChild != nullptr; }
	bool hasRightChild() const { return rightChild != nullptr; }
	T getItem() const { return item; }
	void setItem(const T& newItem) { item = newItem;	/* require copy assignment constructor */ }
	pBSTreeNode<T>* getLeftChild() const { return leftChild; }
	pBSTreeNode<T>*& getLeftChild() { return leftChild; }
	pBSTreeNode<T>* getRightChild() const { return rightChild; }
	pBSTreeNode<T>*& getRightChild() { return rightChild; }
	void displayNode() const { std::cout << item << ", "; }
private:
	T item;
	pBSTreeNode<T>* leftChild;
	pBSTreeNode<T>* rightChild;

	pBSTreeNode(const pBSTreeNode& node) {}
};

template <typename T>
pBSTreeNode<T>::pBSTreeNode(const T& newItem)
{
	item = newItem;
}

template <typename T>
pBSTreeNode<T>::pBSTreeNode(const T& newItem, pBSTreeNode<T>* newLeft, pBSTreeNode<T>* newRight)
{
	item = newItem;
	leftChild = newLeft;
	rightChild = newRight;
}

template <typename T>
pBSTreeNode<T>::~pBSTreeNode()
{
	leftChild = rightChild = nullptr;
}